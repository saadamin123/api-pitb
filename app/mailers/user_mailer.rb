class UserMailer < ApplicationMailer
  default from: 'ata.rabi@clustox.com'
  def send_email_to_complainer(user)
    @user = user
    mail(to: @user.email,
         subject: 'Thanks for sending me an email')
  end
end
