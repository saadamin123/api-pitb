class VitalSerializer < ActiveModel::Serializer
  attributes :id, :weight, :height, :temperature,
             :bp_systolic, :bp_diastolic, :pulse, :resp_rate,
             :o2_saturation, :head_circ, :created_at, :updated_at, :medical_unit_title

	def medical_unit_title
 		object.medical_unit.present? ? object.medical_unit.title : nil
	end
end
