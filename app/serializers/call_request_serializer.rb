# classs Serializer
class CallRequestSerializer < ActiveModel::Serializer
  attributes :id, :code, :created_at, :updated_at
  has_one :from
  has_one :to
end
