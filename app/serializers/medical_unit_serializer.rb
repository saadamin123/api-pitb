# classs Serializer
class MedicalUnitSerializer < ActiveModel::Serializer
  attributes :id,
             :region_id,
             :title,
             :category,
             :identification_number,
             :location,
             :latitude,
             :longitude,
             :created_at,
             :updated_at
    # has_many :stores
    # has_many :departments
end
