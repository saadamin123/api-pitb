# classs Serializer
class FollowUpSerializer < ActiveModel::Serializer
  attributes :id,:follow_up,:patient_id,:patient_name

  def patient_name
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.full_name : nil
  end
end
