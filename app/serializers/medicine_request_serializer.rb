# classs Serializer
class MedicineRequestSerializer < ActiveModel::Serializer
  attributes :id,
             :medical_unit_id,
             :patient_id,
             :visit_id,
             :admission_id,
             :prescribed_by,
             :requested_by,
             :status,
             :item_id,
             :request_id,
             :prescribed_qty,
             :product_name,
             :requested_by_user,
             :created_at,
             :updated_at
  def requested_by_user
      user = User.where(id: object.requested_by) 
  end
end