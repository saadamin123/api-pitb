# classs Serializer
class ComplaintSerializer < ActiveModel::Serializer
  attributes :id,
             :device_id,
             :category,
             :area,
             :description,
             :severity_level,
             :show_identity,
             :status,
             :critical,
             :assign_to_role,
             :is_favorite,
             :high_priority,
             :created_at,
             :updated_at
  has_one :user
  has_one :medical_unit
  has_many :pictures

  def is_favorite
    current_user.favorite_complaints.where(complaint_id: self.id).present?
  end
end
