# classs Serializer
class LookupSerializer < ActiveModel::Serializer
  attributes :id,
             :category,
             :key,
             :value,
             :parent_id,
             :created_at,
             :updated_at
end
