class DrugSerializer < ActiveModel::Serializer
  attributes :id, :name, :overview, :category
end
