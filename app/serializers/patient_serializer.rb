# classs Serializer
class PatientSerializer < ActiveModel::Serializer
  attributes :id,
             :registered_by,
             :registered_at,
             :mrn,
             :first_name,
             :last_name,
             :middle_name,
             :gender,
             :education,
             :birth_day,
             :birth_month,
             :birth_year,
             :patient_nic,
             :patient_passport,
             # :patient_phone,
             :unidentify_patient,
             # :date_of_birth,
             :guardian_relationship,
             :guardian_first_name,
             :guardian_last_name,
             :guardian_middle_name,
             :guardian_nic,
             :guardian_passport,
             :unidentify_guardian,
             :marital_status,
             :state,
             :city,
             :near_by_city,
             :address1,
             :address2,
             :phone1,
             :phone1_type,
             :phone2,
             :phone2_type,
             :phone3,
             :phone3_type,
             :blood_group,
             :hiv,
             :hepatitis_b_antigens,
             :hepatitis_c,
             :age,
             :father_name,
             :unidentify_gender,
             :created_at,
             :updated_at,
             #:location,
             #:last_updated_at,
             :is_active,
             :last_visit,
             :barcode_url,
             :qrcode_url,
             :guardian_state,
             :guardian_city,
             :guardian_near_by_city,
             :guardian_address,
             :guardian_phone_type,
             :guardian_phone,
             :unidentify,
             :open_medicines
     has_one :medical_history
     has_one :surgical_history
     has_many :activities
     has_many :admissions
     has_many :medicine_requests

  def location
    visits = object.visits
    return visits.present? ? visits.last.category_dept : nil
  end
  def last_updated_at
    visits = object.visits
    return visits.present? ? visits.last.updated_at : nil
  end
  def is_active
    patient = DoctorPatient.where(doctor_id: current_user.id,patient_id: object.id)
    if patient.present?
      return patient.last.active
    else
      return false
    end
  end
  def last_visit
    visits = Visit.where(patient_id: object.id ,is_active: true).order("id desc").limit(1)
    return visits.present? ? visits.first : nil
  end
  def open_medicines
    open_medicines = MedicineRequest.joins(:patient).where("mrn = ? AND status =? AND store_id =?", object.mrn, "Open", current_user.stores.first.id) unless current_user.stores.empty?
    return open_medicines.present? ? open_medicines : nil
  end
  # def barcode_url
  #   return "barcode/#{object.created_at.strftime("%Y%m%d")}/barcode_#{object.mrn}.png"
  # end
  # def qrcode_url
  #   return "qrcode/#{object.created_at.strftime("%Y%m%d")}/qrcode_#{object.mrn}.png"
  # end

end
