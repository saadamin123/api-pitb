class LabTestDetailSerializer < ActiveModel::Serializer
  attributes :id, :caption, :range, :unit, :value, :comments, :created_at, :updated_at
end