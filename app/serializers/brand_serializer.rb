class BrandSerializer < ActiveModel::Serializer
  attributes :id, :name, :company_id, :category
  has_one :company
end
