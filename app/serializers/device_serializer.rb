# classs Serializer
class DeviceSerializer < ActiveModel::Serializer
  attributes :id,
             :uuid,
             :assignable_id,
             :assignable_type,
             :role_id,
             :operating_system,
             :os_version,
             :status,
             :latitude,
             :longitude,
             :created_at,
             :updated_at,
             :medical_unit,
             :departments

  def medical_unit
    MedicalUnit.find_by_id(object.assignable_id) if assignable_type == 'MedicalUnit' # rubocop:disable Metrics/LineLength
  end
  def departments
    mu = MedicalUnit.find_by_id(object.assignable_id) if assignable_type == 'MedicalUnit'
    mu.departments if mu.present?
  end
end
