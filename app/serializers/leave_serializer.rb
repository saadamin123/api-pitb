# classs Serializer
class LeaveSerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :medical_unit_id,
             :doctor_name,
             :attendance,
             :shift,
             :date,
             :created_at,
             :updated_at
  def doctor_name
    doc = User.find(user_id)
    return doc ? doc.full_name : nil
  end
end
