class LineItemSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :brand_drug_id, :quantity, :category, :user_id, :expiry, :medical_unit_id, :device_id, :reason, :transaction_type, :order_item_id
  has_one :inventory
end
