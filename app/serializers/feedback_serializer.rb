# classs Serializer
class FeedbackSerializer < ActiveModel::Serializer
  attributes :id,
             :complaint_id,
             :satisfaction,
             :status,
             :message,
             :created_at,
             :updated_at
  has_one :user
end
