# classs Serializer
class MedicalUnitUserSerializer < ActiveModel::Serializer
  attributes :id,
             :user,
             :medical_unit_id,
             :role_id,
             :created_at,
             :updated_at
end