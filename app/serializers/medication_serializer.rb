class MedicationSerializer < ActiveModel::Serializer
  attributes :id, :brand_drug_id, :dosage, :unit, :route,:frequency, :usage,
             :instruction, :created_at, :updated_at, :type, :user_name

  def user_name
    object.user.present? ? object.user.full_name : nil
  end
end
