class PatientHistorySerializer < ActiveModel::Serializer
  attributes :id,:visit_id,:patient_id,:medical_unit_id,:visit_reason,
             :chief_complaint,:diagnosis,:notes,:medication_order,:disposition,
             :allergies,:doctor_name,:created_at,:updated_at
  has_one :user
end
