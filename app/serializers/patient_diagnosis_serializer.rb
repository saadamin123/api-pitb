# classs Serializer
class PatientDiagnosisSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :code,
             :created_at,
             :type,
             :user
  def user_name
    object.user.present? ? object.user.full_name : nil
  end
end
