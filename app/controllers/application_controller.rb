# class documention
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # include SeamlessDatabasePool::ControllerFilter
  # use_database_pool :all => :persistent, [:create, :update, :destroy] => :master

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  skip_before_filter :verify_authenticity_token
  after_filter :allow_cross_domain

  include PublicActivity::StoreController
  # ...
  hide_action :current_user

  def allow_cross_domain
    headers["Access-Control-Allow-Origin"] = request.headers['Origin'] ||'*'
    headers["Access-Control-Request-Method"] = "*"
    headers["Access-Control-Allow-Methods"] = "PUT, OPTIONS, GET, DELETE, POST"
    headers['Access-Control-Allow-Headers'] = '*,x-requested-with,Content-Type,application/json'
    headers["Access-Control-Max-Age"] = '1728000'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
  end

  def access_denied(exception)
    redirect_to admin_organizations_path, alert: exception.message
  end


  def options                                                                                                                                                                                                                                                                              
    head :status => 200, :'Access-Control-Allow-Headers' => 'accept, content-type'                                                                                                                                                                                                         
  end

  protected

  def from_date
    return Date.strptime((params[:from]).gsub("/","-"), "%Y-%m-%d") if params[:from]
    Date.today - 4.weeks
  end

  def to_date
    return Date.strptime((params[:to]).gsub("/","-"), "%Y-%m-%d")+1 if params[:to]
    Date.today+1
  end

  def medical_unit_ids
    ids = nil
    if params[:medical_unit_ids]
      ids =  params[:medical_unit_ids].split(',') 
      ids ||= params[:medical_unit_ids]
      ids = MedicalUnit.where(region_id: params[:medical_unit_ids]).pluck(:id) if params[:category] == 'district'
      district_ids = Region.where(parent_id:  params[:medical_unit_ids]).pluck(:id) if params[:category] == 'province'
      ids = MedicalUnit.where(region_id: district_ids).pluck(:id) if params[:category] == 'province'
    end
    ids
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) } # rubocop:disable Metrics/LineLength
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) } # rubocop:disable Metrics/LineLength
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) } # rubocop:disable Metrics/LineLength
  end

  def verify_authenticity_token
    @current_user = User.find_by_authentication_token(params[:auth_token])
    render status: :unauthorized, json: { message: 'invalid authentication token' } unless @current_user # rubocop:disable Metrics/LineLength
  end

  def verify_device
    @current_device = Device.find_by_uuid(params[:device_id])
    render status: :unauthorized, json: { message: 'unidentify device' } unless @current_device # rubocop:disable Metrics/LineLength
  end

  def fetch_medical_unit
    @current_medical_unit = MedicalUnit.find_by_id(params[:medical_unit_id])
    render status: :unauthorized, json: { message: 'invalid medical unit id' } unless @current_medical_unit
  end

  def fetch_department
    @current_department = Department.find_by_id(params[:department_id])
    render status: :unauthorized, json: { message: 'invalid department id' } unless @current_department
  end

  def current_medical_unit
    MedicalUnit.find_by_id(params[:medical_unit_id])
  end

  def fetch_patient
    @patient = Patient.find_by_mrn(params[:patient_id])
    render status: :unauthorized, json: { message: 'invalid patient mrn' } unless @patient
  end

   def verify_patient
    if params[:patient_id]
      @patient = Patient.find_by_mrn(params[:patient_id])
      render status: :unauthorized, json: { message: 'invalid patient mrn' } unless @patient
    end
  end

  def fetch_complaint
    @complaint = Complaint.find_by_id(params[:complaint_id])
    render status: :unauthorized, json: { message: 'invalid complaint id' } unless @complaint
  end

  def verify_device_medical_unit
    if @current_device.nil? || @current_device.assignable_type != MedicalUnit.name
      render status: :unauthorized, json: { message: 'This device not authorize to perform this operation' }
    end
  end
  def verify_device_role_medical_unit
    # unless @current_device.assignable_type == MedicalUnit.name and (ROLES_ALLOWED.include? @current_user.role.title)
    if (ROLES_ALLOWED - @current_user.roles.pluck(:title)).empty?
      render status: :unauthorized, json: { message: 'This device not authorize to perform this operation' }
    end
  end
  def fetch_visit
    @visit = Visit.find_by_id(params[:visit_id])
    render status: :unauthorized, json: { message: 'invalid visit id' } unless @visit
  end
  def verify_brand_drug
    @brand_drug = BrandDrug.find_by_id(params[:brand_drug_id])
    render status: :unauthorized, json: { message: 'invalid brand drug id' } unless @brand_drug
  end
  def fetch_immunization
    @immunization = Immunization.find_by_id(params[:id])
    render status: :unauthorized, json: { message: 'invalid immunization id' } unless @immunization
  end

  def verify_order
    @order = Order.find_by_id(params[:order_id])
    render status: :unauthorized, json: { message: 'invalid order id' } unless @order
  end

  def verify_item
    @item = Item.find_by_id(params[:item_id])
    render status: :unauthorized, json: { message: 'invalid item id' } unless @item
  end

  def verify_add_admission
    @add_admission = AddAdmission.find_by_id(params[:add_admission_id])
    render status: :unauthorized, json: { message: 'invalid add_admission id' } unless @add_admission
  end

  def verify_order_id
    @order = Order.find_by_id(params[:id])
    render status: :unauthorized, json: { message: 'invalid order id' } unless @order
  end

  def verify_store
    @store = Store.find_by_id(params[:store_id])
    render status: :unauthorized, json: { message: 'invalid store' } unless @store
  end
  def verify_visit
    if params[:visit_id]
      @visit = Visit.find_by_id(params[:visit_id])
      render status: :unauthorized, json: { message: 'invalid visit id' } unless @visit
    end
  end
  def current_inventory
    arr = []
    medical_unit_id = params[:medical_unit_id] || nil
    store_id = params[:store_id] || nil
    department_id = params[:department_id] || nil
    arr << medical_unit_id << store_id << department_id
  end
  def fetch_ward
    @ward = Ward.find_by_id(params[:ward_id])
    render status: :unauthorized, json: { message: 'invalid ward id' } unless @ward
  end
  def fetch_bay
    @bay = Bay.find_by_id(params[:bay_id])
    render status: :unauthorized, json: { message: 'invalid bay id' } unless @bay
  end
  def fetch_bed
    @bed = Bed.find_by_id(params[:bed_id])
    render status: :unauthorized, json: { message: 'invalid bed id' } unless @bed
  end
  def fetch_bed_allocation
    @bed_allocation = BedAllocation.find_by_id(params[:bed_allocation_id])
    render status: :unauthorized, json: { message: 'invalid bed allocation id' } unless @bed_allocation
  end
  def fetch_admission
    @admission = Admission.find_by_id(params[:admission_id])
    render status: :unauthorized, json: { message: 'invalid admission id' } unless @admission
  end
  def fetch_user_departments
    @user_departments = UserDepartment.where(department_id: params[:department_id])
    render status: :unauthorized, json: { message: 'invalid admission id' } unless @user_departments
  end
end
