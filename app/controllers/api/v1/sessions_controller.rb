# class documention
class Api::V1::SessionsController < Devise::SessionsController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' } # rubocop:disable Metrics/LineLength

  skip_before_filter :verify_signed_out_user
  respond_to :json

  def create # rubocop:disable Metrics/MethodLength
    user = User.find_by(username: (params[:user][:username]).downcase)
    if user
      if user.valid_password?(params[:user][:password])
        user.ensure_authentication_token
        user.save
        render status: :ok, json: user
      else
        render status: :unprocessable_entity,
               json: { message: 'Invalid Password' }
      end
    else
      render status: :not_found, json: { message: 'Username does not exist' }
    end
  end

  def destroy
    user = User.find_by(authentication_token: params[:auth_token])
    if user
      user.update_column(:authentication_token, nil)
      render status: :ok, json: { message: 'Logged out' }
    else
      render status: :unprocessable_entity, json: { message: 'Invalid Token' }
    end
  end

  def failure
    render status: :unprocessable_entity, json: { message: 'Login failed' }
  end
end
