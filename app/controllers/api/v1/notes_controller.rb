# class documention
class Api::V1::NotesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:previous_note,:logs]
  before_action :fetch_visit, only: [:create,:previous_note,:logs]
  before_action :fetch_patient, only: [:create]
  before_action :fetch_medical_unit, only: [:create]
  before_action :verify_device, only: [:create,:previous_note,:logs]

  respond_to :json

  def create
    note = Note.new(note_params)
    note.patient_diagnoses.map{ |pt| pt.visit = @visit; pt.user = @current_user; pt.patient = @patient }
    note.visit= @visit
    note.medical_unit = @current_medical_unit
    note.device = @current_device
    note.user = @current_user
    @patient.set_medical_history(params[:note][:med_history]) if params[:note][:med_history].present?
    @patient.set_surgical_history(params[:note][:surgical_history]) if params[:note][:surgical_history].present?
    if note.save
      render status: :ok, json: note
    else
      render status: :unprocessable_entity, json: { message: note.errors.full_messages }
    end
  end

  def previous_note
    note = Note.where(visit_id: @visit.id, department: params[:department]).order("created_at DESC").limit(1).first
    if note.present?
      render status: :ok, json: note, root: "note"
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def logs
    notes = @visit.notes.where(type: params[:type],created_at: params[:start_date]..params[:end_date])
    if notes.present?
      render status: :ok, json: notes
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def note_params
    params.require(:note).permit(:department,:chief_complaint,:med_history,:surgical_history,
                                 :physical_exam,:instructions,:plan,:type,:subjective,:operation_date,
                                 :procedure_indications,:surgeon_assistant_note,:procedure_description,
                                 :anesthesia,:complications,:specimens,:blood_loss,:disposition,
                                  patient_diagnoses_attributes: [:code,:name,:type,:plan,:instructions],
                                  medicines_attributes: [:order_id,:brand_drug_id,:quantity_prescribed,
                                  :frequency,:frequency_unit,:issued_brand_drug_id,:notes,:duration,
                                  :duration_unit,:type,:quantity,:prn,:route ],
                                  procedures_attributes: [:procedure,:option,:left,:right,:instructions],
                                  consultations_attributes: [:department,:description],
                                  addendums_attributes: [:historical_note])
  end
end
