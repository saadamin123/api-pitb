# class documention
class Api::V1::VisitsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:index,:update]
  before_action :verify_device, only: [:create,:index,:update]
  before_action :fetch_patient, only: [:create,:index,:update]
  # before_action :verify_device_medical_unit, only: [:create,:update]
  before_action :fetch_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"

  respond_to :json

  def create
    @visit = @patient.visits.new(visit_params)
    @visit.created_by = @current_user
    # @visit.medical_unit = @current_device.assignable
    @visit.medical_unit = @current_medical_unit

    # @twilio_sms_client = Twilio::REST::Client.new(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    if @visit.save
      #@visit.create_activity key: 'visit.create', owner: @current_user
      # begin
      #   @twilio_sms_client.messages.create(from: TWILIO_NUMBER,
      #                                      to: @patient.phone_in_format, body: "#{@patient.full_name.split.map(&:capitalize)*' '} \n#{@visit.medical_unit.title} \nReg ID: #{@patient.mrn} \n دعا گو محکمه صحت پنجاب ")
      # rescue Exception => e
      # end
      render status: :ok, json: @visit
    else
      render status: :unprocessable_entity,
             json: { message: @visit.errors.full_messages }
    end
  end

  def index
    visits = @patient.visits.where('created_at >= ? AND created_at::date <= ?', params[:start_date], params[:end_date])
    if visits.present?
      render status: :ok, json: visits
    else
      render status: :not_found, json: { message: "Record not found"}
    end
  end

  def update
    @visit = @patient.visits.last if params[:id] == "undefined"
    @visit = @patient.visits.find_by_id(params[:id]) if @visit.nil?
    if @visit && @visit.update_attributes(visit_params)
      #@visit.update_activity key: 'visit.update', owner: @current_user
      @patient.visits.update_all(current_referred_to: params[:visit][:current_referred_to],
                                 current_ref_department: params[:visit][:current_ref_department]) if  params[:visit][:current_referred_to] || params[:visit][:current_ref_department]
      render status: :ok, json: @visit
    else
      render status: :unprocessable_entity, json: { message: @visit.errors.full_messages }
    end
  end

  private

  def visit_params
    params.require(:visit).permit(:reason, :reason_note, :referred_to,
                                  :ref_department, :ref_department_note,
                                  :mode_of_conveyance, :mode_of_conveyance_note,
                                  :amount_paid,:current_ref_department,
                                  :current_referred_to, :ambulance_number,:driver_name,
                                  :mlc, :policeman_name, :belt_number, :police_station_number, :is_active)
  end
end
