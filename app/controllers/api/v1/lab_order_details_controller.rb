# class documention
class Api::V1::LabOrderDetailsController < ApplicationController
  before_action :verify_authenticity_token, only: [:update]
  before_action :verify_device, only: [:update]
  before_action :verify_order, only: [:update]

  respond_to :json

  def update
    lab_order_detail = @order.lab_order_details.first
    if lab_order_detail.update_attributes(lab_order_detail_params)
      order = Order.find_by(id: params[:order_id])
      order.update_attributes(status: 'Collected') if lab_order_detail.sample_collected == true
      render status: :ok, json: lab_order_detail
    else
      render status: :unprocessable_entity,
             json: { message: lab_order_detail.errors.full_messages }
    end
  end

  private

  def lab_order_detail_params
    params.require(:lab_order_detail).permit(:note,:sample_collected, :detail, :instructions, :status)
  end
end
