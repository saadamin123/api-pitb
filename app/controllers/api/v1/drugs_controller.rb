# class documention
class Api::V1::DrugsController < ApplicationController
  before_action :verify_device, only: [:index,:search]
  respond_to :json
  def index
    begin
      File.open(Rails.public_path.join('uploads/data/drugs.csv'), 'w') do |f|
        Drug.select("id","name","category").copy_to do |line|
          begin
            f.write line
          rescue
            new_line = line.force_encoding('UTF-8')
            f.write new_line
          end
        end
      end
      render status: :ok, json: { message: "#{request.base_url}"+"/uploads/data/drugs.csv" }      
    rescue Exception => e
      render status: :unprocessable_entity, json: "#{e.message}"
    end
  end

  def search
    drugs = Drug.arel_table
    possible_drugs = Drug.where(drugs[:name].matches("#{params[:query]}%"))
    if possible_drugs.present?
      render status: :ok, json: possible_drugs
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end
end