# class documention
class Api::V1::OrdersController < ApplicationController
  before_action :verify_authenticity_token
  before_action :verify_device, only: [:search_consultation_patients, :search_admission_patients,:index,:create,:radiology_order, :nursing_order,:procedure_order,:lab_order,:lab_history,:radiology_history, :update, :orders_history]
  before_action :fetch_medical_unit, only: [:search_consultation_patients, :search_admission_patients,:index,:create,:radiology_order, :nursing_order,:procedure_order,:lab_order,:orders_history,:lab_history_for_patient]
  before_action :verify_device_role_medical_unit, only: [:search_consultation_patients, :search_admission_patients,:index] if Rails.env != "test"
  before_action :fetch_patient, only: [:radiology_order, :nursing_order,:procedure_order,:lab_order]
  before_action :fetch_visit, only: [:radiology_order, :nursing_order,:order_logs,:procedure_order,:lab_order]
  before_action :verify_patient, only: [:index,:create, :lab_history_for_patient]
  before_action :verify_order_id, only: [:update]
  before_action :verify_visit, only: [:create]
  serialization_scope :current_inventory
  serialization_scope :current_user
   require 'barby'
   require 'barby/barcode/code_128'
   require 'barby/outputter/html_outputter'
   require 'barby/outputter/png_outputter'
   require 'barby/outputter/cairo_outputter'

  # serialization_scope :current_user
  respond_to :json
  def index
    @orders = Order.get_orders(params,@current_medical_unit,@patient)
    if @orders.present?
      render status: :ok, json: @orders
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  
  def search_admission_patients
    orders = Order.joins(:admission_order_details).where('admission_order_details.category =? AND admission_order_details.department =? AND orders.status != (?)', params[:category], params[:department], 'Closed')
    if orders.present?
      render status: :ok, json: orders
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def search_consultation_patients
    orders = Order.joins(:consultation_order_details).where('consultation_order_details.department =? AND orders.status != (?)', params[:department], 'Closed')
    if orders.present?
      render status: :ok, json: orders
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def create
    order = @current_medical_unit.orders.new(order_params)
    @lab_order_sequence = LabOrderSequence.where(medical_unit_id: current_medical_unit.id).first
    if @lab_order_sequence.blank?
      # @sequence = LabOrderSequence.get_lab_sequence(0000)
      @lab_order_sequence = LabOrderSequence.create(medical_unit_id: current_medical_unit.id, lab_sequence: "0001")
      sequence = @lab_order_sequence.lab_sequence
    else
      $redis.multi
      sequence = LabOrderSequence.get_lab_sequence(@lab_order_sequence.lab_sequence)
      $redis.exec
    end
    formated_sequence = format('%04d',sequence).to_s
    @date = Date.today.strftime("%y") + Date.today.strftime("%m") + Date.today.strftime("%d")
    @lab_order_id = format('%03d',current_medical_unit.id).to_s + @date.to_s + formated_sequence
    $redis.multi
    @lab_order_sequence.lab_sequence = formated_sequence
    @lab_order_sequence.save
    order.lab_order_sequence_number = @lab_order_id
    $redis.exec

    case_no = order.generate_case_number
    mrn = @patient.mrn
    # full_path = "/home/rails/api-pitb/public/barcode_#{case_no}.png"
    # full_path = Rails.root.join("public/","barcode_#{case_no}.png")
    # barcode = Barby::Code128.new(case_no)
    # File.open(full_path, 'w') { |f| f.write barcode.to_png }
    order.device = @current_device
    order.doctor_id = current_user.id
    order.patient = @patient if params[:patient_id].present?
    order.visit= @patient.visits.last if @patient.visits.present?
    if order.admission_order_details.present?
      order.visit.current_referred_to = order.admission_order_details.first.category
      order.visit.current_ref_department = order.admission_order_details.first.department
      order.visit.referred_to = order.admission_order_details.first.category
      order.visit.ref_department = order.admission_order_details.first.department
      order.visit.save
    end
    order.lab_order_details.each { |test| test.order_generator_id = current_user.id }
    if order.save
      # order.lab_order_details.invoice_item.create()
      render status: :ok, json: order      
    else
      render status: :unprocessable_entity, json: { message: order.errors.full_messages }
    end
  end
  def radiology_order
    begin
      radiology_orders = []
      params.require(:radiology_order_details).each do |item|
        order = @current_medical_unit.orders.new(order_params)
        order.device = @current_device
        order.doctor_id = current_user.id
        order.patient = @patient
        order.visit= @visit
        order.save
        order.create_radiology_order_detail(item.permit(:imaging_type,:area,:contrast_option,:other_option,
                                                        :routine,:urgent,:left,:right,:instructions,:result))
        radiology_orders << order
      end
      render status: :ok, json: radiology_orders
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end
  def procedure_order
    begin
      procedure_orders = []
      params.require(:procedure_order_details).each do |item|
        order = @current_medical_unit.orders.new(order_params)
        order.device = @current_device
        order.doctor_id = current_user.id
        order.patient = @patient
        order.visit= @visit
        order.save
        order.create_procedure_order_detail(item.permit(:procedure,:option,:left,:right,:instructions))
        procedure_orders << order
      end
      render status: :ok, json: procedure_orders
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end
  def lab_order
    begin
      lab_orders = []
      params.require(:lab_order_details).each do |item|
        order = @current_medical_unit.orders.new(order_params)
        order.device = @current_device
        order.doctor_id = current_user.id
        order.patient = @patient
        order.visit= @visit
        order.save
        order.lab_order_details.new(item.permit(:test,:detail,:sample_collected,:instructions))
        lab_orders << order
      end
      render status: :ok, json: lab_orders
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end
  def nursing_order
    begin
      nursing_orders = []
      params.require(:nursing_order_details).each do |item|
        order = @current_medical_unit.orders.new(order_params)
        order.device = @current_device
        order.doctor_id = current_user.id
        order.patient = @patient
        order.visit= @visit
        order.save
        order.create_nursing_order_detail(item.permit(:department, :instructions))
        nursing_orders << order
      end
      render status: :ok, json: nursing_orders
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end
  def order_logs
    order_type = params[:order_type]
    logs = @visit.order_logs(order_type)
    if logs.present?
      render status: :ok, json: logs     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def lab_history_for_patient
    lab_order_details = LabOrderDetail.joins(:order).where('orders.medical_unit_id =  ? AND orders.patient_id = ? ',current_medical_unit.id,@patient.id).uniq.order(:created_at)
    
    if lab_order_details.present?
      render status: :ok, json: lab_order_details     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def lab_history
    if params[:type] == 'All'
      orders = Order.joins(:lab_order_details).where('orders.created_at >=  (?) AND orders.created_at::date <= (?) AND patient_id = ?',params[:start_date],params[:end_date],params[:patient_id]).uniq
    elsif params[:type] != 'All'
      orders = Order.joins(:lab_order_details).where('lab_order_details.test = (?) AND orders.created_at >=  (?) AND orders.created_at::date <= (?) AND patient_id = ?',params[:type],params[:start_date],params[:end_date],params[:patient_id]).uniq
    end
    if orders.present?
      render status: :ok, json: orders     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def radiology_history
    orders = Order.joins(:radiology_order_detail).where('radiology_order_details.imaging_type = (?) AND radiology_order_details.area = (?) AND orders.created_at >=  (?) AND orders.created_at::date <= (?) ',params[:type],params[:area],params[:start_date],params[:end_date])
    if orders.present?
      render status: :ok, json: orders     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def update
    order = Order.find_by(id: params[:id])
    order.update_attributes!(order_params)
    order.lab_order_details.first.update_attributes(sample_collected: false, status: order.status) unless order.status == "Pending"
    # if order.lab_order_details.present?
    #   order.lab_order_details.set_status_on_sample_collected
    #   order.lab_order_detail.update_attributes(sample_collected: true) if order.status == "Pending"
    # end
    if order.present?
      render status: :ok, json: order    
    else
      render status: :unprocessable_entity,
             json: { message: @complaint.errors.full_messages }
    end
  end

  def orders_history
    orders = Order.where('medical_unit_id = (?) AND created_at >=  (?) AND created_at::date <= (?) AND department_id = (?)',@current_medical_unit.id,params[:start_date],params[:end_date],params[:department_id])
    if orders.present?
      render status: :ok, json: orders     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def order_barcode
    @order = Order.find_by_id(params[:id])
    @patient = Patient.find_by_id(@order.patient_id)
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'Order Barcode', template: 'api/v1/order_barcode.pdf.haml', user_style_sheet: 'card', page_height: '1in', page_width: '1.5in', margin: {top: '1mm', bottom: '1mm', left: '1mm', right: '1mm'}
      end
    end
  end

  private

  def order_params
    params.require(:order).permit(:status,:updated_by,:order_type,:department_id,
            :department,:category,:cancel_reason,:cancel_reason_details,:store_id,:external_medicine,
            order_items_attributes: [:order_id,:brand_drug_id,:quantity_prescribed,
            :frequency,:frequency_unit,:issued_brand_drug_id,:notes,:duration,
            :duration_unit,:type,:quantity,:prn,:route,:dosage,:dosage_unit,:strength ],
            admission_order_details_attributes: [:id, :order_id, :admission_type, :department, :category, :instructions],
            consultation_order_details_attributes: [:id, :order_id,:department,:description],
            lab_order_details_attributes: [:order_id, :test, :lab_investigation_id, :detail, :sample_collected,
            :instructions, :note,
            invoice_item_attributes: [:invoice_id, :assignable_id, :assignable_type, :invoice_type,
            :amount, :description]],
            invoice_attributes: [:assignable_id, :user_id, :patient_id, :medical_unit_id, :visit_id, :assignable_type, :total_amount,
            :discount_percentage,:discount_value, :total_discount, :payable, :paid])    
  end
end

