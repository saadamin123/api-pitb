# class documention
class Api::V1::FollowUpsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :update]
  before_action :verify_device, only: [:create, :update]
  before_action :fetch_patient, only: [:create, :update, :index]
  before_action :verify_device_medical_unit, only: [:create, :update]
  before_action :verify_device_role_medical_unit, only: [:create, :update] if Rails.env != "test"
  before_action :fetch_medical_unit, only: [:create, :update]
  before_action :fetch_visit, only: [:create, :update]

  # serialization_scope :fetch_medical_unit
  # serialization_scope :current_user
  respond_to :json

  def create
    @follow_up = @patient.follow_ups.new(follow_up_params)
    @follow_up.user = @current_user
    @follow_up.medical_unit = @current_medical_unit
    @follow_up.visit = @visit
    if @follow_up.save
      render status: :ok, json: @follow_up
    else
      render status: :unprocessable_entity,
             json: { message: @follow_up.errors.full_messages }
    end
  end

  def index
    if params[:from] && params[:to]
      @follow_ups = FollowUp.where("medical_unit_id = ? AND user_id  = ? AND follow_up >= ? AND follow_up <= ?",params[:medical_unit_id], params[:user_id], from_date, to_date)
    else 
      @follow_ups = @patient.follow_ups
    end
    if @follow_ups.present?
      render status: :ok, json: @follow_ups
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def follow_up_params
    params.require(:follow_up).permit(:follow_up)
  end
end
