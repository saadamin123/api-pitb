# class documention
class Api::V1::FeedbacksController < ApplicationController
  before_action :verify_authenticity_token, only: [:index, :create]
  before_action :verify_device, only: [:create]
  before_action :fetch_complaint, only: [:index, :create]
  before_action :fetch_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  
  respond_to :json

  def index
    feedback = @complaint.feedbacks
    if feedback.present?
      render status: :ok, json: feedback
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def create
    @feedback = @complaint.feedbacks.new(feedback_params)
    @feedback.user = @current_user
    @feedback.medical_unit = @current_medical_unit
    @feedback.device = @current_device

    if @feedback.save
      render status: :ok, json: @feedback
    else
      render status: :unprocessable_entity,
             json: { message: @feedback.errors.full_messages }
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:satisfaction, :message, :status)
  end
end
