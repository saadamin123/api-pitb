# class documention
class Api::V1::ComplaintsController < ApplicationController
  before_action :verify_authenticity_token, only: [:index, :create, :update, :show]
  before_action :verify_device, only: [:create, :update]
  before_action :fetch_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create, :update] if Rails.env != "test"

  serialization_scope :current_user

  respond_to :json

  def index
    medical_units = current_user.medical_units
    params[:high_priority] ||= false
    if params[:created_by] == 'me' 
      @complaints = @current_user.complaints.where('status = ?',
                              params[:status]) if params[:status]
      @complaints = @current_user.complaints.unresolved_complaints if params[:status] == 'all'
    elsif params[:created_by] == 'other' && medical_units.present? 
      @complaints = Complaint.where("user_id <> ? AND status = ? AND medical_unit_id in (?) AND high_priority = ?", current_user.id, params[:status], medical_units.pluck(:id), params[:high_priority])  if params[:status]
      @complaints = Complaint.unresolved_complaints.where("user_id <> ? AND medical_unit_id in (?) AND high_priority = ?", current_user.id, medical_units.pluck(:id), params[:high_priority])  if params[:status] == 'all'
    elsif params[:created_by] == 'other'
        @complaints = Complaint.where('user_id <> ? AND status = ? AND high_priority = ?',
                              current_user.id, params[:status], params[:high_priority]) if params[:status]
        @complaints = Complaint.unresolved_complaints.where('user_id <> ? AND high_priority = ?', current_user.id, params[:high_priority]) if params[:status] == 'all'
    end
    if @complaints.present?
      render status: :ok, json: @complaints
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def create
    @complaint = Complaint.new(complaint_params)
    @complaint.user = @current_user
    @complaint.medical_unit = @current_medical_unit
    @complaint.device = @current_device
    if @complaint.save
      JSON.parse(params[:complaint][:photo]).each do |photo|
        picture = Picture.find(photo)
        picture.update_attributes(imageable_type: @complaint.class.name, imageable_id: @complaint.id) if picture
      end
      render status: :ok, json: @complaint
      # UserMailer.send_email_to_complainer(@complaint.user).deliver
    else
      render status: :unprocessable_entity,
             json: { message: @complaint.errors.full_messages }
    end
  end

  def update
    @complaint = Complaint.find_by_id(params[:id])
    # @twilio_sms_client = Twilio::REST::Client.new(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    if @complaint.update_attributes(complaint_params)
      if @complaint.critical == true
        # begin
        # @twilio_sms_client.messages.create(from: TWILIO_NUMBER,
        #                                    to: @current_user.phone_in_format, body: "Your Complaint no. #{@complaint.id} has been mark Critical")
        # rescue Exception => e
        # end
      end
      render status: :ok, json: @complaint
    else
      render status: :unprocessable_entity,
             json: { message: @complaint.errors.full_messages }
    end
  end

   def show
    @complaint = @current_user.complaints.find_by_id(params[:id])
    if @complaint.present?
      render status: :ok, json: @complaint
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def complaint_params
    params.require(:complaint).permit(:category, :area, :description,
                                      :severity_level, :show_identity,
                                      :status, :assign_to_role, :critical,
                                      :high_priority)
  end
end
