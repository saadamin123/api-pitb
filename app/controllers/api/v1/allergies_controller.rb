# class documention
class Api::V1::AllergiesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :destroy]
  before_action :verify_device, only: [:create]
  before_action :fetch_patient, only: [:create, :index, :destroy]
  before_action :verify_device_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  before_action :fetch_visit, only: [:create]
  before_action :fetch_medical_unit, only: [:create]

  serialization_scope :fetch_medical_unit
  serialization_scope :current_user
  respond_to :json

  def create
    @allergy = @patient.allergies.new(allergy_params)
    @allergy.user = @current_user
    @allergy.visit = @visit
    @allergy.medical_unit = @current_medical_unit

    if @allergy.save
      render status: :ok, json: @allergy
    else
      render status: :unprocessable_entity,
             json: { message: @allergy.errors.full_messages }
    end
  end

  def index
    @allergies = @patient.allergies
    if @allergies.present?
      render status: :ok, json: @allergies
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def destroy
    @allergy = @patient.allergies.find_by_id(params[:id])
    if @allergy.present?
      @allergy.destroy
       render status: :ok, json: { message: 'Successfully deleted' }
    else
      render status: :unprocessable_entity, json: { message: 'Invalid Request' }
    end
  end

  private

  def allergy_params
    params.require(:allergy).permit(:allergy_type, :value)
  end
end
