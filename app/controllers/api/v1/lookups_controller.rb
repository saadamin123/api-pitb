# class documention
class Api::V1::LookupsController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
    @lookups = Lookup.where('category = ?', params[:category]) if params[:category] # rubocop:disable Metrics/LineLength
    @lookups = Lookup.all if params[:category] == 'all'
    if @lookups.present?
      render status: :ok, json: @lookups
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
end
