# class documention
class Api::V1::LeavesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:index]
  before_action :verify_device, only: [:create]
  before_action :fetch_medical_unit, only: [:create,:index]

  respond_to :json

  def create
    @leave = @current_user.leave.new(leave_params)
    @leave.map {|p| p.user = @current_user;p.medical_unit_id = @current_medical_unit.id} 
    if  @leave.map {|p| p.save }
      render status: :ok, json: @leave
    else
      render status: :unprocessable_entity,
             json: { message: @leave.errors.full_messages }
    end
  end

  def index
    if params[:attendance] == 'All'
      @leaves = Leave.where('medical_unit_id = (?) AND date >=  (?) AND date::date <= (?)',@current_medical_unit.id,params[:start_date],params[:end_date])
    elsif params[:attendance] != 'All'
      @leaves = Leave.where('attendance = (?) AND medical_unit_id = (?) AND date >=  (?) AND date::date <= (?)',params[:attendance],@current_medical_unit.id,params[:start_date],params[:end_date])
    end
    if @leaves.present?
      render status: :ok, json: @leaves     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def leave_params
    params.require(:leave).map { |m| m.permit(:attendance, :shift, :date)}
  end
end
