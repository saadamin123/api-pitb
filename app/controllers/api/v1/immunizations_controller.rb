# class documention
class Api::V1::ImmunizationsController < ApplicationController
  before_action :verify_authenticity_token, only: [:update, :index, :destroy]
  before_action :verify_device, only: [:update]
  before_action :fetch_patient, only: [:update, :index, :destroy]
  before_action :verify_device_medical_unit, only: [:update]
  before_action :verify_device_role_medical_unit, only: [:update] if Rails.env != "test"
  before_action :fetch_visit, only: [:update]
  before_action :fetch_medical_unit, only: [:update]
  before_action :fetch_immunization, only: [:update, :destroy]

  serialization_scope :fetch_medical_unit
  serialization_scope :current_user
  respond_to :json

  def update
    @immunization = @patient.immunizations.find_by_id(params[:id])
    if @immunization.update_attributes(immunization_params)
      @immunization.user = @current_user
      @immunization.medical_unit = @current_medical_unit
      @immunization.visit = @visit
      @immunization.save
      render status: :ok, json: @immunization
    else
      render status: :unprocessable_entity,
             json: { message: @immunization.errors.full_messages }
    end
  end

  def index
    @immunizations = @patient.immunizations.order(id: :asc)
    if @immunizations.present?
      render status: :ok, json: @immunizations
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def immunization_params
    params.require(:immunization).permit(:age, :vaccine, :dose, :due, :given_on, :location)
  end
end
