# class documention
class Api::V1::LineItemsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create]
  before_action :verify_device, only: [:create]
  before_action :fetch_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  # before_action :check_for_batch, only: [:create]
  serialization_scope :fetch_medical_unit
  respond_to :json
  def create
    brand_drug_id = params[:line_item][:brand_drug_id]
    store_id = params[:store_id] || nil
    department_id = params[:department_id] || nil
    inventory = Inventory.where(medical_unit_id: params[:medical_unit_id], brand_drug_id: brand_drug_id, store_id: store_id, department_id: department_id).first_or_create
    line_item = inventory.line_items.build(line_item_params)
    check_for_batch(inventory)
    line_item.user_id = @current_user.id
    line_item.device_id = @current_device.id
    line_item.medical_unit_id = @current_medical_unit.id

    line_item.batch_id = @batch.id if @batch
    
    if line_item.save
      render status: :ok, json: line_item
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def all_batches
    inventroy = Inventory.where(brand_drug_id: params[:brand_drug_id], medical_unit_id: params[:medical_unit_id], store_id: params[:store_id])
    if inventroy.first
      render status: :ok, json: inventroy.first.line_items.map{|line_item| line_item.batch}.uniq.compact
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end


  def update_batches

    params[:batches].each do |bat|
      batch = Batch.find(bat[:id])
      batch.update_attribute(:quantity , (batch.quantity - bat[:issued].to_i)) if (batch.quantity > 0) && (batch.quantity > bat[:issued].to_i)
    end
    render status: :ok , json: 'success'
    
    # inventroy = Inventory.where(brand_drug_id: params[:brand_drug_id], medical_unit_id: params[:medical_unit_id], store_id: params[:store_id])
    # if inventroy.first
    #   render status: :ok, json: inventroy.first.line_items.map{|line_item| line_item.batch}.uniq
    # else
    #   render status: :not_found, json: { message: 'Record not found' }
    # end
  end


  private

  def line_item_params
    params.require(:line_item).permit(:medical_unit_id, :brand_drug_id, :quantity,
                                  :category, :user_id, :expiry, :device_id,
                                  :order_type, :status, :description, :reason, :order_id, :order_item_id,
                                  :transaction_type )
  end

  def check_for_batch inventory
    if params[:line_item][:number]
      line_item = inventory.line_items.joins(:batch).where("batches.number = (?)",params[:line_item][:number]).first if inventory && inventory.line_items
      if line_item && line_item.batch
        @batch = line_item.batch
      else 
        @batch = Batch.new(number: params[:line_item][:number])
      end
      @batch.update_attributes(expiry_date: params[:line_item][:expiry])
    elsif params[:batch][:id]
      @batch = Batch.find(params[:batch][:id])
      @batch.update_attributes(expiry_date: params[:batch][:expiry_date])
    end

  end

end

