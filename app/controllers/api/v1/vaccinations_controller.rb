# class documention
class Api::V1::VaccinationsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :update]
  before_action :verify_device, only: [:create, :update]
  before_action :fetch_patient, only: [:create, :update, :index]
  before_action :verify_device_medical_unit, only: [:create, :update]
  before_action :verify_device_role_medical_unit, only: [:create, :update] if Rails.env != "test"
  before_action :fetch_medical_unit, only: [:create, :update]
  before_action :fetch_visit, only: [:create, :update]
  before_action :fetch_vaccination, only: [:update]

  # serialization_scope :fetch_medical_unit
  # serialization_scope :current_user
  respond_to :json

  def create
    @vaccination = @patient.vaccinations.new(vaccination_params)
    @vaccination.user = @current_user
    @vaccination.medical_unit = @current_medical_unit
    @vaccination.visit = @visit
    if @vaccination.save
      render status: :ok, json: @vaccination
    else
      render status: :unprocessable_entity,
             json: { message: @vaccination.errors.full_messages }
    end
  end

  def update
    @vaccination = @patient.vaccinations.find_by_id(params[:id])
    if @vaccination.update_attributes(immunization_params)
      @vaccination.user = @current_user
      @vaccination.medical_unit = @current_medical_unit
      @vaccination.visit = @visit
      @vaccination.save
      render status: :ok, json: @vaccination
    else
      render status: :unprocessable_entity,
             json: { message: @vaccination.errors.full_messages }
    end
  end

  def index
    @vaccinations = @patient.vaccinations
    if @vaccinations.present?
      render status: :ok, json: @vaccinations
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def vaccination_params
    params.require(:vaccination).permit(:name, :given_on, :follow_up, :instruction)
  end
end
