# class documention
class Api::V1::BrandsController < ApplicationController
  before_action :verify_authenticity_token, only: [:search]
  before_action :verify_device, only: [:index,:search]
  respond_to :json

  def index
    begin
      File.open(Rails.public_path.join('uploads/data/brands.csv'), 'w') do |f|
        Brand.select("id","name","category").copy_to do |line|
          begin
            f.write line
          rescue
            new_line = line.force_encoding('UTF-8')
            f.write new_line
          end
        end
      end
      render status: :ok, json: { message: "#{request.base_url}"+"/uploads/data/brands.csv" }     
    rescue Exception => e
      render status: :unprocessable_entity, json: "#{e.message}"
    end
  end

  def search
    brands = Brand.arel_table
    possible_brands = Brand.where(brands[:name].matches("#{params[:query]}%")).where(brands[:category].matches("#{params[:category]}"))
    if possible_brands.present?
      render status: :ok, json: possible_brands
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end
end
