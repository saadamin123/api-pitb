# class documention
class Api::V1::DevicesController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
    @devices = Device.where('status=? AND uuid= ?', 'active', params[:uuid])
    if @devices.present?
      render status: :ok, json: @devices
    else
      render status: :not_found, json: { message: 'Device not found' }
    end
  end
end
