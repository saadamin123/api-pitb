class Api::V1::DepartmentsController < ApplicationController
  before_action :verify_authenticity_token, only: [:index]
  def index
    if params[:store_id] != "undefined"
      @departments = Department.joins(:stores).where('stores.medical_unit_id =? AND stores.id =?', params[:medical_unit_id], params[:store_id]).order('title ASC')
    elsif params[:medical_unit_id].present?
      @departments = Department.where(medical_unit_id: params[:medical_unit_id]).order('title ASC')
    end  
    if @departments.present?
      render status: :ok, json: @departments
    else
      render status: :not_found, json: { message: 'Department not found' }
    end
  end  
end