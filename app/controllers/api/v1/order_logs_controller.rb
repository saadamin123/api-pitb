# class documention
class Api::V1::OrderLogsController < ApplicationController
  before_action :verify_authenticity_token, only: [:index]
  before_action :verify_device, only: [:index]
  before_action :verify_order, only: [:index]

  respond_to :json

  def index
    @order_logs = @order.order_logs
    if @order_logs.present?
      render status: :ok, json: @order_logs
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
end
