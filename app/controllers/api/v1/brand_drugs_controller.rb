# class documention
class Api::V1::BrandDrugsController < ApplicationController
  before_action :verify_authenticity_token, only: [:search,:alternative]
  before_action :verify_device, only: [:search,:alternative]
  before_action :fetch_medical_unit, only: [:search,:alternative]
  serialization_scope :current_inventory
  respond_to :json
  def search
    type = params[:type]
    if params[:id].present? 
      id = params[:id]
      if type == "brand"
        @brand_drugs = BrandDrug.where(brand_id: id).page(params[:page])
      else
        @brand_drugs = BrandDrug.where(drug_id: id).page(params[:page])
      end
    else # view all button
      if params[:category] == "medicine"
        @brand_drugs = BrandDrug.where(category: "medicine").page(params[:page])
      else
        @brand_drugs = BrandDrug.where(category: "supply").page(params[:page])
      end
    end
    if @brand_drugs.present?
      render status: :ok, json: @brand_drugs
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def alternative
    store_id = params[:store_id] || nil
    brand_drug = BrandDrug.find(params[:brand_drug_id])
    @brand_drugs = brand_drug.alternatives(@current_medical_unit,params[:page],store_id)
    if @brand_drugs.present?
      render status: :ok, json: @brand_drugs
    else
      render status: :not_found, json: { message: 'Alternative not found' }
    end
  end
end

