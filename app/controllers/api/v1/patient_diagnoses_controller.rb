# class documention
class Api::V1::PatientDiagnosesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :destroy]
  before_action :verify_device, only: [:create]
  before_action :fetch_patient, only: [:create, :destroy]
  before_action :fetch_visit, only: [:create, :destroy, :logs]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"

  respond_to :json

  def create
    @patient_diagnosis = @visit.patient_diagnoses.new(patient_diagnosis_params)
    @patient_diagnosis.map {|p| p.user = @current_user;p.patient = @patient} 
    if  @patient_diagnosis.map {|p| p.save }
      render status: :ok, json: @patient_diagnosis
    else
      render status: :unprocessable_entity,
             json: { message: @patient_diagnosis.errors.full_messages }
    end
  end

  def destroy
    @patient_diagnoses = @visit.patient_diagnoses.where(:id => params[:id].split(','))
    if @patient_diagnoses.present?
      @patient_diagnoses.destroy_all
      render status: :ok, json: { message: 'Successfully destroyed' }
    else
      render status: :unprocessable_entity, json: { message: 'Invalid request' }
    end
  end

  def logs
    patient_diagnoses = @visit.patient_diagnoses.where(type: params[:type])
    if patient_diagnoses.present?
      render status: :ok, json: patient_diagnoses
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def patient_diagnosis_params
    params.require(:patient_diagnosis).map { |m| m.permit(:name, :code)}
  end
end
