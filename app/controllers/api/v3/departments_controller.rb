class Api::V3::DepartmentsController < ApplicationController
  before_action :verify_authenticity_token
  before_action :fetch_medical_unit
  before_action :fetch_department,only: [:fetch_doctors]
  before_action :fetch_user_departments,only: [:fetch_doctors]

  respond_to :json

  def index
    user_departments = @current_user.departments
    if user_departments.present?
      render status: :ok, json: user_departments
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def fetch_doctors
    @doctors = MedicalUnitUser.includes(:user).where("user_id in (?) AND role_id in (?)", @user_departments.pluck(:user_id), [4, 24])
    if @doctors.present?
      render status: :ok, json: @doctors
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
end