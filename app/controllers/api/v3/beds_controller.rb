class Api::V3::BedsController < ApplicationController
  # before_action :verify_authenticity_token, only: [:create, :index, :update, :get_patients_by_type,:patient_activity, :csv_data, :unknown_patients]
  before_action :verify_authenticity_token,  only: [:get_count, :index]
  before_action :fetch_medical_unit ,  only: [:get_count, :index]
  before_action :fetch_bay ,  only: [:index]
  before_action :fetch_ward, only: [:get_count]
  respond_to :json

  def index
    if @bay.present?
      beds = @bay.beds.order(:bed_number)
      if beds.present?
        render status: :ok, json: beds
      else
        render status: :not_found, json: { message: 'No Beds in this Bay' }  
      end
    else
      render status: :not_found, json: { message: 'No Bays in this ward' }
    end
  end

  def get_count
    if params[:ward_id].present?
      total = @ward.beds.count
      booked = @ward.bed_allocations.where("status = 'Occupied'").pluck(:bed_id).uniq.count
      free =  total - booked
      # free = @beds.where("status = 'Free'").count
      if total.present?
        render status: :ok, json: { total: total, free: free, booked: booked }
      else
        render status: :not_found, json: { message: 'No Beds in this Ward' }
      end
      # @beds = Bed.where(ward_id: params[:ward_id])
      # total = @beds.count
      # free = @beds.where("status = 'Free'").count
      # if total.present?
      #   render status: :ok, json: { total: total, free: free, booked: total - free }
      # else
      #   render status: :not_found, json: { message: 'No Beds in this Ward' }
      # end
    else
      render status: :not_found, json: { message: 'No ward ID passed as parameter' }
    end
  end
end