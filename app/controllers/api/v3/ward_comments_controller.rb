# class documention
class Api::V3::WardCommentsController < ApplicationController
  before_action :verify_authenticity_token, only: [:index]
  before_action :fetch_admission, only: [:index]
  before_action :fetch_medical_unit, only: [:index]
  before_action :fetch_patient, only: [:index]

  respond_to :json

  def index
    @ward_comment = WardComment.includes(:user).where(admission_id: @admission.id)
      if @ward_comment.present?
        render status: :ok, json: @ward_comment
      else
        render status: :not_found, json: { message: 'Record not found' }
      end
  end

end