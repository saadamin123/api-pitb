# class documention
class Api::V3::UsersController < ApplicationController
  respond_to :json

  def user_default_screen_wise
    role_ids = Role.where(default_screen: params[:default_screen]).pluck(:id)
    if role_ids.present?
      users = User.joins(:medical_unit_users).where('medical_unit_id=? AND users.role_id in (?)', params[:medical_unit_id], role_ids)
      if users.present?
        render status: :ok, json: users
      else
        render status: :not_found, json: { message: 'No user found with this user role' }
      end
    else
      render status: :not_found, json: { message: 'No Role found with this user role' }
    end
  end

end
