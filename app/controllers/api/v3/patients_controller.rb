require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/html_outputter'
require 'barby/outputter/pdfwriter_outputter'
require 'barby/outputter/png_outputter'
require 'barby/outputter/cairo_outputter'
require 'rqrcode'
require 'time'
# class documention
class Api::V3::PatientsController < ApplicationController
  # skip_before_filter  :verify_authenticity_token
  before_action :verify_authenticity_token, only: [:create, :index, :update, :get_patients_by_type,:patient_activity, :csv_data, :unknown_patients,:fetch_admitted_patients,:fetch_opd_patients,:fetch_all_patients,:fetch_all_visits_by_today,:fetch_all_visits,:today_opd,:today_emergency ]
  before_action :verify_device, only: [:create, :update,:patient_activity, :unknown_patients, :fetch_admitted_patients,:fetch_opd_patients]
  before_action :fetch_medical_unit, only: [:create, :index, :update, :get_patients_by_type,:patient_activity, :unknown_patients, :fetch_admitted_patients,:fetch_opd_patients]
  # before_action :fetch_department, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  serialization_scope :current_user
  respond_to :json

  def index # rubocop:disable Metrics/MethodLength
    @patients = Patient.where("mrn = ? OR guardian_nic LIKE ? OR phone1 LIKE ?", "#{params[:query]}", "#{params[:query]}","#{params[:query]}")
    if @patients.present?
      render status: :ok, json: @patients
    else
      render status: :not_found, json: { message: 'Patient not found' }
    end
  end

  def get_patients_by_type
    @patients = Patient.where("#{params[:type]} = ?", params[:query] )
    if @patients.present? && (@patients.first.phone1 != '03000000000' || params[:type] == 'mrn')
      render status: :ok, json: @patients
    else
      render status: :not_found, json: { message: 'Patient not found' }
    end
    # @medicines = MedicineRequest.joins(:patient).where("mrn = ? AND status =?", @patients.first.mrn, "Open")
    # if @medicines.present?
      
    # else
    #   if @patients.present? && (@patients.first.phone1 != '03000000000' || params[:type] == 'mrn')
    #     render status: :ok, json: @patients
    #   else
    #     render status: :not_found, json: { message: 'Patient not found' }
    #   end
    # end
  end

  def create
    @patient = Patient.new(patient_params)
    @patient.registered_by = @current_user.id
    @patient.registered_at = @current_medical_unit
    @patient.generate_new_mrn(@current_medical_unit)
    if @patient.save
      # @visit = @patient.visits.new(referred_to: params[:patient][:referred_to], current_referred_to: params[:patient][:referred_to])
      @visit = Visit.new_visit(@patient,params[:patient])
      @visit.created_by = @current_user
      @visit.medical_unit = @current_medical_unit
      @visit.department_id = params[:department_id] if params[:department_id]
      @visit.doctor_id = params[:doctor_id] if params[:doctor_id]
      @visit.save
      @visit.create_activity key: 'visit.create', owner: @current_user, parameters: {id: @visit.id,medical_unit_id:@visit.medical_unit_id,
      patient_id: @visit.patient_id, user_id: @visit.user_id, visit_number: @visit.visit_number,
      reason: @visit.reason,reason_note: @visit.reason_note,ref_department: @visit.ref_department,
      ref_department_note: @visit.ref_department_note,mode_of_conveyance: @visit.mode_of_conveyance,
      amount_paid: @visit.amount_paid,current_ref_department: @visit.current_ref_department,
      current_referred_to: @visit.current_referred_to,is_active: @visit.is_active,
      ambulance_number: @visit.ambulance_number,driver_name: @visit.driver_name,mlc: @visit.mlc,
      policeman_name: @visit.policeman_name,belt_number: @visit.belt_number,police_station_number: @visit.police_station_number,
      department_id: @visit.department_id,doctor_id: @visit.doctor_id }

      # @visit = @visit.update_attributes(mode_of_conveyance: params[:patient][:mode_of_conveyance],
      #                                   ambulance_number: params[:patient][:ambulance_number],
      #                                   driver_name: params[:patient][:driver_name]) if params[:patient][:mode_of_conveyance] || params[:patient][:ambulance_number] || params[:patient][:driver_name]
      render status: :ok, json: @patient
    else
      render status: :unprocessable_entity, json: { message: @patient.errors.full_messages }
    end
  end

  def update
    @patient = Patient.find_by_id(params[:id])
    if @patient.update_attributes(patient_update_params)
      if params[:patient][:ward].present?
        @visit = @patient.visits.new(referred_to: params[:patient][:referred_to], 
                                    ref_department: params[:patient][:ward])
         @visit.created_by = @current_user
         @visit.medical_unit = @current_medical_unit
         @visit.save
      elsif params[:patient][:new_referred_to].present?
        @visit = @patient.visits.new(referred_to: params[:patient][:new_referred_to],
                                     current_referred_to: params[:patient][:new_referred_to])
        @visit.department_id = params[:patient][:department_id] if params[:patient][:department_id].present?
        @visit.created_by = @current_user
        @visit.medical_unit = @current_medical_unit
        @visit.save
      end
      render status: :ok, json: @patient
    else
      render status: :unprocessable_entity, json: { message: @patient.errors.full_messages }
    end
  end

  def generate_prescription_slip_pdf
    @patient = Patient.find_by_id(params[:id])
    # @patient_age = age(@patient.birth_year, @patient.birth_month,
    #                @patient.birth_day).split[0...4].join(' ')
    pdf = render :pdf => "generate_order_pdf", :layout => 'pdf.html.haml'
    save_path = Rails.root.join('public','filename.pdf')
    File.open(save_path, 'wb') do |file|
      file << pdf
    end
    system("lpr", "public/filename.pdf")
  end

  def print
     @duplicate = params[:print_type].present?
    @patient = Patient.find_by_id(params[:id])
  end

  def csv_data
    @current_user.medical_units.first
    csv_data = Patient.patient_csv_data_with_date(params[:start_date], params[:end_date], @current_user.medical_units.first.id)
    csv = Patient.create_csv(csv_data)
    send_data csv.encode(), type: 'text/csv; header=present', disposition: "attachment; filename=report.csv"
  end
  # def emergency_slip
  #   @duplicate = params[:print_type].present?
  #   @patient = Patient.find_by_id(params[:id])
  # end
  def adult_opd_slip
    @duplicate = params[:print_type].present?
    @patient = Patient.find_by_id(params[:id])
    @doctor = User.where(id:@patient.visits.last.doctor_id) if !(@patient.visits.last.doctor_id.blank?)
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Adult OPD Slip" , page_size: 'A4', margin: {top: '5mm', bottom: '5mm', left: '5mm', right: '5mm'}  # Excluding ".pdf" extension.
      end
    end
  end

  def adult_emergency_slip
    @duplicate = params[:print_type].present?
    @patient = Patient.find_by_id(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Adult EMERGENCY Slip" , page_size: 'A4', margin: {top: '5mm', bottom: '5mm', left: '5mm', right: '5mm'}  # Excluding ".pdf" extension.
      end
    end
  end

  def non_adult_emr_slip
    @duplicate = params[:print_type].present?
    @patient = Patient.find_by_id(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "NON Adult EMERGENCY Slip" , page_size: 'A4', margin: {top: '5mm', bottom: '5mm', left: '5mm', right: '5mm'}  # Excluding ".pdf" extension.
      end
    end
  end

  def prescription_slip
    @duplicate = params[:print_type].present?
    @patient = Patient.find_by_id(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Prescription Slip" , page_size: 'A4', margin: {top: '5mm', bottom: '5mm', left: '5mm', right: '5mm'}  # Excluding ".pdf" extension.
      end
    end
  end

  def print_health_card
    @patient = Patient.find_by_id(params[:id])
    date = @patient.birth_month.to_s() +"-"+ @patient.birth_day.to_s() + "-" + @patient.birth_year.to_s()
    # @patient.age = date.strftime("%B")
    # @patient.age = Date.strptime(date, '%d-%m-%Y')
    @patient_age = Date.strptime((date).gsub("/","-"), "%m-%d-%Y")
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'Health Card', user_style_sheet: 'card', page_height: '54mm', page_width: '86mm', margin: {top: '1mm', bottom: '1mm', left: '1mm', right: '1mm'}
      end
    end
  end

  def print_wrist_band
    @patient = Patient.find_by_id(params[:id])
    date = @patient.birth_month.to_s() +"-"+ @patient.birth_day.to_s() + "-" + @patient.birth_year.to_s()
    # @patient.age = date.strftime("%B")
    # @patient.age = Date.strptime(date, '%d-%m-%Y')
    @patient_age = Date.strptime((date).gsub("/","-"), "%m-%d-%Y")
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'Wrist Band', user_style_sheet: 'card', page_height: '1in', page_width: '11in', margin: {top: '1mm', bottom: '1mm', left: '1mm', right: '1mm'}
      end
    end
  end

  def admission_slip
    @duplicate = params[:print_type].present?
    @patient = Patient.find_by_id(params[:id])
    @patient_admission = @patient.admissions.last
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Admission Slip" , page_size: 'A4', margin: {top: '5mm', bottom: '5mm', left: '5mm', right: '5mm'}  # Excluding ".pdf" extension.
      end
    end
  end
  
  # def humanize secs
  #   [ 
  #     [60, :seconds], 
  #     [60, :minutes], 
  #     [24, :hours], 
  #     [365, :days], 
  #     [150, :years]
  #   ].map do |count, name|
  #     if secs > 0
  #       secs, n = secs.divmod(count)
  #       "#{n.to_i} #{name}"
  #     end
  #   end.compact.reverse.join(' ')
  # end
  # def age(y,m,d)
  #   distance = Time.new - Time.parse(y.to_s+"-"+m.to_s+"-"+d.to_s)
  #   humanize(distance)
  # end
  def patient_activity
    @patient_activities = Patient.where(id: params[:patient_id]).first.activities
    if @patient_activities.present?
      render status: :ok, json: @patient_activities
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def unknown_patients
    @unknown_patients = Patient.where(unidentify: true)
    if @unknown_patients.present?
      render status: :ok, json: @unknown_patients
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def get_patient_by_medicnes
    patient =  Patient.where("mrn = ?", params[:mrn]).first
    medicines = patient.item_transactions.group_by { |v| v.created_at.to_date.strftime("%Y-%m-%d") } if patient.present?
    if medicines.present?
      render status: :ok, json: {patient_mrn: patient.mrn, patient_name: patient.full_name,
                                  patient_age: patient.age, patient_gender: patient.gender,
                                  patient_father_name: patient.father_name, patient_location: patient.near_by_city,
                                  date: medicines.keys, medicines: medicines.values.first.map{|m| m.item.product_name}}
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def fetch_admitted_patients
    @patient =  Patient.joins(:admissions).where("mrn ILIKE ? AND medical_unit_id =? AND status =?", "#{params[:query]}%", params[:medical_unit_id], "Admitted")
    if @patient.present?
      render status: :ok, json: @patient
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def fetch_opd_patients
    #@patient =  Patient.joins(:visits).where("medical_unit_id =? AND referred_to =?", params[:medical_unit_id], "Outpatient")
    @patient = Visit.joins(:department).includes(:patient).where("visits.Created_At >= ? AND visits.Created_at <=? AND is_active =? AND visits.medical_unit_id =? AND departments.Opd =?", Time.zone.now.beginning_of_day, Time.zone.now.end_of_day, true, @current_medical_unit.id, true)
    if @patient.present?
      render status: :ok, json: @patient
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def fetch_all_patients
    data = $redis.get('all_patient_count')
    if data.present?
      render status: :ok, json: data
    else
      render status: :not_found, json: { message: "No patient found" }
    end
  end

  def fetch_all_visits
    visit_count = $redis.get('all_visit')
    if visit_count.present?
      render status: :ok, json: visit_count
    else
      render status: :not_found, json: { message: "No visit found" }
    end
  end

  def fetch_all_visits_by_today
    visit_count = $redis.get('total_visit_count')
    if visit_count.present?
      render status: :ok, json: visit_count
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def today_opd
    visit_count = $redis.get('opd_count')
    if visit_count.present?
      render status: :ok, json: visit_count
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def today_emergency
    visit_count =  $redis.get('emr_count')
    if visit_count.present?
      render status: :ok, json: visit_count
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  private

  def patient_params
    params.require(:patient).permit(:first_name, :last_name, :middle_name,
                                  :gender, :education, :birth_day,
                                  :birth_month, :birth_year, :patient_nic,
                                  :patient_passport, :unidentify_patient,
                                  :guardian_relationship,
                                  :guardian_first_name, :guardian_last_name,
                                  :guardian_middle_name, :guardian_nic,
                                  :guardian_passport, :unidentify_guardian,
                                  :marital_status, :state, :city, :near_by_city,
                                  :address1, :address2, :phone1, :phone1_type,
                                  :phone2, :phone2_type, :phone3, :phone3_type,
                                  :age, :father_name, :unidentify_gender, :guardian_state,
                                  :guardian_city, :guardian_near_by_city, :guardian_address,
                                  :guardian_phone_type, :guardian_phone, :unidentify)
  end

  def patient_update_params # rubocop:disable Metrics/MethodLength
    params.require(:patient).permit(:guardian_nic, :address1, :address2, :phone1,
                                    :phone1_type, :phone2, :phone2_type, :father_name,
                                    :guardian_first_name, :guardian_state,
                                    :guardian_city, :guardian_near_by_city, :guardian_address,
                                    :guardian_phone_type, :guardian_phone,:guardian_relationship,
                                    :patient_nic)
  end
end