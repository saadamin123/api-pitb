class Api::V3::BedAllocationsController < ApplicationController
  before_action :verify_authenticity_token, only: [:update, :index, :update_ward, :fetch_bed_allocations_ward_wise,:fetch_beds_ward_wise,:fetch_allocated_beds_ward_wise]
  before_action :fetch_medical_unit, only: [:update, :index, :update_ward,:fetch_bed_allocations_ward_wise,:fetch_beds_ward_wise,:fetch_allocated_beds_ward_wise]
  # before_action :fetch_visit, only: [:update]
  before_action :fetch_ward, only: [:update]
  before_action :fetch_patient, only: [:update]
  before_action :fetch_bay, only: [:update]
  before_action :fetch_bed, only: [:update]
  before_action :fetch_bed_allocation, only: [:update, :update_ward]

  respond_to :json

  def update
    last_bed = @bed_allocation.bed
    if (!last_bed || @bed_allocation.bed != @bed)
      bed_allocation = @bed_allocation.update_attributes(status: params[:bed_allocation][:status])
      # @bed.update_attributes(status: 'Occupied')
      @bed_allocation.user = @current_user
      @bed_allocation.bed = @bed
      @bed.update_attributes(status: 'Occupied')
      @bed_allocation.bay = @bay
      BedAllocation.update_status(last_bed) if last_bed.present?
    end

    if @bed_allocation.save
      WardComment.new_comment(@bed_allocation,params[:bed_allocation][:comment]) if params[:bed_allocation][:comment].present?
      render status: :ok, json: @bed_allocation
    else
      render status: :unprocessable_entity, json: { message: @bed_allocation.errors.full_messages }
    end
  end
  def update_ward
    @ward = Ward.find_by_id(params[:ward_id])
    if params[:bed_allocation][:patient_mrn].present?
      @bed_allocation = Patient.find_by_mrn(params[:bed_allocation][:patient_mrn]).bed_allocations.last
    end
  # @bed.update_attributes(status: 'Occupied')
    if @bed_allocation.bed.present?
      BedAllocation.update_status(@bed_allocation.bed)
      @bed_allocation.bed = nil
      @bed_allocation.bay = nil
      @bed_allocation.update_attributes(status: params[:bed_allocation][:status])
    end
    bed_allocation = @bed_allocation.update_attributes(status: params[:bed_allocation][:status])
    @bed_allocation.ward = @ward if params[:bed_allocation][:status] == 'Transfer'
    if @bed_allocation.save
      @bed_allocation.update_medicine_request_status if params[:bed_allocation][:status] == 'Leave'
      WardComment.new_comment(@bed_allocation,params[:bed_allocation][:comment]) if params[:bed_allocation][:comment].present?
      render status: :ok, json: @bed_allocation
    else
      render status: :unprocessable_entity, json: { message: @bed_allocation.errors.full_messages }
    end
  end
  def index
    # admissions = @ward.admissions
    status = ['Occupied','Null']  
    if params[:type] == "Admitted"
      @ward = Ward.find_by_id(params[:ward_id])
      nobeds = BedAllocation.includes(:patient,:bed, :bay).where("ward_id =? AND status !=? AND bed_id is NULL AND status != 'Leave'", params[:ward_id],'Discharged').order("bed_id asc")
      beds = BedAllocation.includes(:patient,:bed, :bay).where("ward_id =? AND (status !=? AND bed_id is NOT NULL OR status = 'Leave')", params[:ward_id],'Discharged').order("bed_id asc")
      # @admissions = BedAllocation.includes(:patient,:bed, :bay).where("ward_id =? AND status !=?", params[:ward_id],'Discharged').order("bed_id asc")
      @admissions = nobeds + beds
    elsif params[:type] == "All"
      @admissions = BedAllocation.includes(:patient,:bed, :bay, :ward).where("medical_unit_id =?", params[:medical_unit_id])
    end
    if @admissions.present?
      render status: :ok, json: @admissions
    else
      render status: :not_found, json: { message: 'No Admissions found in this ward' }
    end
  end
  def fetch_bed_allocations_ward_wise
    ward_ids =  @current_user.wards
    @bed_allocations_ward_wise = []
    ward_ids.each do |ward_id|
      @bed_allocations_ward_wise << BedAllocation.where(ward_id: ward_id,medical_unit_id: params[:medical_unit_id])
      @bed_allocations_ward_wise.flatten!
    end
    if @bed_allocations_ward_wise.present?
      render status: :ok, json: @bed_allocations_ward_wise
    else
      render status: :not_found, json: { message: 'No Bed Allocation found in this ward' }
    end
  end
  def fetch_beds_ward_wise
    ward_ids =  @current_user.wards
    @beds_ward_wise = []
    ward_ids.each do |ward_id|
      @beds_ward_wise << Bed.where(ward_id: ward_id,medical_unit_id: params[:medical_unit_id]).count
      @beds_ward_wise.flatten!
    end
    if @beds_ward_wise.present?
      render status: :ok, json: @beds_ward_wise
    else
      render status: :not_found, json: { message: 'No Beds found in this ward' }
    end
  end
  def fetch_allocated_beds_ward_wise
    ward_ids =  @current_user.wards
    @beds_ward_wise = []
    ward_ids.each do |ward_id|
      @beds_ward_wise << Bed.where(ward_id: ward_id,medical_unit_id: params[:medical_unit_id],status: 'Occupied').count
      @beds_ward_wise.flatten!
    end
    if @beds_ward_wise.present?
      render status: :ok, json: @beds_ward_wise
    else
      render status: :not_found, json: { message: 'No Beds found in this ward' }
    end
  end
end