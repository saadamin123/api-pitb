# class documention
class Api::V3::PatientDiagnosesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index]
  before_action :fetch_patient, only: [:create, :index]
  before_action :fetch_visit, only: [:create, :index]

  respond_to :json

  def create
    params[:selected_dignosis].each do |sd|
     @patient_diagnosis = PatientDiagnosis.new(user_id: @current_user.id, name: sd['name'], 
      code: sd['code'],type: params[:type],visit_id:  params[:visit_id],
      patient_id: @patient.id,instructions: params[:instructions],
      plan: params[:plan])
    end
    if @patient_diagnosis.save
      render status: :ok, json: @patient_diagnosis
    else
      render status: :unprocessable_entity,
             json: { message: @patient_diagnosis.errors.full_messages }
    end
  end
  def index
    @patient_diagnosis = PatientDiagnosis.includes(:user).where(patient_id: @patient.id,visit_id: @visit.id)
      if @patient_diagnosis.present?
        render status: :ok, json: @patient_diagnosis
      else
        render status: :not_found, json: { message: 'Record not found' }
      end
  end

end