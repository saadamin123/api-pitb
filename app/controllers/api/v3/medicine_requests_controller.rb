# class documention
class Api::V3::MedicineRequestsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index]
  before_action :verify_device, only: [:create]
  before_action :fetch_medical_unit, only: [:create, :index]
  before_action :fetch_patient, only: [:create, :index]
  before_action :fetch_visit, only: [:create]
  #before_action :fetch_admission, only: [:create]

  respond_to :json

  def create
    request_id = nil
    request_id =  Date.today.strftime("%Y") + '-' + Date.today.strftime("%m") + '-' + Date.today.strftime("%d")+ '-' + (MedicineRequest.where('Created_At >= ?', Date.current).count+1).to_s
    params[:items].each do |item|
      @medicine_request = MedicineRequest.new(prescribed_qty: item[:quantity])
      @item_stock = ItemStock.find(item[:id])
      @medicine_request.item_id = @item_stock.item_id
      @item = Item.find(@medicine_request.item_id)
      @medicine_request.product_name = @item.product_name 
      @medicine_request.visit = @visit
      @medicine_request.requested_by = @current_user.id
      @medicine_request.patient = @patient
      @store = Store.find_by_id(item[:store_id])
      @medicine_request.store = @store if @store.present?
      @medicine_request.medical_unit = @current_medical_unit
      @admission = Admission.find_by_id(params[:admission_id])
      @medicine_request.admission = @admission if @admission.present?
      @medicine_request.request_id = request_id
      @medicine_request.status = 'Open'
      @medicine_request.save    
    end
    render status: :ok, json: @medicine_request
    
  end

  def index
    @medicines = MedicineRequest.where(patient_id: @patient.id, medical_unit_id: params[:medical_unit_id])
    if @medicines.present?
      render status: :ok, json: @medicines
    else
      render status: :unprocessable_entity, json: { message: 'No Medicine Requests found' }
    end
  end


end

