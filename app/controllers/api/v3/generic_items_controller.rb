# class documention
class Api::V3::GenericItemsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    @generic_items = GenericItem.where('category = ?',
                            params[:category]) if params[:category]
    if @generic_items.present?
      @generic_items = @generic_items.order('name ASC')
      render status: :ok, json: @generic_items
    else
      render status: :not_found, json: { message: 'Region not found' }
    end
  end

  def search
    generic_items = GenericItem.arel_table
    possible_generic_items = GenericItem.where(generic_items[:name].matches("#{params[:query]}%"))
    if possible_generic_items.present?
      render status: :ok, json: possible_generic_items
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end
end
