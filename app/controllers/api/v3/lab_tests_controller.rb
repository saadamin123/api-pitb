class Api::V3::LabTestsController < ApplicationController
  include ApplicationHelper
  before_action :verify_authenticity_token, :check_machine_and_parameter_code, :check_machine_code, only: [:get_test_paramsters]
  before_action :verify_patient, only: [:parameters_ranges]
  respond_to :json

  def index
    lab_service = LabService.new(params[:medical_unit_id], @patient)
    @tests = lab_service.hospital_tests_list
    # @tests = LabInvestigation.where(medical_unit_id: params[:medical_unit_id], status: "true")
    if @tests.present?
      render status: :ok, json: @tests
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def parameters_ranges
    results = []
    lab_service = LabService.new(params[:medical_unit_id], @patient)
    params[:profile_name].split(',').each do |profile_name|
      results << lab_service.test_parameters_ranges(profile_name, @current_user)
    end
    if results.present?
      render status: :ok, json: results
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def get_test_paramsters

    order_id = params[:order_id]
    @date =  create_lab_order_sequence(order_id)
    @lab_order_sequence_number = format('%03d',@medical_unit_id).to_s + @date + order_id[2..5]
    machine_code = params[:machine_code]
    @lab_assessment_data = []
    @data = []
    order = Order.where(lab_order_sequence_number:  @lab_order_sequence_number).first
    order.lab_order_details.each do |lab_order_detail|
      @assessment = lab_order_detail.lab_investigation.lab_assessments if lab_order_detail.lab_investigation.present?
      @lab_assessment_data << @assessment if @assessment.present?
    end
    @lab_assessment_data.each do |lab_assessments|
      lab_assessments.each do |lab_assessment|
      @machine_info = Machine.find_by(lab_assessment_id: lab_assessment.id, medical_unit_id: @medical_unit_id, machine_code: params[:machine_code])
      if @machine_info.present?
        @data << @machine_info.machine_parameter_code
      end
      end
    end

    if @data.present?
      render  status: :ok, json: { parameters_code: @data }
    else
      render status: :not_found, json: { message: 'No parameters found' }
    end
  end

  def check_machine_code
    machine_info = Machine.find_by(machine_code: params[:machine_code])
    if machine_info.blank?
      render status: :not_found, json: { message: 'Invalid machine code' }
    else
      @medical_unit_id = machine_info.medical_unit_id
    end
  end

  def check_machine_and_parameter_code
    if params[:machine_code].blank?
      render status: :not_found, json: { message: 'Please enter machine code first' }
    elsif params[:order_id].blank?
      render status: :not_found, json: { message: 'Please enter order id first' }
    end
  end

end