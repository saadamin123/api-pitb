# class documention
class Api::V2::OrdersController < ApplicationController
  before_action :verify_authenticity_token
  before_action :verify_device, only: [:index]
  before_action :fetch_medical_unit, only: [:index]
  before_action :verify_device_role_medical_unit, only: [:index] if Rails.env != "test"
  before_action :verify_patient, only: [:index]
  serialization_scope :current_inventory
  serialization_scope :current_user
  respond_to :json
  def index
    status = []
    status = params[:status].split(',')
    from_date = Time.now() -1.day
    to_date = Time.now()
    @orders = Order.find_by_scope(current_medical_unit.id, from_date, to_date, params[:type], params[:query], params[:order_type], @current_user, status)
    if @orders.present?
      render status: :ok, json: @orders.sort_by(&:updated_at).reverse
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end


  def lab_history
    if params[:type] == 'All'
      orders = Order.joins(:lab_order_details).where('orders.created_at >=  (?) AND orders.created_at::date <= (?) AND patient_id = ?',params[:start_date],params[:end_date],params[:patient_id]).uniq
    elsif params[:type] != 'All'
      orders = Order.joins(:lab_order_details).where('lab_order_details.test = (?) AND orders.created_at >=  (?) AND orders.created_at::date <= (?) AND patient_id = ?',params[:type],params[:start_date],params[:end_date],params[:patient_id]).uniq
    end
    if orders.present?
      render status: :ok, json: orders     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def order_params
    params.require(:order).permit(:status,:updated_by,:order_type,:department_id,
            :department,:category,:cancel_reason,:cancel_reason_details,:store_id,
            lab_order_details_attributes: [:order_id, :test, :detail, :sample_collected,
            :instructions, :note])     
  end
end

