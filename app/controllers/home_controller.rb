class HomeController < ApplicationController
  def index
  end
  def get_departments
  	@user = User.find_by_id(params[:id])
  	@user_department = @user.medical_units.joins(:departments).pluck("departments.id, departments.title")
  	render layout: false
  end
  def get_wards
  	@user = User.find_by_id(params[:id])
  	@user_ward = Ward.where(medical_unit_id: @user.medical_units.pluck(:id)).pluck(:id, :title)
  	# @user_department = @user.medical_units.joins(:wards).pluck("wards.id, wards.title")
  	render layout: false
  end
  def get_wards_in_medical_unit
    @medical_unit_ward = Ward.where(medical_unit_id: params[:id]).pluck(:id, :title)
    render layout: false
  end
  def get_bays_in_ward
    @bay_ward = Bay.where(ward_id: params[:id]).pluck(:id, :title)
    render layout: false
  end
  def get_labInvestigations_in_medicalUnit
    @labInvestigations = LabInvestigation.where(medical_unit_id: params[:id]).pluck(:id, :profile_name)
    render layout: false
  end
end
