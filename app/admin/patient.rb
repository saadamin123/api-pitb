ActiveAdmin.register Patient do
  menu parent: 'Manage Patient'
  filter :patient_nic
  actions :all, except: [:destroy]

  permit_params :first_name, :last_name, :middle_name,
                :gender, :education, :birth_day,
                :birth_month, :birth_year, :patient_nic,
                :patient_passport, :unidentify_patient,
                :guardian_relationship,
                :guardian_first_name, :guardian_last_name,
                :guardian_middle_name, :guardian_nic,
                :guardian_passport, :unidentify_guardian, :marital_status,
                :state, :city, :near_by_city, :address1,
                :address2, :phone1, :phone1_type, :phone2,
                :phone2_type, :phone3, :phone3_type, :father_name, :age

  index do
    selectable_column
    id_column
    column :mrn
    column :first_name
    column :last_name
    column :patient_nic
    column :state
    column :city
    actions
  end

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :middle_name
      f.input :gender
      f.input :education
      f.input :birth_day
      f.input :birth_month
      f.input :birth_year
      f.input :patient_nic
      f.input :patient_passport
      f.input :unidentify_patient
      f.input :guardian_first_name
      f.input :guardian_last_name
      f.input :guardian_middle_name
      f.input :guardian_nic
      f.input :guardian_passport
      f.input :unidentify_guardian
      f.input :marital_status
      f.input :state
      f.input :city
      f.input :near_by_city
      f.input :address1
      f.input :address2
      f.input :phone1
      f.input :phone1_type
      f.input :phone2
      f.input :phone2_type
      f.input :phone3
      f.input :phone3_type
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end

  collection_action :download_report, :method => :get do
    csv_data = Patient.patient_csv_data(Date.today-20, Date.today)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << ['first_name', 'last_name', 'father_name', 'mrn', 'age', 'gender', 'created_at', 'city', 'near_by_city',
              'guardian_first_name', 'guardian_nic', 'address1', 'phone1', 'current_referred_to', 'user_first_name', 'user_last_name']
      csv_data.each do |record|
        csv << [record['first_name'], record['last_name'],record['father_name'], record['mrn'], record['age'], record['gender'], record['created_at'], record['city'],record['near_by_city'],
               record['guardian_first_name'],record['guardian_nic'],record['address1'],record['phone1'],record['current_referred_to'], record['user_first_name'], record['user_last_name'] ]
        # csv << [record.patient.first_name, record.patient.last_name, record.patient.father_name, record.patient.mrn, record.patient.age, record.patient.gender, record.patient.city,record.patient.near_by_city,
        #      record.patient.guardian_first_name,record.patient.guardian_nic,record.patient.address1,record.patient.phone1,record.current_referred_to, record.created_by.full_name ]
      end      
    end
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=report.csv"
  end

  action_item do
    link_to(('csv report'), params.merge(action: :download_report))
  end 

  index :download_links => false do
    # off standard download link
  end

  # csv do
  #   # q= 'select CONCAT_WS(patients.first_name, patients.last_name) AS patient_full_name, patients.mrn, patients.age, patients.gender, patients.city, visits.current_referred_to, CONCAT(users.first_name, users.last_name) AS user_full_name FROM patients INNER JOIN visits ON patients.id=visits.patient_id INNER JOIN users on users.id = visits.user_id;'
  #   column :full_name
  #   column :mrn
  #   column :age
  #   column :gender
  #   column :city

  #   # column 'Referred To' do |patient|
  #   #   patient.visits.last.current_referred_to if !patient.visits.last.nil?
  #   # end
  #   # column 'User Name' do |patient|
  #   #   user_id = patient.visits.last.user_id if !patient.visits.last.nil?
  #   #   user = User.find(user_id).full_name if !user_id.nil?
  #   # end
  #   column :created_at

  # end

  show do
    attributes_table  :mrn, :first_name, :last_name, :middle_name,
                      :gender, :education, :birth_day,
                      :birth_month, :birth_year, :patient_nic,
                      :patient_passport, :unidentify_patient,
                      :guardian_relationship,
                      :guardian_first_name, :guardian_last_name,
                      :guardian_middle_name, :guardian_nic,
                      :guardian_passport, :unidentify_guardian,
                      :marital_status, :state, :city, :near_by_city,
                      :address1,:address2, :phone1, :phone1_type, :phone2,
                      :phone2_type, :phone3, :phone3_type,:age
  end
end
