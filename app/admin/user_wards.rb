ActiveAdmin.register UserWard do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

	filter :user, collection: proc { User.order(:first_name) }
	filter :ward, collection: proc { Ward.order(:title) }
actions :all, :except => [:create,:update]
	index do
		column :user
		column :ward
		column :created_at
		column :updated_at

		column "action" do |resource|
	      links = ''.html_safe
	      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
	      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
	      links
	    end
	end	

	show do
		attributes_table do
			row :user
			row :ward
	    end
	end	

	form do |f|
      f.inputs do
      f.input :user, input_html: { id:'userIDWard'}
      if f.object.new_record?
      	f.input :ward,multiple: true,
      	input_html: { id:'wardID' , disabled: 'disabled'}
      else
      	f.input :ward,
     	input_html: { id:'wardID' , disabled: 'disabled'}
      end
    end
    f.actions
  end

  controller do
    def create
	  	params[:user_ward][:ward_id].each do |ward_id|
	  		@user_wards = UserWard.create(user_id: params[:user_ward][:user_id],ward_id: ward_id)
	  	end
	    redirect_to admin_user_wards_url
  	end
  	def update
  		@user_ward = UserWard.find_by(user_id: params[:user_ward][:user_id])
	  	@user_ward.update_attributes(ward_id: params[:user_ward][:ward_id])
	    redirect_to admin_user_wards_url
  	end
  end


end
