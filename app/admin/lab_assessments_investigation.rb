ActiveAdmin.register LabAssessmentsInvestigation do
  # filter :label,
  # :as => :select, :collection => MedicalUnit.all.map {|mu| [mu.title, mu.id]},
  # input_html: { id:'muId'}
  # filter :lab_investigation_id,
  # :as => :select, :collection => LabInvestigation.all.map {|li| [li.profile_name, li.id]},
  # input_html: { id:'liId'  , disabled: 'disabled'}
  filter :lab_investigation_id,
  :as => :select, :collection => LabInvestigation.all.map {|li| [li.profile_name, li.id]}

  index do
    selectable_column
    id_column
    column 'Medical Unit', sortable: :lab_investigation do |lai|
      link_to  lai.lab_investigation.medical_unit.title
    end
    column :lab_investigation do |lai|
      link_to lai.lab_investigation.profile_name
    end
    column :lab_assessment
    column :sequence
    actions
  end
end