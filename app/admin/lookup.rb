ActiveAdmin.register Lookup do
  menu parent: 'Global Settings'

  scope :all, default: true
  scope :visit_reason
  scope :visiter_referred_to
  scope :mode_of_conveyance
  scope :education
  scope :relationship
  scope :complaint_category
  scope :complaint_role
  scope :Procedure
  scope :Lab
  scope :Historical_notes
  scope :Radiology_Imaging_type
  scope :Radiology_Area
  scope :Radiology_other_options
  scope :Radiology_other_options

  form do |f|
    f.inputs do
      f.semantic_errors # shows errors on :base
      f.input :category,
              as: :select,
              collection: Lookup::CATEGORY.collect { |category| [category, category] } # rubocop:disable Metrics/LineLength
      f.input :key
      f.input :value
      f.input :parent_id
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end
end
