ActiveAdmin.register MedicalUnit do

  menu parent: "Manage Medical Unit"
  
  permit_params :region_id, :title, :category, :identification_number,
                :location, :latitude, :longitude, :parent_id, :code

  scope :all, default: true
  scope :THQ
  scope :DHQ
  scope :BHU
  scope :RHC
  scope :MH
  scope :Teaching

  index do
    selectable_column
    id_column
    column :title
    column :category
    column :region
    column :code
    actions
  end

  filter :title
  filter :category

  form do |f|
    f.inputs do
      f.input :region, as: :select, collection: (Region.all)
      f.input :title
      f.input :code
      f.input :category,
              as: :select,
              collection: MedicalUnit::CATEGORY.collect { |category| [category, category] }
      f.input :parent, as: :select, collection: (MedicalUnit.all).order('title ASC')
    end
    f.actions
  end

  show do
    attributes_table :region, :title, :category, :parent, :code
  end
end
