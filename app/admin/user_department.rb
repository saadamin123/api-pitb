ActiveAdmin.register UserDepartment do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	filter :user, collection: proc { User.order(:first_name) }
	filter :department, collection: proc { Department.order(:title) }

	 actions :all, :except => [:create,:update]

	index do
		column :user
		column :department
		column :created_at
		column :updated_at

		column "action" do |resource|
	      links = ''.html_safe
	      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
	      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
	      links
	    end
	end	

	show do
		attributes_table do
			row :user
			row :department
	    end
	end	

	form do |f|
      f.inputs do
      f.input :user, input_html: { id:'userID'}
      if f.object.new_record?
      	f.input :department,multiple: true,
      	input_html: { id:'depID' , disabled: 'disabled'}
      else
      	f.input :department,
      	input_html: { id:'depID' , disabled: 'disabled'}
      end
    end
    f.actions
  end
  controller do
    def create
	  	params[:user_department][:department_id].each do |department_id|
	  		@user_departments = UserDepartment.create(user_id: params[:user_department][:user_id],department_id: department_id)
	  	end
	    redirect_to admin_user_departments_url
  	end
  	def update
  		@user_department = UserDepartment.find_by(user_id: params[:user_department][:user_id])
	  	@user_department.update_attributes(department_id: params[:user_department][:department_id])
	    redirect_to admin_user_departments_url
  	end
  end

end
