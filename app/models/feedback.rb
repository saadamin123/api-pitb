# == Schema Information
#
# Table name: feedbacks
#
#  id              :integer          not null, primary key
#  complaint_id    :integer
#  medical_unit_id :integer
#  user_id         :integer
#  device_id       :integer
#  satisfaction    :string
#  message         :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string
#

# class documentation
class Feedback < ActiveRecord::Base
  after_create :updated_complaint

  belongs_to :medical_unit
  belongs_to :user
  belongs_to :device
  belongs_to :complaint

  validates :satisfaction, presence: true
  validates :status, presence: true

  private

  def updated_complaint
    self.complaint.update_attributes!(updated_at: Time.now)
  end
end
