# == Schema Information
#
# Table name: procedure_order_details
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  procedure    :string
#  option       :string
#  left         :boolean          default("false")
#  right        :boolean          default("false")
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  instructions :string
#  note_id      :integer
#  type         :string
#

class ProcedureOrderDetail < ActiveRecord::Base
  validates :procedure, presence: true

  belongs_to :order
end
