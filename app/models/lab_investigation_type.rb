# == Schema Information
#
# Table name: lab_investigation_types
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LabInvestigationType < ActiveRecord::Base
	has_many :lab_investigations
end
