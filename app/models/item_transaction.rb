# == Schema Information
#
# Table name: item_transactions
#
#  id                      :integer          not null, primary key
#  item_id                 :integer
#  medical_unit_id         :integer
#  department_id           :integer
#  store_id                :integer
#  user_id                 :integer
#  patient_id              :integer
#  visit_id                :integer
#  transaction_type        :string
#  issue_department_id     :integer
#  recieve_store_id        :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  quantity                :string
#  item_stock_id           :integer
#  receipt_number          :string
#  doctor_id               :integer
#  open_stock              :string
#  sister_incharge_name    :string
#  registrar_name          :string
#  pharmacist_name         :string
#  store_keeper_name       :string
#  pharmacist_dms_ams_name :string
#  doctor_nurse_name       :string
#  unit                    :float
#  comment                 :string
#

class ItemTransaction < ActiveRecord::Base
  belongs_to :item
  belongs_to :item_stock
  belongs_to :medical_unit
  belongs_to :department
  belongs_to :store
  belongs_to :user
  belongs_to :patient
  belongs_to :visit

  validates :quantity, presence: true

  def self.top_ten_issued_medicine(medical_unit_ids, start_date, end_date)
    items_hash = ItemTransaction.joins(:item).where('item_transactions.medical_unit_id in (?) AND item_transactions.Created_At >= ? AND item_transactions.Created_at <=? AND transaction_type = ? AND sub_group=?' ,medical_unit_ids ,start_date, end_date, "issue", "medicine").group(:item_id).count
    top_ten = Hash[*items_hash.sort_by { |k,v| -v }[0..9].flatten]
    return Hash[*top_ten.sort_by { |k,v| -k }[0..9].flatten]
  end
end
