# == Schema Information
#
# Table name: wards
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  department_id   :integer
#  parent_id       :integer
#  title           :string
#  active          :boolean          default("true")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#  beds_count      :integer          default("0")
#  bays_count      :integer          default("0")
#  deleted_at      :datetime
#  ward_code       :string
#

# class documentation
class Ward < ActiveRecord::Base
	acts_as_paranoid
	
	has_many :bays
	belongs_to :department, counter_cache: :wards_count
	# belongs_to :user
	belongs_to :parent, class_name: "Ward", foreign_key: "parent_id"
	has_many :sub_wards, class_name: "Ward", foreign_key: "parent_id"
	# validates :medical_unit_id, presence: true
	validates :department_id, presence: false
	has_many :user_wards
  	has_many :users, through: :user_wards
  	has_many :admissions
  	has_many :bed_allocations
  	has_many :beds
end
