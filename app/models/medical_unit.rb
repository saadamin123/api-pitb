# == Schema Information
#
# Table name: medical_units
#
#  id                    :integer          not null, primary key
#  region_id             :integer
#  title                 :string
#  identification_number :string
#  location              :string
#  latitude              :float
#  longitude             :float
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  category              :string
#  bays_count            :integer          default("0")
#  parent_id             :integer
#  code                  :string
#

# class documentation
class MedicalUnit < ActiveRecord::Base
  CATEGORY = %w(THQ DHQ BHU RHC MH Teaching)

  scope :THQ, -> { where(category: 'THQ') }
  scope :DHQ, -> { where(category: 'DHQ') }
  scope :BHU, -> { where(category: 'BHU') }
  scope :RHC, -> { where(category: 'RHC') }
  scope :MH, -> { where(category: 'MH/SP') }
  scope :Teaching, -> { where(category: 'Teaching') }

  belongs_to :region
  has_many :services
  has_many :devices, as: :assignable
  has_many :departments
  has_many :stores
  has_many :complaints
  has_many :feedbacks
  has_many :medical_unit_users
  has_many :users, through: :medical_unit_users
  has_many :inventories
  has_many :visits
  has_many :orders
  has_many :vaccinations, :dependent => :destroy
  has_many :organizations
  has_many :lab_investigations
  has_many :lab_investigation_prices
  has_many :lab_access_ranges
  has_many :lab_results
  has_many :admissions
  has_many :bed_allocations
  has_many :bays
  has_many :discharge_patients
  has_many :investigation_notes

  belongs_to :parent, class_name: "MedicalUnit", foreign_key: "parent_id"


  validates :title, presence: true
  validates :code, presence: true, length: { is: 4 }, uniqueness:{message: "has been already associated with some other Hospital"}

  def self.fetch_assigned_units(current_user)
    medical_unit_user_ids = current_user.medical_unit_users.pluck(:medical_unit_id)
    medical_unit = MedicalUnit.where('id in(?) AND (parent_id != ? OR parent_id IS NULL)',medical_unit_user_ids, 50).pluck(:id,:title) if medical_unit_user_ids.present?
    return medical_unit
  end

  def self.fetch_assigned_clinics(current_user)
    medical_unit_user_ids = current_user.medical_unit_users.pluck(:medical_unit_id)
    medical_unit = MedicalUnit.where('id in(?) AND parent_id = 50',medical_unit_user_ids).pluck(:id,:title) if medical_unit_user_ids.present?
    return medical_unit
  end

  def self.performance_statistics(from_date, to_date, medical_unit_ids)
    user_ids = staff_ids(medical_unit_ids)
    result = Order.select(:doctor_id).
             where('doctor_id IN (?)', user_ids).
             order('DATE(created_at) DESC').
             group(:doctor_id, 'DATE(created_at)').
             having('COUNT("orders"."doctor_id") > 3').count
    hash = calculate_active_staff(result)
    return build_series(hash, from_date, to_date, user_ids.count)
  end

  def self.staff_ids(medical_unit_ids)
    ids = []
    role = Role.where(title: 'Dr').first
    if medical_unit_ids.nil?
      ids = MedicalUnitUser.where(role_id: role.id).map(&:user_id)
    else
      ids = MedicalUnitUser.where(role_id: role.id, medical_unit_id: medical_unit_ids).map(&:user_id)
    end
    return ids  
  end

  def visited_patients(category,department)
    patient_ids = self.visits.pluck(:patient_id).uniq
    patients = Patient.where('id IN (?)', patient_ids)
    p_ids = []
    patients.each do |p|
      visit = p.visits.order('created_at Desc').first
      if visit.current_referred_to == category &&  visit.current_ref_department == department
        p_ids.push(p.id)
      end
    end
    # patient_ids = self.visits.where(current_referred_to: category,current_ref_department: department).pluck(:patient_id)
    Patient.where(id: p_ids)
  end

  private

  def self.build_series(result, from , to, total_staff)
    [ 
      {
        name: 'Total staff',
        data: total_staff
      },
      {
        name: 'active staff',
        data: extract_info(result, from, to)
      }
    ]
  end

  def self.calculate_active_staff(result)
    hash = {}
    result.keys.each do |k|
      key = k[1].strftime("%Y-%m-%d")
      hash[key] = hash[key].present? ? hash[key]+1 : 1
    end
    hash
  end

  def self.extract_info(result, from , to)
    array = []
    (from..to).each do |date|
      key = date.strftime("%Y-%m-%d")
      if result[key]
        array << result[key]
      else
        array << 0
      end
    end
    array
  end
end
