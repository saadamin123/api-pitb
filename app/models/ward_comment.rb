# == Schema Information
#
# Table name: ward_comments
#
#  id                :integer          not null, primary key
#  medical_unit_id   :integer
#  user_id           :integer
#  patient_id        :integer
#  visit_id          :integer
#  admission_id      :integer
#  bed_allocation_id :integer
#  comments          :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class WardComment < ActiveRecord::Base
	belongs_to :user
	belongs_to :patient
	belongs_to :visit
	belongs_to :admission
	belongs_to :bed_allocation

	def self.new_comment(bed_allocation,comment)
		WardComment.create(user_id: bed_allocation.user_id,patient_id: bed_allocation.patient_id,
			visit_id: bed_allocation.visit_id,admission_id: bed_allocation.admission_id,
			bed_allocation_id: bed_allocation.id,comments: comment)
	end
end
