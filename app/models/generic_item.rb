# == Schema Information
#
# Table name: generic_items
#
#  id         :integer          not null, primary key
#  name       :string
#  category   :string
#  created_at :datetime
#  updated_at :datetime
#

class GenericItem < ActiveRecord::Base

  acts_as_copy_target   # To enable the copy commands of postgres-copy gem.
  validates :name, presence: true
  validates :category, presence: true
end
