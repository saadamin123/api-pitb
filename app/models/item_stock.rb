# == Schema Information
#
# Table name: item_stocks
#
#  id               :integer          not null, primary key
#  medical_unit_id  :integer
#  department_id    :integer
#  store_id         :integer
#  user_id          :integer
#  item_id          :integer
#  quantity_in_hand :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class ItemStock < ActiveRecord::Base
  belongs_to :medical_unit
  belongs_to :department
  belongs_to :store
  belongs_to :user
  belongs_to :item

  has_many :item_transactions


  def update_quantity(quantity)
    self.quantity_in_hand.to_f - quantity.to_f
  end

  def create_item_transactions(item_stock, params, patient, transaction_type, current_user, receipt_number, doctor_id, on_hand_quantity)
    if transaction_type == 'add' || transaction_type == 'adjust'
      item_stock.item_transactions.create(medical_unit_id: item_stock.medical_unit_id, department_id: item_stock.department_id,
                                        store_id: item_stock.store_id, user_id: current_user.id, item_id: item_stock.item_id,
                                        transaction_type: transaction_type, quantity: params[:quantity], unit: params[:unit], open_stock: on_hand_quantity, comment: params[:comment], receipt_number: receipt_number, pharmacist_name: current_user.first_name+' '+current_user.last_name)
    
    elsif transaction_type == 'issue'
      item_stock.item_transactions.create(medical_unit_id: item_stock.medical_unit_id, department_id: item_stock.department_id,
                                        store_id: item_stock.store_id, user_id: current_user.id, item_id: item_stock.item_id,
                                        transaction_type: transaction_type, quantity: params[:quantity], unit: params[:unit], receipt_number: receipt_number,
                                        patient_id: patient.id, visit_id: patient.visits.last.id, doctor_id: doctor_id, open_stock: on_hand_quantity, pharmacist_name: current_user.first_name+' '+current_user.last_name)
    end
  end

  def create_department_item_transactions(item_stock, params, transaction_type, current_user, receipt_number, doctor_id, on_hand_quantity, transaction_params = {})
    transaction_params = {} if transaction_params.blank?
    if transaction_type == 'issue'
      item_stock.item_transactions.create(
        medical_unit_id: item_stock.medical_unit_id, 
        department_id: item_stock.department_id,
        store_id: item_stock.store_id, 
        user_id: current_user.id, 
        item_id: item_stock.item_id,
        transaction_type: transaction_type, 
        quantity: params[:quantity], 
        receipt_number: receipt_number,
        doctor_id: doctor_id, 
        open_stock: on_hand_quantity,
        doctor_nurse_name: transaction_params[:doctor_nurse_name], 
        sister_incharge_name: transaction_params[:sister_incharge_name], 
        registrar_name: transaction_params[:registrar_name],
        pharmacist_name: transaction_params[:pharmacist_name],
        store_keeper_name: transaction_params[:store_keeper_name],
        pharmacist_dms_ams_name: transaction_params[:pharmacist_dms_ams_name]
      )
    end
  end
end
