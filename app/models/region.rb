# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  category   :string
#  name       :string
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# class documentation
class Region < ActiveRecord::Base
  CATEGORY = %w(country province district tehsil)
  belongs_to :parent, class_name: 'Region', foreign_key: 'parent_id'

  scope :country, -> { where(category: 'country') }
  scope :province, -> { where(category: 'province') }
  scope :district, -> { where(category: 'district') }
  scope :tehsil, -> { where(category: 'tehsil') }

  validates :name, presence: true
  validates :category, presence: true
end
