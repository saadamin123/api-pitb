# == Schema Information
#
# Table name: pictures
#
#  id             :integer          not null, primary key
#  imageable_id   :integer
#  imageable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  file           :string
#  visit_id       :integer
#

# class documentation
class Picture < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  belongs_to :user
  belongs_to :visit

  mount_uploader :file, ImageUploader
  validates :file, presence: true
  validates :user, presence: true
end
