# == Schema Information
#
# Table name: lab_results
#
#  id                   :integer          not null, primary key
#  patient_id           :integer
#  medical_unit_id      :integer
#  lab_investigation_id :integer
#  lab_order_detail_id  :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  note                 :text
#

class LabResult < ActiveRecord::Base
	belongs_to :patient
	belongs_to :medical_unit
	belongs_to :lab_investigation
	has_many :lab_result_parameters
	belongs_to :lab_order_detail
end
