# == Schema Information
#
# Table name: departments
#
#  id              :integer          not null, primary key
#  title           :string
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#  department_code :string
#  ipd             :boolean          default("false")
#  opd             :boolean          default("false")
#  emergency       :boolean          default("false")
#  wards_count     :integer          default("0")
#  lookup_id       :integer
#

# class documentation
class Department < ActiveRecord::Base
  belongs_to :medical_unit
  belongs_to :parent, class_name: "Department", foreign_key: "parent_id"
  has_many :sub_departments, class_name: "Department", foreign_key: "parent_id"
  has_many :stores
  validates :title, presence: true
  has_many :lab_investigations
  #has_many :users
  has_many :user_departments
  has_many :users, through: :user_departments
  has_many :lab_order_details
  has_many :wards
  belongs_to :lookup

  validates :medical_unit_id, uniqueness: { scope: [:department_code, :title],
                                            message: "Should not duplicate department" }
  
end
