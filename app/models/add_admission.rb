# == Schema Information
#
# Table name: add_admissions
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  category     :string
#  department   :string
#  floor_number :string
#  bed_number   :string
#  bay_number   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class AddAdmission < ActiveRecord::Base
  belongs_to :order
  validates :department, presence: true
  validates :category, presence: true
  validates :floor_number, presence: true
  validates :bed_number, presence: true
  before_save :update_visit_location
  after_create :close_order

  def update_visit_location
    order.visit.update_attributes(current_referred_to: self.category,current_ref_department: self.department)
  end
  def close_order
    order.update_attributes(status: "Closed")
  end
end
