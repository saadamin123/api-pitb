# == Schema Information
#
# Table name: visit_users
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  visit_id    :integer
#  is_checkout :boolean          default("false")
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class VisitUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :visit

  def self.total_number_of_patients_seen(medical_unit_ids=nil,user_id, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      visit_ids = VisitUser.where('Created_At >= ? AND Created_at <=?', start_date, end_date).pluck(:visit_id).uniq
      # result = visit_ids.count
      result = Patient.joins(:visits).where("visits.id in (?)", visit_ids)
    else
    	visit_ids = Visit.where('medical_unit_id in (?)', medical_unit_ids).pluck(:id)
      result = VisitUser.where('visit_id in (?) AND user_id = ? AND Created_At >= ? AND Created_at <=?', visit_ids,user_id, start_date, end_date).pluck(:visit_id).uniq
      result = Patient.joins(:visits).where("visits.id in (?)", result)
    end
    return result
  end
end
