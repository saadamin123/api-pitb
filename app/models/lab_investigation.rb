# == Schema Information
#
# Table name: lab_investigations
#
#  id                        :integer          not null, primary key
#  profile_name              :string
#  medical_unit_id           :integer
#  status                    :string
#  lab_investigation_type_id :integer
#  profile_code              :string
#  report_type               :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  department_id             :integer
#

class LabInvestigation < ActiveRecord::Base
	
	has_and_belongs_to_many :lab_assessments
	belongs_to :lab_investigation_type
	belongs_to :medical_unit
	has_many :lab_investigation_prices
	has_many :lab_results
	has_many :lab_order_details
	belongs_to :department
	has_many :lab_access_ranges

	#accepts_nested_attributes_for :lab_investigation_prices
	
	attr_accessor :price

	def self.list
		self.pluck(:profile_name, :id)
	end
end
