# == Schema Information
#
# Table name: order_items
#
#  id                   :integer          not null, primary key
#  order_id             :integer
#  brand_drug_id        :integer
#  quantity_prescribed  :integer
#  frequency_unit       :string
#  quantity_calculated  :integer
#  issued_brand_drug_id :integer
#  notes                :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  duration_unit        :string
#  duration             :integer
#  type                 :string
#  quantity             :float
#  frequency            :string
#  prn                  :boolean
#  route                :string
#  note_id              :integer
#  dosage               :string
#  dosage_unit          :string
#  strength             :string
#

class OrderItemMedicine < OrderItem
  # validates :frequency, presence: true # nurse dept orders issue
  # after_create :calculate_quantity

  def calculate_quantity
    if self.frequency == "once"
        self.quantity_calculated = quantity
        self.save
    else
      unless self.frequency_unit.present?
        if self.frequency == "QID"
          f = 4
        elsif self.frequency == "BID"
          f = 2
        else # TID
          f = 3
        end 
        fu = "1.day"
      else
        f_str = self.frequency.delete "Q"
        f = f_str.to_i
        fu = "1." << self.frequency_unit
      end
      du = self.duration.to_s << "." << self.duration_unit
      new_quantity = (((eval(du))/(eval(fu)))/f) * quantity
      self.quantity_calculated = new_quantity.ceil
      self.save
    end
  end
end
