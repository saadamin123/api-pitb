# == Schema Information
#
# Table name: complaint_watchers
#
#  id           :integer          not null, primary key
#  complaint_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# class documentation
class ComplaintWatcher < ActiveRecord::Base
  belongs_to :complaint
  belongs_to :user
end
