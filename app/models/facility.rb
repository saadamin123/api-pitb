# == Schema Information
#
# Table name: facilities
#
#  id            :integer          not null, primary key
#  facility_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# class documentation
class Facility < ActiveRecord::Base
	has_many :beds
	
	validates :facility_type, presence: true
end
