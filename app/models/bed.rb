# == Schema Information
#
# Table name: beds
#
#  id              :integer          not null, primary key
#  bed_number      :string
#  bay_id          :integer
#  ward_id         :integer
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  facility_id     :integer
#  deleted_at      :datetime
#  status          :string           default("Free")
#  bed_code        :string
#

# class documentation
class Bed < ActiveRecord::Base
	acts_as_paranoid
	belongs_to :facility, counter_cache: :beds_count
	belongs_to :bay, counter_cache: :beds_count
	belongs_to :ward, counter_cache: :beds_count
	has_many :bed_allocations
	has_many :discharge_patients

	validates :bay_id, presence: true
	#validates :ward_id, presence: true
	# validates :medical_unit_id, presence: true
	# validates :facility_id, presence: true
end
