# == Schema Information
#
# Table name: medications
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  brand_drug_id   :integer
#  dosage          :string
#  unit            :string
#  route           :string
#  frequency       :string
#  usage           :string
#  instruction     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  type            :string
#

class Medication < ActiveRecord::Base

  validates :brand_drug, presence: true

  belongs_to :patient
  belongs_to :user
  belongs_to :medical_unit
  belongs_to :visit
  belongs_to :brand_drug
end
