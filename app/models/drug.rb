# == Schema Information
#
# Table name: drugs
#
#  id             :integer          not null, primary key
#  name           :string
#  overview       :text
#  characterstics :text
#  indications    :text
#  contradictions :text
#  interactions   :text
#  interference   :text
#  effects        :text
#  risk           :text
#  warning        :text
#  storage        :text
#  category       :string
#  created_at     :datetime
#  updated_at     :datetime
#

# class documentation
class Drug < ActiveRecord::Base
  has_many :brand_drugs
  has_many :brands, through: :brand_drugs
  acts_as_copy_target   # To enable the copy commands of postgres-copy gem.
  validates :name, presence: true
  validates :category, presence: true
  validates :overview, presence: true
end
