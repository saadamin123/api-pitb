# == Schema Information
#
# Table name: nursing_order_details
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  department   :string
#  instructions :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class NursingOrderDetail < ActiveRecord::Base
  belongs_to :order
  validates :department, presence: true
end
