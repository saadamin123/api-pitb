# == Schema Information
#
# Table name: lab_test_details
#
#  id                  :integer          not null, primary key
#  order_id            :integer
#  lab_order_detail_id :integer
#  caption             :string
#  range               :string
#  unit                :string
#  value               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  comments            :string
#  test_label          :string
#

class LabTestDetail < ActiveRecord::Base
  after_update :set_status_closed

  belongs_to :order
  belongs_to :lab_order_detail

  validates :caption, presence: true

  private

  def set_status_closed
  	# order = Order.find_by(id: self.order.id)
   #  order.update_attributes(status: 'Closed') if value.present?
    lab_order = LabOrderDetail.find_by(id: self.lab_order_detail.id)
    lab_order.update_attributes(status: 'Unverified') if value.present?
  end
end
