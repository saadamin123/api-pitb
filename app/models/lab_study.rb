# == Schema Information
#
# Table name: lab_studies
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LabStudy < ActiveRecord::Base
  validates :name, presence: true
end
