# == Schema Information
#
# Table name: admissions
#
#  id                     :integer          not null, primary key
#  medical_unit_id        :integer
#  visit_id               :integer
#  patient_id             :integer
#  referred_from          :integer
#  referred_to            :integer
#  ward_id                :integer
#  doctor_id              :integer
#  user_id                :integer
#  attendent_name         :string
#  attendent_mobile       :string
#  attendent_address      :string
#  attendent_nic          :string
#  attendent_relationship :string
#  status                 :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  admission_number       :string
#

# class documentation
class Admission < ActiveRecord::Base
	include PublicActivity::Model
  	tracked owner: Proc.new { |controller, model| controller && controller.current_user }
	
	belongs_to :medical_unit
	belongs_to :visit
	belongs_to :patient
	belongs_to :ward
	belongs_to :user
	has_many :bed_allocations
	has_many :discharge_patients
	has_many :investigation_notes
	has_many :ward_comments
	after_create :create_bed_allocation

	def create_bed_allocation
		@bed_allocation = BedAllocation.create(medical_unit_id: self.medical_unit_id,patient_id: self.patient_id,
						admission_id: self.id,ward_id: self.ward_id,patient_last_updated_by: self.user_id,
						visit_id: self.visit_id)
	end

	def generate_admission_number
    self.admission_number =  Date.today.strftime("%Y") + '-' + Date.today.strftime("%m") + '-' + Date.today.strftime("%d")+ '-' + (Admission.where('Created_At >= ?', Date.current).count+1).to_s
  	end
end
