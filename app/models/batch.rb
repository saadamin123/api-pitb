# == Schema Information
#
# Table name: batches
#
#  id          :integer          not null, primary key
#  number      :string
#  expiry_date :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  quantity    :integer          default("0")
#

class Batch < ActiveRecord::Base

  has_many :line_items
end
