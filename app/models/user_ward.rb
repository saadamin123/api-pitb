# == Schema Information
#
# Table name: user_wards
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  ward_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserWard < ActiveRecord::Base
	belongs_to :user
  	belongs_to :ward
end
