# == Schema Information
#
# Table name: medications
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  brand_drug_id   :integer
#  dosage          :string
#  unit            :string
#  route           :string
#  frequency       :string
#  usage           :string
#  instruction     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  type            :string
#

class HomeMedication < Medication
	UNIT = %w(mg mg/ml ml G)
  ROUTE = %w(PO IV Inhaler)
  FREQUENCY = %w(Once Q12 Q24 Q48)
  USAGE = %w(Hr Day Week Month)

  scope :mg, -> { where(unit: 'mg') }
  scope :mg_ml, -> { where(unit: 'mg/ml') }
  scope :ml, -> { where(unit: 'ml') }
  scope :G, -> { where(unit: 'G') }
  scope :PO, -> { where(route: 'PO') }
  scope :IV, -> { where(route: 'IV') }
  scope :Inhaler, -> { where(route: 'Inhaler') }
  scope :Once, -> { where(frequency: 'Once') }
  scope :Q12, -> { where(frequency: 'Q12') }
  scope :Q24, -> { where(frequency: 'Q24') }
  scope :Q48, -> { where(frequency: 'Q48') }
  scope :Hr, -> { where(usage: 'Hr') }
  scope :Day, -> { where(usage: 'Day') }
  scope :Week, -> { where(usage: 'Week') }
  scope :Month, -> { where(usage: 'Month') }

  validates :dosage, presence: true
  validates :unit, presence: true
  validates_length_of :instruction, :maximum => 150, :allow_blank => true
  validates :unit,
            inclusion: { in: %w(mg mg/ml ml G) }
  validates :route,
            inclusion: { in: %w(PO IV Inhaler) }, :allow_blank => true
  validates :frequency,
            inclusion: { in: %w(Once Q12 Q24 Q48) }, :allow_blank => true
  validates :usage,
            inclusion: { in: %w(Hr Day Week Month) }, :allow_blank => true
end
