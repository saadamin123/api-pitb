# == Schema Information
#
# Table name: complaints
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  device_id       :integer
#  category        :string
#  area            :string
#  description     :string
#  severity_level  :string
#  show_identity   :boolean          default("false")
#  status          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  created_by      :string
#  assign_to_role  :integer
#  critical        :boolean          default("false")
#  high_priority   :boolean          default("false")
#

# class documentation
class Complaint < ActiveRecord::Base
  after_create :set_status
  after_create :set_critical
  # after_create :assign_to

  STATUS = %w(Open Closed In\ Progress Pending Resolved)

  scope :Open, -> { where(status: 'Open') }
  scope :Closed, -> { where(status: 'Closed') }
  scope :In_Progress, -> { where(status: 'In\ Progress') }
  scope :Pending, -> { where(status: 'Pending') }
  scope :Resolved, -> { where(status: 'Resolved') }

  belongs_to :medical_unit
  belongs_to :user
  belongs_to :device

  has_many :feedbacks
  has_many :complaint_watchers
  has_many :pictures, as: :imageable
  
  validates :medical_unit_id, presence: true
  validates :user_id, presence: true
  validates :device_id, presence: true
  validates :category, presence: true
  validates :area, presence: true
  validates :description, presence: true
  validates :severity_level, presence: true
  validates :show_identity, inclusion: {in: [true, false]}
  validates :assign_to_role, presence: true
  validates :critical, inclusion: { in: [true, false] }
  validates :status,
            on: :update,
            inclusion: { in: %w(Open Closed In\ Progress Pending Resolved) }
  scope :unresolved_complaints, -> { where.not(status: 'Closed') }

  private

  def set_status
    update_attributes!(status: 'Open')
  end

  def set_critical
    update_attributes!(critical: false)
  end

  # def assign_to
  #   roles = Role.where(title: 'MS').select(:id)
  #   user_ids = MedicalUnitUser.where('medical_unit_id = ? AND role_id in (?)',
  #                                    medical_unit.id, roles).select('user_id')
  #   user_ids.each do |user|
  #     ComplaintWatcher.create!(complaint_id: self.id, user_id: user.user_id)
  #   end
  # end
end
