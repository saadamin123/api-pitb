# == Schema Information
#
# Table name: doctor_patients
#
#  id         :integer          not null, primary key
#  doctor_id  :integer
#  active     :boolean          default("false")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  patient_id :integer
#

class DoctorPatient < ActiveRecord::Base
	belongs_to :patient
	belongs_to :doctor, class_name: 'User' , foreign_key: 'doctor_id'
	#has_many :addmissions
end
