# == Schema Information
#
# Table name: order_items
#
#  id                   :integer          not null, primary key
#  order_id             :integer
#  brand_drug_id        :integer
#  quantity_prescribed  :integer
#  frequency_unit       :string
#  quantity_calculated  :integer
#  issued_brand_drug_id :integer
#  notes                :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  duration_unit        :string
#  duration             :integer
#  type                 :string
#  quantity             :float
#  frequency            :string
#  prn                  :boolean
#  route                :string
#  note_id              :integer
#  dosage               :string
#  dosage_unit          :string
#  strength             :string
#

class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :brand_drug
  validates :brand_drug, presence: true

  has_many :invoice_items, as: :assignable
end
