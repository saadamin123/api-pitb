# == Schema Information
#
# Table name: call_requests
#
#  id           :integer          not null, primary key
#  request_from :integer
#  request_to   :integer
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CallRequest < ActiveRecord::Base
  before_create :generate_code

  belongs_to :from, class_name: 'User', foreign_key: 'request_from'
  belongs_to :to, class_name: 'User', foreign_key: 'request_to'

  def generate_code
    loop do
      self.code = rand.to_s[2..5]
      break unless CallRequest.where(code: code).first
    end
  end
end
