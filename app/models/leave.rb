# == Schema Information
#
# Table name: leaves
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  department_id   :integer
#  attendance      :string
#  shift           :string
#  date            :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Leave < ActiveRecord::Base
  belongs_to :user
  belongs_to :medical_unit
  belongs_to :department
end
