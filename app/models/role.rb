# == Schema Information
#
# Table name: roles
#
#  id             :integer          not null, primary key
#  title          :string
#  default_screen :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

# class documentation
class Role < ActiveRecord::Base
  validates :title, uniqueness: true
end
