# == Schema Information
#
# Table name: notes
#
#  id                     :integer          not null, primary key
#  visit_id               :integer
#  department             :string
#  chief_complaint        :text
#  med_history            :text
#  surgical_history       :text
#  physical_exam          :text
#  instructions           :text
#  plan                   :text
#  type                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :integer
#  device_id              :integer
#  medical_unit_id        :integer
#  subjective             :text
#  operation_date         :date
#  procedure_indications  :text
#  surgeon_assistant_note :text
#  procedure_description  :text
#  anesthesia             :text
#  complications          :text
#  specimens              :text
#  blood_loss             :string
#  disposition            :string
#  admission_date         :date
#  discharge_date         :date
#  free_text              :text
#  hospital_course        :text
#  follow_up              :text
#

class Note < ActiveRecord::Base
  belongs_to :visit
  belongs_to :medical_unit
  belongs_to :user
  belongs_to :device
  has_many :patient_diagnoses, :dependent => :destroy
  has_many :pictures, as: :imageable, :dependent => :destroy
  has_many :procedures, :dependent => :destroy
  has_many :consultations, class_name: "ConsultationOrderDetail", :dependent => :destroy
  has_many :medicines, class_name: "OrderItem", :dependent => :destroy
  has_many :addendums, :dependent => :destroy
  validates :department, presence: true
  accepts_nested_attributes_for :patient_diagnoses, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :procedures, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :medicines, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :consultations, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :addendums, :allow_destroy => true,
                                :reject_if => :all_blank
end
