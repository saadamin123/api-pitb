# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string
#  address    :text
#  phone      :string
#  fax        :string
#  created_at :datetime
#  updated_at :datetime

# class documentation
class Company < ActiveRecord::Base
  has_many :brands
  acts_as_copy_target   # To enable the copy commands of postgres-copy gem.
  validates_presence_of :name
end
