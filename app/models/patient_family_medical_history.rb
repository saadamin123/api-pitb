# == Schema Information
#
# Table name: patient_family_medical_histories
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  note            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class PatientFamilyMedicalHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :patient
  belongs_to :visit
  belongs_to :medical_unit
end
