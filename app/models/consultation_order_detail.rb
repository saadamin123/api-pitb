# == Schema Information
#
# Table name: consultation_order_details
#
#  id          :integer          not null, primary key
#  order_id    :integer
#  department  :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  note_id     :integer
#

class ConsultationOrderDetail < ActiveRecord::Base

  belongs_to :order
  belongs_to :note
  validates :department, presence: true
end
