# == Schema Information
#
# Table name: lab_assessments_investigations
#
#  id                   :integer          not null, primary key
#  lab_assessment_id    :integer
#  lab_investigation_id :integer
#  sequence             :integer
#  label                :string
#

class LabAssessmentsInvestigation < ActiveRecord::Base
	belongs_to :lab_investigation
	belongs_to :lab_assessment
end
