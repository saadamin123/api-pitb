# == Schema Information
#
# Table name: inventories
#
#  id              :integer          not null, primary key
#  brand_drug_id   :integer
#  quantity        :integer          default("0")
#  medical_unit_id :integer
#  device_id       :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  store_id        :integer
#  department_id   :integer
#

class Inventory < ActiveRecord::Base
  belongs_to :brand_drug
  belongs_to :medical_unit
  belongs_to :device
  belongs_to :store
  belongs_to :department
  has_many :line_items
  validates :quantity, presence: true
  validates :brand_drug, presence: true
  validates :medical_unit, presence: true

  def self.generate_medicine_statistics(from, to, medical_unit_ids=nil)
    result = nil
    unless medical_unit_ids != ['All']
      result = Inventory.where(created_at: from .. to, quantity: 0).order('DATE(created_at) DESC')
      total_inventories = Inventory.where(created_at: from .. to).order('DATE(created_at) DESC')
    else
      result = Inventory.where(created_at: from .. to, medical_unit_id: medical_unit_ids, quantity: 0).order('DATE(created_at) DESC')
      total_inventories = Inventory.where(created_at: from .. to, medical_unit_id: medical_unit_ids).order('DATE(created_at) DESC')
    end
    result = result.group_by { |r| r.created_at.to_date.strftime("%Y-%m-%d") }
    total_inventories = total_inventories.group_by { |i| i.created_at.to_date.strftime("%Y-%m-%d") }
    return build_series(result, from , to, total_inventories)
  end

  def self.total_medicine_list(medical_unit_ids=nil)
    result = nil
    unless medical_unit_ids != ['All']
      inv_hash = Inventory.all.pluck(:brand_drug_id, :quantity)
    else
      inv_hash = Inventory.where(medical_unit_id: medical_unit_ids).pluck(:brand_drug_id, :quantity)
    end
    result =  BrandDrug.where(id: inv_hash.collect {|ind| ind[0]})
    return result, inv_hash.collect {|ind| ind[1]}
  end

  private

  def self.build_series(result, from , to, total_inventories)
      [
        
          {
              name: 'Percentage of Medicine in Stock',
              data: extract_info(result, from, to, total_inventories)

          }
      ]
  end

  def self.extract_info(result, from , to, total_inventories)
    array = []
    (from..to).each do |date|
      array1 = nil
      array2 = nil
      key = date.strftime("%Y-%m-%d")
      if result[key]
        array1 = result[key].count.to_f if total_inventories
        array2 = total_inventories[key].count if total_inventories
        array << (array1/array2) * 100 if total_inventories
      else
        array << 0
      end
    end
    array
  end

end
