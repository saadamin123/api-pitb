# == Schema Information
#
# Table name: immunizations
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  age             :string
#  vaccine         :string
#  dose            :string
#  due             :date
#  given_on        :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Immunization < ActiveRecord::Base
  belongs_to :patient
  belongs_to :user
  belongs_to :medical_unit
  belongs_to :visit
end
