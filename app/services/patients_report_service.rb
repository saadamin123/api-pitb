class PatientsReportService
  def initialize(user_id, store_id, hospital_id, start_date = '', end_date = '')
    @user_id = user_id
    @store_id = store_id
    @hospital_id = hospital_id
    @start_date = "#{start_date} 00:00:00"
    @end_date = "#{end_date} 23:59:59"
  end

  def user_patients_report
    q = "SELECT 
        patients.first_name,
        patients.last_name,
        patients.father_name,
        patients.mrn,
        patients.patient_nic,
        patients.age,
        patients.gender,
        patients.city,
        patients.near_by_city,
        patients.state,
        patients.guardian_first_name,
        patients.guardian_last_name,
        patients.guardian_nic,
        patients.guardian_state,
        patients.guardian_city,
        patients.guardian_near_by_city,
        patients.guardian_address,
        patients.guardian_phone,
        patients.created_at::timestamp::date AS patient_created_date,
        patients.created_at::timestamp::time + interval '5 hour' AS patient_created_time,
        patients.address1,
        patients.phone1,
        patients.phone2,
        patients.unidentify AS unknown,
        CASE WHEN visits.current_referred_to IS NULL THEN visits.referred_to ELSE visits.current_referred_to END AS referred_to,
        visits.created_at::timestamp::date AS created_at_date,
        visits.created_at::timestamp::time AS created_at_time,
        users.first_name AS user_first_name,
        users.last_name AS user_last_name 
    FROM patients 
    INNER JOIN visits ON patients.id=visits.patient_id 
    INNER JOIN users ON users.id = visits.user_id"
    q += " where visits.medical_unit_id = #{@hospital_id} 
    AND visits.user_id = #{@user_id} 
    AND visits.created_at >= '#{@start_date}' 
    AND visits.created_at <= '#{@end_date}' 
    order by visits.created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(q)
    generate_csv(user_patients_report_params, csv_data)
  end

  private

  def user_patients_report_params
    [
      'first_name', 'last_name', 'father_name', 'mrn', 'patient_nic',
      'age', 'gender', 'city', 'near_by_city', 'state', 'address1',
      'phone1', 'phone2', 'unknown', 'patient_created_date',
      'patient_created_time', 'guardian_first_name',
      'guardian_last_name', 'guardian_nic', 'guardian_state',
      'guardian_city', 'guardian_near_by_city', 'guardian_address',
      'guardian_phone', 'created_at_date', 'created_at_time',
      'referred_to', 'user_first_name', 'user_last_name'
    ]
  end

  def set_date_format(date)
    date.present? ? date.to_date.strftime("%d-%m-%Y") : date
    # date.present? ? (date.to_datetime+5.hours).strftime("%d-%m-%Y") : date.to_datetime+5.hours
  end

  def generate_csv(parameters = [], csv_data)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << parameters
      csv_data.each do |record|
        parameters_array = []
        parameters.each do |parameter|
          parameter = parameter.downcase
          if parameter == 'created_at_date' or parameter == 'patient_created_date'
            parameters_array << set_date_format(record[parameter].to_s.gsub(";",""))
          elsif parameter == 'created_at_time' or parameter == 'patient_created_time'
            parameters_array << record[parameter].split('.')[0]
            # parameters_array << (record[parameter].to_time+5.hours).strftime("%I:%M:%S").split('.')[0]
          elsif parameter == 'unknown'
            parameters_array << (record[parameter] == 'f' ? 'false' : 'true')
          else
            parameters_array << record[parameter].to_s.gsub(";","")
          end
        end
        csv << parameters_array
      end
    end
    return csv
  end
end