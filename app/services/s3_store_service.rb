class S3StoreService
  BUCKET = ENV['BUCKET'].freeze

  def initialize(file)
    @file = file
    @s3 = Aws::S3::Resource.new(region: 'us-east-1')
    @bucket = @s3.bucket("ranecs-production")
  end

  def store(key)
    @obj = @bucket.object(key+'/'+filename)
    @obj.upload_file(@file, {acl: 'public-read'})
  end

  def url
    @obj.public_url.to_s
  end

  def csv_store(key, current_medical_unit)
    fileID = DataFile.last ? DataFile.last.id+1 : 1
    hospitalName = current_medical_unit.title.parameterize
    @obj = @bucket.object(key+'/'+fileID.to_s+'-'+hospitalName+'.csv')
    @obj.upload_file(@file, {acl: 'public-read'})
  end

  private
  
  def filename
    @filename ||= @file.to_s.split('/').last
  end
end