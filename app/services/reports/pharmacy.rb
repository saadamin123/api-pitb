class Reports::Pharmacy
  def initialize(params, hospital)
    start_arr = params[:from].split(" ")
    date_split = start_arr[0].split("/")
    @start_date = date_split[2]+'-'+date_split[0]+'-'+date_split[1]+' '+start_arr[1]
    end_arr = params[:end_date].split(" ")
    date_split = end_arr[0].split("/")
    @end_date = date_split[2]+'-'+date_split[0]+'-'+date_split[1]+' '+end_arr[1]
    # @start_date = params[:from].to_datetime.strftime("%Y-%m-%d %H:%M:%S")
    # @end_date = params[:end_date].to_datetime.strftime("%Y-%m-%d %H:%M:%S")
    @medicine_id = params[:medicine].present? ? Item.find_by(product_name: params[:medicine]).try(:id) : ""
    @user_id = params[:user_id].present? && params[:user_id] == "undefined" ? "" : params[:user_id]
    @store_id = params[:store_id].present? && params[:store_id] == "undefined" ? "" : params[:store_id]
    # @hospital_id = params[:medical_unit_id].present? && params[:medical_unit_id] == "undefined" ? "" : params[:medical_unit_id]
    @hospital_id = hospital.try(:id)
    @mrn = params[:mrn].present? && params[:mrn] == "undefined" ? "" : params[:mrn]
    @item_type = params[:type].present? && params[:type] == "undefined" ? "" : params[:type]
    @sub_group = params[:sub_group].present? && params[:sub_group] == "undefined" ? "" : params[:sub_group]
  end

  def transaction_report
    sql = "SELECT fit.created_at+interval '5 hour' as dispensing_date_time, 
    fp.first_name, fp.last_name, fp.mrn as mrn, 
    fi.product_name, fi.strength, fit.quantity, 
    fu.first_name AS dispenser_first_name, fu.last_name AS dispenser_last_name, st.name as pharmacy_name
    FROM item_transactions as fit inner 
    join items as fi on fit.item_id = fi.id
    inner join patients as fp on fit.patient_id = fp.id
    INNER JOIN stores AS st ON st.id = fit.store_id
    inner join users as fu on fit.user_id = fu.id 
    WHERE fit.transaction_type = 'issue' 
    AND fit.created_at >= '#{@start_date}' 
    AND fit.created_at <= '#{@end_date}' 
    AND fit.medical_unit_id = #{@hospital_id} "
    sql += @store_id.present? ? "AND fit.store_id = #{@store_id} " : " "
    sql += @item_type.present? ? "AND fi.item_type = '#{@item_type}' " : " "
    sql += @medicine_id.present? ? "AND fi.id = #{@medicine_id} " : " "
    sql += @sub_group.present? ? "AND fi.sub_group = '#{@sub_group}' " : " "
    sql += @user_id.present? ? "AND fu.id = #{@user_id} " : " "
    sql += @mrn.present? ? "AND fp.mrn = '#{@mrn}' " : " "
    sql += "ORDER BY fit.created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(sql)
    return generate_transaction_report_csv(csv_data)
  end

  def list
    sql = "SELECT items.product_name AS item_name, 
    items.item_code, items.item_type, items.uom, items.upc, 
    items.price, items.strength, items.status FROM items 
    WHERE items.medical_unit_id = #{@hospital_id} 
    AND (status = 'Open' OR status = 'open') "
    sql += @user_id.present? ? "AND items.user_id = #{@user_id} " : " "
    sql += @item_type.present? ? "AND items.item_type = '#{@item_type}' " : " "
    sql += @medicine_id.present? ? "AND items.id = #{@medicine_id} " : " "
    sql += @sub_group.present? ? "AND items.sub_group = '#{@sub_group}' " : " "
    sql += "order by items.created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(sql)
    return generate_list_csv(csv_data)
  end

  def stock_report
    sql = "SELECT it.created_at+interval '5 hour' as created_at, it.receipt_number, i.item_code, 
      i.product_name, i.strength, it.open_stock, it.transaction_type,it.comment, st.name as pharmacy_name,
      it.quantity, u.first_name, u.last_name
      FROM item_transactions AS it 
      INNER JOIN items AS i ON i.id = it.item_id
      INNER JOIN users AS u ON u.id = it.user_id
      INNER JOIN stores AS st ON st.id = it.store_id
      WHERE it.medical_unit_id = #{@hospital_id} 
      AND it.quantity NOT IN ('')
      AND it.created_at+interval '5 hour' >= '#{@start_date}' 
      AND it.created_at+interval '5 hour' <= '#{@end_date}' "
    sql += @store_id.present? ? "AND it.store_id = #{@store_id} " : " "
    sql += @item_type.present? ? "AND i.item_type = '#{@item_type}' " : " "
    sql += @medicine_id.present? ? "AND it.item_id = #{@medicine_id} " : " "
    sql += @sub_group.present? ? "AND i.sub_group = '#{@sub_group}' " : " "
    sql += @user_id.present? ? "AND it.user_id = #{@user_id} " : " "
    sql += "ORDER BY i.item_code, it.created_at ASC;"
    csv_data = ActiveRecord::Base.connection.execute(sql)
    return generate_stock_report_csv(csv_data)
  end

  private

  def generate_transaction_report_csv(csv_data)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << ["Dispensing Date", "Dispensing Time", "MR Number", 
        "Patient Name", "Description of Item", "Strength", 
        "Quantity", "Dispenser", "Pharmacy Name" ]
      csv_data.each_with_index do |record, index|
        date = record["dispensing_date_time"].to_datetime.strftime("%d-%m-%Y")
        time = record["dispensing_date_time"].to_datetime.strftime("%H:%M:%S")
        csv << [date, time, record["mrn"], record["first_name"].to_s.gsub(";","")+record["last_name"].to_s.gsub(";",""), 
          record["product_name"], record["strength"], record["quantity"], 
          record["dispenser_first_name"].to_s+record["dispenser_last_name"].to_s,record["pharmacy_name"]]
      end
    end
    return csv
  end

  def generate_list_csv(csv_data)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << ["Item Name", "Item Code", "Item Type", "UOM", 
        "UPC", "Price", "Strength", "Status"]
      csv_data.each_with_index do |record, index|
        csv << [record["item_name"], record["item_code"],
          record["item_type"], record["uom"], record["upc"], 
          record["price"], record["strength"], record["status"]]
      end
    end
    return csv
  end

  def set_date_format(date)
    date.present? ? date.to_date.strftime("%d-%m-%Y") : date
  end

  def set_closing(data)
    closing = {}
    data.each do |record|
      if record["transaction_type"] == "issue"
        issued_stock = record["quantity"]
        add_stock = 0
        adjust_stock = 0
      elsif record["transaction_type"] == "add" 
        issued_stock = 0
        adjust_stock = 0
        add_stock = record["quantity"].to_f
      elsif record["transaction_type"] == "adjust"
        issued_stock = 0
        add_stock = 0
        adjust_stock = record["quantity"].to_f - record["open_stock"].to_f
      end
      if closing[record["item_code"]+"-issue"].blank?
        closing[record["item_code"]+"-issue"] = issued_stock.to_f
      else
        closing[record["item_code"]+"-issue"] += issued_stock.to_f
      end
      if closing[record["item_code"]+"-adjust"].blank?
        closing[record["item_code"]+"-adjust"] =  adjust_stock
      else
        closing[record["item_code"]+"-adjust"] += adjust_stock 
      end
      if closing[record["item_code"]+"-add"].blank?
        closing[record["item_code"]+"-add"] = add_stock.to_f
      else
        closing[record["item_code"]+"-add"] += add_stock.to_f
      end
    end
    return closing
  end

  def generate_stock_report_csv(csv_data)
    closing = set_closing(csv_data)
    total_count = 0
    next_opening = ''
    opening = ''
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      parameters = ["Dispensing Date", "Dispensing Time", 
        "Invoice Number", "Product Code", "Generic Name", 
        "Strength", "Opening Stock", "Issued Stock", "Add Stock", "Adjust Stock",
        "Closing Stock/On-Hand Stock", "Dispenser Name", "Remarks", "Pharmacy Name"]
      csv << parameters
      csv_data.each_with_index do |record, index|
        opening = record["open_stock"] if index == 0
        if index != 0 && record["item_code"] != csv_data[index-1]["item_code"]
          prev_record = csv_data[index-1]
          total_summary_array = []
          total_summary_array << "Summary"
          total_summary_array << "total_count = "+total_count.to_s
          total_summary_array << ""
          total_summary_array << prev_record["item_code"]
          total_summary_array << prev_record["product_name"]
          total_summary_array << prev_record["strength"]
          total_summary_array << opening.to_f
          total_summary_array << closing[prev_record["item_code"]+"-issue"]
          total_summary_array << closing[prev_record["item_code"]+"-add"]
          total_summary_array << closing[prev_record["item_code"]+"-adjust"]
          total_summary_array << opening.to_f - closing[prev_record["item_code"]+"-issue"] + closing[prev_record["item_code"]+"-add"]
          csv << total_summary_array
          total_count = 0
          opening = record["open_stock"].to_f
        end
        total_count += 1
        parameters_array = []
        parameters_array << record["created_at"].to_datetime.strftime("%d-%m-%Y")
        parameters_array << record["created_at"].to_datetime.strftime("%H:%M:%S")
        parameters_array << record["receipt_number"].to_s
        parameters_array << record["item_code"]
        parameters_array << record["product_name"]
        parameters_array << record["strength"]
        parameters_array << record["open_stock"].to_f
        if record["transaction_type"] == "issue"
          parameters_array << record["quantity"]
          parameters_array << "0"
          parameters_array << "0"
          parameters_array << record["open_stock"].to_f - record["quantity"].to_f
        elsif record["transaction_type"] == "add"
          parameters_array << "0"
          parameters_array << record["quantity"]
          parameters_array << "0"
          parameters_array << record["open_stock"].to_f + record["quantity"].to_f
        elsif record["transaction_type"] == "adjust"
          parameters_array << "0"
          parameters_array << "0"
          parameters_array << record["quantity"].to_f - record["open_stock"].to_f
          parameters_array << record["quantity"]
        end
        parameters_array << record["first_name"].to_s.gsub(";","") + record["last_name"].to_s.gsub(";","")
        parameters_array << record["comment"]
        parameters_array << record["pharmacy_name"]
        csv << parameters_array
      end
      # last record total
      if csv_data.count != 0
        last_record = csv_data[csv_data.count-1]
        total_summary_array = []
        total_summary_array << "Summary"
        total_summary_array << "total_count = "+total_count.to_s
        total_summary_array << ""
        total_summary_array << last_record["item_code"]
        total_summary_array << last_record["product_name"]
        total_summary_array << last_record["strength"]
        total_summary_array << opening.to_f
        total_summary_array << closing[last_record["item_code"]+"-issue"]
        total_summary_array << closing[last_record["item_code"]+"-add"]
        total_summary_array << closing[last_record["item_code"]+"-adjust"]
        total_summary_array << last_record["quantity"]
        # total_summary_array << opening.to_f - closing[last_record["item_code"]+"-issue"] + closing[last_record["item_code"]+"-add"]
        csv << total_summary_array
      end
    end
    return csv
  end
end