class Reports::Registration
  def initialize(params, hospital)
    # @start_date = params[:from]
    # @end_date = params[:end_date]
    start_arr = params[:from].split(" ")
    date_split = start_arr[0].split("/")
    @start_date = date_split[2]+'-'+date_split[0]+'-'+date_split[1]+' '+start_arr[1]
    end_arr = params[:end_date].split(" ")
    date_split = end_arr[0].split("/")
    @end_date = date_split[2]+'-'+date_split[0]+'-'+date_split[1]+' '+end_arr[1]
    @department = params[:department].present? && params[:department] == "undefined" ? "" : params[:department]
    @hospital_id = params[:medical_unit_id].present? ? params[:medical_unit_id] : hospital.try(:id)
    @service = params[:service].present? && params[:service] == "undefined" ? '' : params[:service]
    @mrn = params[:mrn].present? && params[:mrn] == "undefined" ? '' : params[:mrn]
    @user_id = params[:user_id].present? && params[:user_id] == "undefined" ? '' : params[:user_id]
    @city = params[:city].present? && params[:city] == "undefined" ? '' : params[:city]
    # @follow_up = params[:is_follow_up]
    @mlc = params[:mlc]
    @services = params[:is_service]
  end

  def transaction_report

    sql = "SELECT Date(v.created_at+interval '5 hour') created_at_date,v.created_at,p.mrn,
    CASE WHEN v.current_referred_to IS NULL THEN v.referred_to ELSE v.current_referred_to END AS referred_to ,
    (CASE WHEN referred_to = 'Outpatient' THEN d.title ELSE referred_to end) As department_name,
    p.first_name, p.last_name, p.age,p.gender, p.near_by_city,
    u.first_name || ' ' || u.last_name AS user,
    p.guardian_relationship AS patient_guardian_relationship,
    p.guardian_first_name AS patient_guardian_name,
    (CASE WHEN p.address1 IS NULL THEN 'No Address' ELSE p.address1 end) AS patient_address,
    (CASE WHEN v.mlc IS NULL THEN 'No' ELSE 'Yes' end) as is_mlc,
    (CASE WHEN v.ambulance_number IS NULL THEN 'No' ELSE 'Yes' end) as is_ambulance_acquired 
    FROM visits as v 
    INNER JOIN patients as p ON v.patient_id = p.id 
    INNER JOIN users as u ON v.user_id = u.id
    LEFT OUTER JOIN departments as d ON v.department_id = d.id
    where Date(v.created_at + interval '5 hour') >= '#{@start_date}'
    AND Date(v.created_at + interval '5 hour') <= '#{@end_date}'
    AND v.medical_unit_id = #{@hospital_id}"
    sql += @department.present? ? "AND referred_to = '#{@department}' " : " "
    sql += @mrn.present? ? "AND p.mrn = '#{@mrn}' " : " "
    sql += @city.present? ? "AND p.near_by_city = '#{@city}' " : " "
    sql += @user_id.present? ? "AND v.user_id = #{@user_id} " : " "
    if @mlc == 'true'
      sql += @mlc.present? ? "AND v.mlc = 'true' " : " "
    end
    if @services == 'true'
      sql += @services.present? ? "AND v.ambulance_number IS NOT NULL " : " "
    end
    sql += "ORDER BY v.created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(sql)
    return generate_registration_csv(csv_data)
  end

  private

  def generate_registration_csv(csv_data)
    count = 0
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << ["Visit Date","Visit Time","MR Number", "Patient Name", "Age","Gender",
        "Description of Service", "Department","Department Name","User", "City", 
        "MLC", "Services","Guardian Relationship","Guardian Name","Patient Address"]
      csv_data.each_with_index do |record, index|
        begin
        date = record["created_at_date"]
        time =  (Time.parse(Time.parse(record["created_at"]).strftime("%I:%M %P"))) + 5.hour
        csv << [date,time.strftime("%I:%M %P"),record["mrn"], record["first_name"].to_s.gsub(";","")+record["last_name"].to_s.gsub(";",""),
          record["age"], record["gender"],
          record["description_of_services"], record["referred_to"],record["department_name"],
          record["user"].to_s.gsub(";",""), record["near_by_city"], 
          record["is_mlc"], record["is_ambulance_acquired"],
          record["patient_guardian_relationship"],record["patient_guardian_name"],
          record["patient_address"]]
        rescue Exception => e
          count = count+1
          puts(count)
        end
      end
    end
    return csv
  end
end