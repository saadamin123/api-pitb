namespace :dummy_order do
  desc "TODO"
  task :generate, [:n]  => [:environment] do |t, args|
    medical_units = []
    medical_units << MedicalUnit.where(title: 'Services Hospital').first
    medical_units << MedicalUnit.where(title: 'Mayo Hospital').first
    medical_units << MedicalUnit.where(title: 'Sir Ganga Ram Hospital').first
    
    dr = Role.where(title: 'Dr').first
    
    medical_units.each do |medical_unit|
      doctors = []
      (args[:n].to_i).times do
        user = User.create!(username: Faker::Internet.user_name, 
                   email: Faker::Internet.email, 
                   role_id: dr.id, 
                   password: Faker::Internet.password, 
                   first_name: Faker::Name.first_name, 
                   last_name: Faker::Name.last_name, 
                   phone: Faker::PhoneNumber.phone_number, 
                   nic: Faker::Number.between(1, 10), 
                   dob: Date.new)
        MedicalUnitUser.create!(medical_unit_id: medical_unit.id , user_id: user.id, role_id: dr.id) 
        doctors << user
      end

      (-1..90).each do |date|
        puts date
        puts '----------------'
        doctors.each do |doctor|
          visits = Visit.where('medical_unit_id = ? and created_at > ?', medical_unit.id, date.days.ago.beginning_of_day)
          if visits.count > 0
            random = Faker::Number.between(1,10)
            # randomly pick patients and generate order
            (0..random).each do |i|
              visit =  visits[Faker::Number.between(0,visits.length-1)]
              Order.create!(
                medical_unit_id: medical_unit.id,
                patient: visit.patient,
                visit: visit,
                doctor_id: doctor.id,
                device: Device.first,
                status: 'Open',
                order_type: 'Admission',
                created_at: date.day.ago,
                updated_at: date.day.ago
              )
            end
          end
        end
      end
    end
  end
end
