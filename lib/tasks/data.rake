require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/html_outputter'
require 'barby/outputter/pdfwriter_outputter'
require 'barby/outputter/png_outputter'
require 'barby/outputter/cairo_outputter'
require 'rqrcode'
require 'time'

namespace :data do
  desc "generate barcode"
  task :code, [:n]  => [:environment] do |t, args|
    Patient.all.order('id desc').each do |patient|
      
      cdate = patient.created_at.strftime("%Y%m%d")
      puts cdate
      begin
        cdate_dir = Dir.mkdir(Rails.root.join("public/barcode/#{cdate}")) unless Dir.exist?(Rails.root.join("public/barcode/#{cdate}"))
        Dir.mkdir(Rails.root.join("public/qrcode/#{cdate}")) unless Dir.exist?(Rails.root.join("public/qrcode/#{cdate}"))
      rescue
      end        
        barcode = Barby::Code128.new(patient.mrn)
        qrcode = RQRCode::QRCode.new(patient.mrn)

      begin  
        full_path = Rails.root.join("public/barcode/#{cdate}","barcode_#{patient.mrn}.png")
        File.open(full_path, 'w') { |f| f.write barcode.to_png }
        puts 'writing bar code'
        png = qrcode.as_png(
          resize_gte_to: false,
          resize_exactly_to: false,
          fill: 'white',
          color: 'black',
          size: 120,
          border_modules: 4,
          module_px_size: 6,
          
          file: Rails.root.join("public/qrcode/#{cdate}","qrcode_#{patient.mrn}.png")
          )
          puts "#{patient.mrn} bar code generated"
      rescue
        puts 'unable to create directry '
      end
    end
  end

  desc "update qrcode to vcard"
  task :update_lab_qrcode_to_vcard  => [:environment] do |t, args|
    logger = Logger.new("log/barcode_qrcode.log")
    Patient.order(id: :asc).limit(10000).each do |patient|
      logger.info("==============================")
      logger.info("patient id = #{patient.id} ======= patient mrn = #{patient.mrn}")
      patient.update_qrcode_to_vcard
      logger.info("DONE with patient")
      puts "#{patient.id}"
    end
  end

  task :update_mrn  => [:environment] do |t, args|
    Patient.where("length(mrn) = 13").order('id desc').each do |patient|
      patient.update_attributes(mrn: patient.mrn.first(12))
      puts "#{patient.id}"
    end
  end

  desc "reset all lab_sequence to '0000'."
  task :reset_lab_sequence  => [:environment] do |t, args|
    LabOrderSequence.all.each do |lab_sequence|
      lab_sequence.update_attributes(lab_sequence: '0000')
      puts "#{lab_sequence}"
    end
  end

  desc "Set opd count"
  task :set_opd_count  => [:environment] do |t, args|
    opd_count = ''
    visit_count = Visit.where("medical_unit_id NOT IN  (?) AND Date(visits.created_at + interval '5 hour') >=? AND (current_referred_to = ? OR referred_to = ?)", [43, 85, 54, 15, 87, 45, 27], Time.zone.now.to_date, 'Outpatient','Outpatient').count
    $redis.set('opd_count', visit_count)
  end

  desc "Set emr count"
  task :set_emr_count  => [:environment] do |t, args|
    opd_count = ''
    visit_count = Visit.where("medical_unit_id NOT IN  (?) AND Date(visits.created_at + interval '5 hour') >= ? AND (current_referred_to = ? OR referred_to = ?)", [43, 85, 54, 15, 87, 45, 27], Time.zone.now.to_date, 'Emergency','Emergency').count
    $redis.set('emr_count', visit_count)
  end

  desc "Set total visits count of today"
  task :set_total_today_visit_count  => [:environment] do |t, args|
    opd_count = ''
    visit_count = Visit.where("medical_unit_id NOT IN  (?) AND Date(visits.created_at + interval '5 hour') >= ?", [43, 85, 54, 15, 87, 45, 27], Time.zone.now.to_date).count
    $redis.set('total_visit_count', visit_count)
  end

  desc "Set total visits count"
  task :set_all_visit_count  => [:environment] do |t, args|
    opd_count = ''
    visit_count = Visit.where("medical_unit_id NOT IN  (?)", [43, 85, 54, 15, 87, 45, 27]).count
    $redis.set('all_visit', visit_count)
  end

  desc "Set total lab tests count"
  task :set_all_lab_tests_count  => [:environment] do |t, args|
    opd_count = ''
    lab_tests_count = LabOrderDetail.count
    $redis.set('all_lab_tests_count', lab_tests_count)
  end

  desc "Set total patients count"
  task :set_all_patients_count  => [:environment] do |t, args|
    opd_count = ''
    patient_count = Visit.where("medical_unit_id NOT IN  (?)", [43, 85, 54, 15, 87, 45, 27]).pluck(:patient_id).uniq.count
    $redis.set('all_patient_count', patient_count)
  end

  desc "Set total medicines count"
  task :set_all_medicines_count  => [:environment] do |t, args|
    opd_count = ''
    medicines_count = ItemTransaction.where(transaction_type: 'issue').count(:visit_id)
    $redis.set('all_medicines_count', medicines_count)
  end

  desc "Set all items status to open."
  task :item_status_open  => [:environment] do |t, args|
    Item.update_all(status: "Open")
  end

  desc "Refresh Materalized Views"
  task :refresh_mat_views  => [:environment] do |t, args|
    sql = 'select public.refreshallmaterializedviews()'
    ActiveRecord::Base.connection.execute(sql)
  end

  desc "Update History Tables"
  task :update_history_tables  => [:environment] do |t, args|
    sql = 'select public.updateallhistorytables()'
    ActiveRecord::Base.connection.execute(sql)
  end

  # How to call it 
  # rake data:lab_data['sample_lab_data_1.csv']
  # desc "Set all items status to open."
  # task :lab_data, [:file_name] => [:environment] do |t, args|
  #   require 'csv'
  #   file_path = Rails.root+args[:file_name]
  #   csv_file = File.read(file_path)
  #   csv = CSV.parse(csv_file, :headers => true)
  #   csv.each do |row|
  #     hospital = MedicalUnit.find_by(title: row["hospital"])
  #     dept = Department.where(title: hospital.title+" Lab", medical_unit_id: hospital.id, parent_id: nil).first_or_create!
  #     lab_section = dept.sub_departments.where(title: hospital.title+' '+row["lab_section"].downcase, medical_unit_id: hospital.id, department_code: row["lab_section_code"]).first_or_create!
  #     lit = LabInvestigationType.where(title: row["lab_section"]).first_or_create!
  #     if "years".include?(row["date_unit"].downcase)
  #       age_start = row["age_start"].to_i * 365
  #       age_end = row["age_end"].to_i * 365
  #     elsif "months".include?(row["date_unit"].downcase)
  #       age_start = row["age_start"].to_f * (365.0/12.0)
  #       age_end = row["age_end"].to_f * (365.0/12.0)
  #     else
  #       age_start = row["age_start"].to_i
  #       age_end = row["age_end"].to_i
  #     end
  #     p row
  #     if hospital.present?
  #       li = LabInvestigation.where(
  #         medical_unit_id: hospital.id,
  #         profile_name: row["profile_name"],
  #         lab_investigation_type_id: lit.id,
  #         profile_code: row["profile_code"],
  #         status: "true",
  #         department_id: lab_section.id,
  #         report_type: row["report_type"]
  #       ).first_or_create!
  #       la = li.lab_assessments.where(
  #         title: row["parameter"],
  #         uom: row["uom"],
  #         specimen: row["specimen"],
  #         status: "true",
  #         result_type1: row["result_type1"],
  #         result_type2: row["result_type2"],
  #         medical_unit_id: hospital.id,
  #         parameter_code: row["parameter_code"],
  #       ).first_or_create!
  #       lar = la.lab_access_ranges.where(
  #         gender: row["gender"],
  #         age_start: age_start.round,
  #         age_end: age_end.round,
  #         date_unit: 'days',
  #         start_range: row["start_range"],
  #         end_range: row["end_range"],
  #         range_title: row["range_title"],
  #         status: "true",
  #         heading: row["heading"]
  #       ).first_or_create!
  #     end
  #   end
  #   # file_path = Rails.root+"sample_lab_data_price.csv"
  #   # csv_file = File.read(file_path)
  #   # csv = CSV.parse(csv_file, :headers => true)
  #   # csv.each do |row|
  #   #   hospital = MedicalUnit.find_by(title: row["hospital"])
  #   #   li = LabInvestigation.find_by(profile_name: row["profile_name"])
  #   #   LabInvestigationPrice.where(
  #   #     medical_unit_id: hospital.try(:id),
  #   #     department_name: row["department"],
  #   #     lab_investigation_id: li.try(:id),
  #   #     price: row["price"]
  #   #   ).first_or_create!
  #   # end
  # end

  # How to call it 
  # rake data:new_lab_data['Lab-CSV/sample_lab_data_1.csv']
  desc "Set all items status to open."
  task :new_lab_data, [:file_name] => [:environment] do |t, args|
    require 'csv'
    file_path = Rails.root + args[:file_name]
    csv_file = File.read(file_path)
    csv = CSV.parse(csv_file, :headers => true)
    csv.each do |row|
      hospital = MedicalUnit.find_by(title: row["hospital"])
      dept = Department.where(title: hospital.title+" Lab", medical_unit_id: hospital.id, parent_id: nil).first_or_create!
      lab_section = Department.where(title: hospital.title+' '+row["lab_section"].downcase, medical_unit_id: hospital.id, department_code: row["lab_section_code"])
      lab_section = lab_section.first if lab_section.present?
      if lab_section.blank?
       lab_section = dept.sub_departments.create(title: hospital.title+' '+row["lab_section"].downcase, medical_unit_id: hospital.id, department_code: row["lab_section_code"])
      end
      lit = LabInvestigationType.where(title: row["lab_section"].downcase).first_or_create!
      if "years".include?(row["date_unit"].downcase)
        age_start = row["age_start"].to_i * 365
        age_end = row["age_end"].to_i * 365
      elsif "months".include?(row["date_unit"].downcase)
        age_start = row["age_start"].to_f * (365.0/12.0)
        age_end = row["age_end"].to_f * (365.0/12.0)
      else
        age_start = row["age_start"].to_i
        age_end = row["age_end"].to_i
      end
      p row
      if hospital.present?
        p "1111111111111111111"
        li = LabInvestigation.where(
          medical_unit_id: hospital.id,
          profile_name: row["profile_name"],
          lab_investigation_type_id: lit.id,
          profile_code: row["profile_code"],
          status: "true",
          department_id: lab_section.id,
          report_type: row["report_type"]
        ).first_or_create!
        p "22222222222222222"
        la = LabAssessment.where(parameter_code: row["parameter_code"]).first_or_initialize
        if la.new_record?
          la.title = row["parameter"]
          la.uom = row["uom"]
          la.specimen = row["specimen"]
          la.status = "true"
          la.result_type1 = row["result_type1"]
          la.result_type2 = row["result_type2"]
          la.machine_code = row["machine_code"]
          la.machine_parameter_code = row["machine_parameter_code"]
          la.save
        end
        p "333333333333333333333333333"
        li.lab_assessments << la unless li.lab_assessments.include?(la)

        lar = la.lab_access_ranges.where(
          gender: row["gender"],
          age_start: age_start.round,
          age_end: age_end.round,
          date_unit: 'Day',
          start_range: row["start_range"],
          end_range: row["end_range"],
          range_title: row["range_title"],
          status: "true",
          medical_unit_id: hospital.id,
          lab_investigation_id: li.id
        ).first_or_create!
      end
    end
  end

  desc "barcode qrcode created and push to s3."
  task :barcode_qrcode_on_s3  => [:environment] do |t, args|
    logger = Logger.new("log/barcode_qrcode.log")
    Patient.where("barcode_url is null or qrcode_url is null").each do |patient|
      logger.info("==============================")
      logger.info("patient id = #{patient.id} ======= patient mrn = #{patient.mrn}")
      patient.barcode_qrcode
      logger.info("DONE with patient")
    end
  end

  desc "Public Activity"
  task :first_activity  => [:environment] do |t, args|
    # Patient.all.each do |patient|
    query = "insert into activities 
    (trackable_id, trackable_type, owner_id, owner_type, key, created_at, updated_at) 
    select distinct on (patients.id) patients.id, 'Patient' as trackable_type, 
    visits.user_id, 'User' as owner_type, 'patient.create' as key, 
    current_timestamp, current_timestamp 
    FROM patients 
    INNER JOIN visits ON patients.id=visits.patient_id;"
    result = ActiveRecord::Base.connection.execute(query)
    # Patient.includes(:visits).each do |patient|
    #   if patient.visits.present? && patient.visits.first.user_id.present?
    #     activity = patient.activities.create!(trackable_id: patient.id, 
    #                trackable_type: "Patient", 
    #                owner_id: patient.visits.first.user_id, 
    #                owner_type: "User", 
    #                key: "patient.create")
    #     p "created #{patient.id}"
    #   end
    # end
  end

  desc "TODO"
  task :generate, [:n]  => [:environment] do |t, args|
    medical_units = []
    medical_units << MedicalUnit.where(title: 'Mayo Hospital').first
    medical_units << MedicalUnit.where(title: 'Sir Ganga Ram Hospital').first
    medical_units << MedicalUnit.where(title: 'Services Hospital').first

    referred_to  = ['Emergency', 'Inpatient', 'OutPatient', 'Checkup']
    visitreasons = ['Injury', 'Diarrhea', 'Pregnancy', 'Congestion', 'Headache', 'Chest Pain', 'Ear Pain/Infection', 'Respiration', 'Dental']
    modeofconveyances = ['walk-in', 'Public Transport', 'Private Transport', '1122', 'Hospital ambulance', 'Private ambulance']
    
    fdo = Role.where(title: 'fdo').first
    ms = Role.where(title: 'MS').first
    
    medical_units.each do |medical_unit|
      user = User.create!(username: Faker::Internet.user_name, 
                 email: Faker::Internet.email, 
                 role_id: fdo.id, 
                 password: Faker::Internet.password, 
                 first_name: Faker::Name.first_name, 
                 last_name: Faker::Name.last_name, 
                 phone: Faker::PhoneNumber.phone_number, 
                 nic: Faker::Number.between(1, 10), 
                 dob: Date.new)

      user2 = User.create!(username: Faker::Internet.user_name, 
                 email: Faker::Internet.email, 
                 role_id: ms.id, 
                 password: Faker::Internet.password, 
                 first_name: Faker::Name.first_name, 
                 last_name: Faker::Name.last_name, 
                 phone: Faker::PhoneNumber.phone_number, 
                 nic: Faker::Number.between(1, 10), 
                 dob: Date.new)
     MedicalUnitUser.create!(medical_unit_id: medical_unit.id , user_id: user.id, role_id: fdo.id) 
     MedicalUnitUser.create!(medical_unit_id: medical_unit.id , user_id: user2.id, role_id: ms.id) 
    end

    

    (-1..5).each do |date|
      puts date
      puts '----------------'
      (args[:n].to_i).times do
        medical_unit = medical_units[Faker::Number.between(0,2)]
        referr = referred_to[Faker::Number.between(0,2)]
        reason = visitreasons[Faker::Number.between(0,8)]
        mode   = modeofconveyances[Faker::Number.between(0,5)]

        patient = Patient.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, gender: 'male',
                   patient_nic: Faker::Number.number(10), guardian_nic: Faker::Number.number(10),
                   patient_passport: Faker::Number.between(1, 10),
                   guardian_passport: Faker::Number.between(1, 10),
                   birth_day: Faker::Number.between(1, 30),
                   birth_month: Faker::Number.between(3, 12),
                   birth_year: Faker::Number.between(1970, 2015),
                   unidentify_patient: false,
                   guardian_relationship: Faker::Name.name,
                   guardian_first_name: Faker::Name.first_name,
                   guardian_last_name: Faker::Name.last_name,
                   unidentify_guardian: false,
                   marital_status: 'Married', state: Faker::Address.state,
                   city: Faker::Address.city, near_by_city: Faker::Address.city,
                   address1: Faker::Address.street_address,
                   address2: Faker::Address.secondary_address, phone1_type: 'mobile',
                   phone1: Faker::PhoneNumber.cell_phone, phone2_type: 'mobile',
                   phone2: Faker::PhoneNumber.cell_phone, phone3_type: 'phone',
                   phone3: Faker::PhoneNumber.phone_number,
                   registered_at: medical_unit.id,
                   registered_by: medical_unit.users.where(role_id: fdo.id).first.id,
                   created_at: date.days.ago,
                   updated_at: date.days.ago
                   )
        
        patient.visits.create(referred_to: referr, 
                              reason: reason, 
                              mode_of_conveyance: mode,
                              ref_department: 'ICU',
                              medical_unit_id: medical_unit.id, 
                              patient_id: patient.id, 
                              user_id: medical_unit.users.where(role_id: fdo.id).first.id,
                              created_at: date.days.ago,
                              updated_at: date.days.ago
                              )
      end
    end
  end

  desc "import Patients and their visits CSV from s3"
  task :import_patients_csv_from_s3  => [:environment] do |t, args|
    logger = Logger.new("log/import_patients_csv_from_s3.log")
    data_files = DataFile.where(status: "uploaded")
    if data_files.length > 0
      data_files.each do |datafile|
        if datafile.download
          datafile.update_attributes(status: "downloaded")
        end
        logger.info("DONE with csv")
      end
    end
  end

  desc "import Patients and their visits data from CSV"
  task :import_patients_data_from_csv  => [:environment] do |t, args|
    logger = Logger.new("log/import_Patients_and_their_visits_data_from_CSV.log")
    require 'csv'
    files = Dir.glob("#{Rails.root}/tmp/csv_files/**/*").sort_by{ |f| File.ctime(f) }
    files.each do |file|
      @data_file = DataFile.find_by(id: file.delete("^0-9"))
      puts "-----------------------------#{@data_file.id}-----------------------------------------------------"
      begin
      if @data_file && @data_file.status == 'downloaded'
          csv_text = File.read(file).scrub
          @data_file.update_attributes(status: "processing")
          csv = CSV.parse(csv_text, :headers => true)
          csv.each do |row|
            if row['patient_id']
              @patient = Patient.find_by_external_patient_id(row['patient_id'])
              if !@patient
                @patient = Patient.new(Patient.fetch_patient_date_from_csv(row))
                @patient.generate_new_mrn(@data_file.medical_unit)
                @patient.registered_by = @data_file.user.id
                @patient.registered_at = @data_file.medical_unit

                if @patient.check_patient_age_and_dob
                  @patient.save!
                end
                puts "#{@patient.mrn}"
              end
              @visit = @patient.visits.find_by_external_visit_number(row['visit_number']) if @patient.check_patient_age_and_dob
              if !@visit
                @visit = Visit.fetch_visits_from_csv(row)
                @visit.patient = @patient
                @visit.created_by = @data_file.user
                @visit.medical_unit = @data_file.medical_unit
                @visit.save!
              end
            end
          end
          @data_file.update_attributes(status: "processed")
          FileUtils.rm_rf Dir.glob(file)
      elsif @data_file && @data_file.status == 'processed'
        FileUtils.rm_rf Dir.glob(file)
      end
      rescue => e
        puts "------------------#{e}---------------"
        FileUtils.rm_rf Dir.glob(file)
        @data_file.update_attributes(status: "failed") if @data_file
        logger.info("Failed! Can not save data from file ")
      end
    end
  end

end
