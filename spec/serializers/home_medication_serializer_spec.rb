require 'rails_helper'

RSpec.describe HomeMedicationSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }
  it { should have_one(:patient) }
  it { should have_one(:medical_unit) }
  it { should have_one(:visit) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:home_medication) }

    let(:serialization) { HomeMedicationSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['home_medication']
    end

    it 'has a dosage' do
      expect(subject['dosage']).to eql(resource.dosage)
    end
    it 'has a unit' do
      expect(subject['unit']).to eql(resource.unit)
    end
    it 'has a route' do
      expect(subject['route']).to eql(resource.route)
    end
    it 'has a frequency' do
      expect(subject['frequency']).to eql(resource.frequency)
    end
    it 'has a usage' do
      expect(subject['usage']).to eql(resource.usage)
    end
    it 'has a instruction' do
      expect(subject['instruction']).to eql(resource.instruction)
    end
  end
end
