# == Schema Information
#
# Table name: feedbacks
#
#  id              :integer          not null, primary key
#  complaint_id    :integer
#  medical_unit_id :integer
#  user_id         :integer
#  device_id       :integer
#  satisfaction    :string
#  message         :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string
#

require 'rails_helper'

RSpec.describe FeedbackSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:feedback) }

    let(:serialization) { FeedbackSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['feedback']
    end

    it 'has a complaint_id' do
      expect(subject['complaint_id']).to eql(resource.complaint_id)
    end
    it 'has a satisfaction' do
      expect(subject['satisfaction']).to eql(resource.satisfaction)
    end
    it 'has a status' do
      expect(subject['status']).to eql(resource.status)
    end
    it 'has a message' do
      expect(subject['message']).to eql(resource.message)
    end
  end
end
