# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  category   :string
#  name       :string
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe RegionSerializer, type: :serializer do

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:region) }

    let(:serialization) { RegionSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['region']
    end

    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
    it 'has a name' do
      expect(subject['name']).to eql(resource.name)
    end
    it 'has a parent_id' do
      expect(subject['parent_id']).to eql(resource.parent_id)
    end
  end
end
