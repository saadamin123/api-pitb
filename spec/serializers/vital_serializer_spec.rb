require 'rails_helper'

RSpec.describe VitalSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }
  it { should have_one(:patient) }
  it { should have_one(:medical_unit) }
  it { should have_one(:visit) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:vital) }

    let(:serialization) { VitalSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['vital']
    end

    it 'has a weight' do
      expect(subject['weight']).to eql(resource.weight)
    end
    it 'has a height' do
      expect(subject['height']).to eql(resource.height)
    end
    it 'has a temperature' do
      expect(subject['temperature']).to eql(resource.temperature)
    end
    it 'has a bp_systolic' do
      expect(subject['bp_systolic']).to eql(resource.bp_systolic)
    end
    it 'has a bp_diastolic' do
      expect(subject['bp_diastolic']).to eql(resource.bp_diastolic)
    end
    it 'has a pulse' do
      expect(subject['pulse']).to eql(resource.pulse)
    end
    it 'has a resp_rate' do
      expect(subject['resp_rate']).to eql(resource.resp_rate)
    end
    it 'has a o2_saturation' do
      expect(subject['o2_saturation']).to eql(resource.o2_saturation)
    end
    it 'has a head_circ' do
      expect(subject['head_circ']).to eql(resource.head_circ)
    end
  end
end
