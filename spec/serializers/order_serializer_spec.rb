require 'rails_helper'

RSpec.describe OrderSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:radiology_order_detail) }
  it { should have_one(:procedure_order_detail) }
  it { should have_one(:lab_order_detail) }
  it { should have_one(:nursing_order_detail) }
  it { should have_many(:order_items) }
  it { should have_many(:admission_order_details) }
  it { should have_many(:consultation_order_details) }
  it { should have_many(:lab_test_details) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:order) }

    subject { described_class.new(resource) } 
    it 'has a id' do
      expect(subject.id).to eql(resource.id)
    end
    it 'has a patient_id' do
      expect(subject.patient_id).to eql(resource.patient_id)
    end
    it 'has a doctor_id' do
      expect(subject.last_name).to eql(resource.last_name)
    end
    it 'has a status' do
      expect(subject.status).to eql(resource.status)
    end
    it 'has a medical_unit_id' do
      expect(subject.medical_unit_id).to eql(resource.medical_unit_id)
    end
    it 'has a device_id' do
      expect(subject.device_id).to eql(resource.device_id)
    end
    it 'has a doctor_name' do
      expect(subject.doctor_name).to eql(resource.doctor_name)
    end
    it 'has a patient_id' do
      expect(subject.patient_id).to eql(resource.patient_id)
    end
    it 'has a patient_name' do
      expect(subject.patient_name).to eql(resource.patient_name)
    end
    it 'has a patient_mrn' do
      expect(subject.patient_mrn).to eql(resource.patient_mrn)
    end
    it 'has a order_type' do
      expect(subject.order_type).to eql(resource.order_type)
    end
    it 'has a cancel_reason' do
      expect(subject.cancel_reason).to eql(resource.cancel_reason)
    end
    it 'has a cancel_reason_details' do
      expect(subject.cancel_reason_details).to eql(resource.cancel_reason_details)
    end
    it 'has a patient_birth_day' do
      expect(subject.patient_birth_day).to eql(resource.patient_birth_day)
    end
    it 'has a patient_birth_month' do
      expect(subject.patient_birth_month).to eql(resource.patient_birth_month)
    end
    it 'has a patient_birth_year' do
      expect(subject.patient_birth_year).to eql(resource.patient_birth_year)
    end
  end
end
