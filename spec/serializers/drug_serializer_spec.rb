require 'rails_helper'

RSpec.describe DrugSerializer, type: :serializer do
	
  context 'Individual Resource Representation' do

    let(:resource) { FactoryGirl.create(:drug) }

    let(:serialization) { DrugSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['drug']
    end

    it 'has a name' do
      expect(subject['name']).to eql(resource.name)
    end
    it 'has a overview' do
      expect(subject['overview']).to eql(resource.overview)
    end
    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
  end
end
