# == Schema Information
#
# Table name: medical_units
#
#  id                    :integer          not null, primary key
#  region_id             :integer
#  title                 :string
#  identification_number :string
#  location              :string
#  latitude              :float
#  longitude             :float
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  category              :string
#

require 'rails_helper'

RSpec.describe MedicalUnitSerializer, type: :serializer do
  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:medical_unit) }

    let(:serialization) { MedicalUnitSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['medical_unit']
    end

    it 'has a region_id' do
      expect(subject['region_id']).to eql(resource.region_id)
    end
    it 'has a title' do
      expect(subject['title']).to eql(resource.title)
    end
    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
  end
end
