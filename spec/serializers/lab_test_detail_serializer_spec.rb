require 'rails_helper'

RSpec.describe LabTestDetailSerializer, type: :serializer do

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:lab_test_detail) }

    subject { described_class.new(resource) } 

    it 'has a lab_test_detail_id' do
      expect(subject.id).to eql(resource.id)
    end
    it 'has a lab_test_detail_caption' do
      expect(subject.caption).to eql(resource.caption)
    end
    it 'has a lab_test_detail_range' do
      expect(subject.range).to eql(resource.range)
    end
    it 'has a lab_test_detail_unit' do
      expect(subject.unit).to eql(resource.unit)
    end
    it 'has a lab_test_detail_value' do
      expect(subject.value).to eql(resource.value)
    end
    it 'has a lab_test_detail_comments' do
      expect(subject.comments).to eql(resource.comments)
    end
    it 'has a lab_test_detail_created_at' do
      expect(subject.created_at).to eql(resource.created_at)
    end
    it 'has a lab_test_detail_updated_at' do
      expect(subject.updated_at).to eql(resource.updated_at)
    end
  end
end
