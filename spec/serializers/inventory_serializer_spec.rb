require 'rails_helper'

RSpec.describe InventorySerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:brand_drug) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:inventory) }

    let(:serialization) { InventorySerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['inventory']
    end

    # it 'has a quantity' do
    #   expect(subject['quantity']).to eql(resource.quantity)
    # end
  end
end
