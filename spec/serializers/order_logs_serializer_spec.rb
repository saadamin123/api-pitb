require 'rails_helper'

RSpec.describe OrderLogSerializer, type: :serializer do

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:order_log) }

    let(:serialization) { OrderLogSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['order_log']
    end

    it 'has a id' do
      expect(subject['id']).to eql(resource.id)
    end
    it 'has a status' do
      expect(subject['status']).to eql('Ordered')
    end
    it 'has a updated_by' do
      expect(subject['updated_by']).to eql(resource.updated_by)
    end
  end
end
