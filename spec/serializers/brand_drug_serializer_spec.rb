require 'rails_helper'

RSpec.describe BrandDrugSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:brand) }
  it { should have_one(:drug) }

	
  context 'Individual Resource Representation' do

    let(:resource) { FactoryGirl.create(:brand_drug) }

    let(:serialization) { BrandDrugSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['brand_drug']
    end

    # it 'has a name' do
    #   expect(subject['name']).to eql(resource.name)
    # end
    # it 'has a category' do
    #   expect(subject['category']).to eql(resource.category)
    # end
    # it 'has a form' do
    #   expect(subject['form']).to eql(resource.form)
    # end
    # it 'has a dumb' do
    #   expect(subject['dumb']).to eql(resource.dumb)
    # end
    # it 'has a packing' do
    #   expect(subject['packing']).to eql(resource.packing)
    # end
    # it 'has a trade_price' do
    #   expect(subject['trade_price']).to eql(resource.trade_price)
    # end
    # it 'has a retail_price' do
    #   expect(subject['retail_price']).to eql(resource.retail_price)
    # end
    # it 'has a mg' do
    #   expect(subject['mg']).to eql(resource.mg)
    # end
    # it 'has a drug_id' do
    #   expect(subject['drug_id']).to eql(resource.drug_id)
    # end
  end
end
