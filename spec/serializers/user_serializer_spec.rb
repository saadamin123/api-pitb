# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  role_id                :integer
#  username               :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  title                  :string
#  phone                  :string
#  nic                    :string
#  dob                    :date
#  father_name            :string
#  father_nic             :string
#  mother_name            :string
#  mother_nic             :string
#  address1               :string
#  authentication_token   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  failed_attempts        :integer          default("0"), not null
#  unlock_token           :string
#  locked_at              :datetime
#  address2               :string
#

require 'rails_helper'

RSpec.describe UserSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:role) }
  it { should have_one(:device) }
  it { should have_many(:medical_units) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:user) }

    let(:serialization) { UserSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['user']
    end

    it 'has a role_id' do
      expect(subject['role_id']).to eql(resource.role_id)
    end
    it 'has a first_name' do
      expect(subject['first_name']).to eql(resource.first_name)
    end
    it 'has a last_name' do
      expect(subject['last_name']).to eql(resource.last_name)
    end
    it 'has a phone' do
      expect(subject['phone']).to eql(resource.phone)
    end
    it 'has a nic' do
      expect(subject['nic']).to eql(resource.nic)
    end
    # it 'has a dob' do
    #   expect(subject['dob']).to eql(resource.dob)
    # end
    it 'has a email' do
      expect(subject['email']).to eql(resource.email)
    end
  end
end
