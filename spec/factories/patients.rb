# == Schema Information
#
# Table name: patients
#
#  id                    :integer          not null, primary key
#  registered_by         :integer
#  registered_at         :integer
#  mrn                   :string
#  first_name            :string
#  last_name             :string
#  middle_name           :string
#  gender                :string
#  education             :string
#  birth_day             :integer
#  birth_month           :integer
#  birth_year            :integer
#  patient_nic           :string
#  patient_passport      :string
#  unidentify_patient    :boolean          default("false")
#  guardian_relationship :string
#  guardian_first_name   :string
#  guardian_last_name    :string
#  guardian_middle_name  :string
#  guardian_nic          :string
#  guardian_passport     :string
#  unidentify_guardian   :boolean          default("false")
#  state                 :string
#  city                  :string
#  near_by_city          :string
#  address1              :string
#  address2              :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  phone1                :string
#  phone1_type           :string
#  phone2                :string
#  phone2_type           :string
#  phone3                :string
#  phone3_type           :string
#  marital_status        :string
#  blood_group           :string
#  hiv                   :string
#  hepatitis_b_antigens  :string
#  hepatitis_c           :string
#  age                   :string
#  father_name           :string
#  unidentify_gender     :boolean          default("false")
#  guardian_state        :string
#  guardian_city         :string
#  guardian_near_by_city :string
#  guardian_address      :string
#  guardian_phone_type   :string
#  guardian_phone        :string
#  barcode_url           :string
#  qrcode_url            :string
#  unidentify            :boolean          default("false")
#  external_patient_id   :string
#

# spec/factories/patient.rb
FactoryGirl.define do
  factory :patient do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    gender 'male'
    patient_nic do 
      loop do
        possible_nic = Faker::Number.number(10)
        break possible_nic unless Patient.exists?(patient_nic: possible_nic)
      end
    end
    # patient_nic Faker::Number.number(10)
    guardian_nic Faker::Number.number(10)
    patient_passport Faker::Number.between(1, 10)
    guardian_passport Faker::Number.between(1, 10)
    birth_day Faker::Number.between(1, 30)
    birth_month Faker::Number.between(1, 12)
    birth_year Faker::Number.between(1900, 3000)
    unidentify_patient false
    guardian_relationship Faker::Name.name
    guardian_first_name Faker::Name.first_name
    guardian_last_name Faker::Name.last_name
    unidentify_guardian false
    marital_status 'Married'
    state Faker::Address.state
    city Faker::Address.city
    near_by_city Faker::Address.city
    address1 Faker::Address.street_address
    address2 Faker::Address.secondary_address
    phone1_type 'mobile'
    phone1 Faker::PhoneNumber.cell_phone
    phone2_type 'mobile'
    phone2 Faker::PhoneNumber.cell_phone
    phone3_type 'phone'
    phone3 Faker::PhoneNumber.phone_number
  end
end
