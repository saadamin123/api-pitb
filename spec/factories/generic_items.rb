# == Schema Information
#
# Table name: generic_items
#
#  id         :integer          not null, primary key
#  name       :string
#  category   :string
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :generic_item do
    name "MyString"
    category "MyString"
  end
end
