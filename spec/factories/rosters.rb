# == Schema Information
#
# Table name: rosters
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  department_id   :integer
#  doctors         :string
#  shift           :string
#  date            :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :roster do
    user nil
    medical_unit nil
    department nil
    doctors "MyString"
    shift "MyString"
    date "2016-10-19"
  end
end
