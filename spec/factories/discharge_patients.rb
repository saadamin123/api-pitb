# == Schema Information
#
# Table name: discharge_patients
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  patient_id      :integer
#  bed_id          :integer
#  admission_id    :integer
#  treatment_plan  :string
#  medications     :string
#  follow_up       :string
#  discharge_type  :string
#  comments        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visit_id        :integer
#

FactoryGirl.define do
  factory :discharge_patient do
    medical_unit_id 1
    patient_id 1
    bed_id 1
    admission_id 1
    treatment_plan "MyString"
    medication "MyString"
    follow_up 1
    check_box "MyString"
    comments "MyString"
  end
end
