# == Schema Information
#
# Table name: roles
#
#  id             :integer          not null, primary key
#  title          :string
#  default_screen :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :role do
  	title do 
      loop do
        possible_title = Faker::Name.title
        break possible_title unless Role.exists?(title: possible_title)
      end
    end
    # sequence(:title) { |n| "fdo#{n}" }
    default_screen 'FDOHomeActivity'
  end
end
