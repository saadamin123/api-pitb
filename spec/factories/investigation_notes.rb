# == Schema Information
#
# Table name: investigation_notes
#
#  id                 :integer          not null, primary key
#  medical_unit_id    :integer
#  patient_id         :integer
#  visit_id           :integer
#  admission_id       :integer
#  user_id            :integer
#  note               :string
#  investigation_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :investigation_note do
    medical_unit_id 1
    patient_id 1
    visit_id 1
    admission_id 1
    user_id 1
    note "MyString"
    type ""
  end
end
