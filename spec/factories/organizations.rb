# == Schema Information
#
# Table name: organizations
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :organization do
    user nil
    medical_unit nil
  end
end
