# == Schema Information
#
# Table name: devices
#
#  id               :integer          not null, primary key
#  uuid             :string
#  assignable_id    :integer
#  assignable_type  :string
#  role_id          :integer
#  operating_system :string
#  os_version       :string
#  status           :string
#  latitude         :float
#  longitude        :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

# spec/factories/device.rb
FactoryGirl.define do
  factory :device do
    uuid Faker::Number.between(1, 10)
    assignable_id Faker::Number.digit
    assignable_type 'MedicalUnit'
    role
    operating_system 'os'
    os_version '4.1'
    status 'active'
  end
end
