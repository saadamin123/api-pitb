# == Schema Information
#
# Table name: complaints
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  device_id       :integer
#  category        :string
#  area            :string
#  description     :string
#  severity_level  :string
#  show_identity   :boolean          default("false")
#  status          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  created_by      :string
#  assign_to_role  :integer
#  critical        :boolean          default("false")
#  high_priority   :boolean          default("false")
#

FactoryGirl.define do
  factory :complaint do |f|
  	f.id 1
    f.user_id 1
    f.medical_unit_id 2
    f.device_id 2
	  f.category 'doctor'
	  f.area 'emergency'
    f.description 'good'
    f.severity_level 'low'
    f.show_identity true
	  f.status 'open'
	  f.assign_to_role '2'
    f.critical false
  end
end
