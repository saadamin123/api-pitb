# == Schema Information
#
# Table name: medicine_requests
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  patient_id      :integer
#  visit_id        :integer
#  admission_id    :integer
#  prescribed_by   :integer
#  requested_by    :integer
#  status          :string
#  item_id         :integer
#  request_id      :string
#  prescribed_qty  :float
#  product_name    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  store_id        :integer
#

FactoryGirl.define do
  factory :medicine_request do
    medical_unit nil
    patient nil
    visit nil
    admission nil
    prescribed_by 1
    requested_by 1
    status "MyString"
    item_stock nil
    request_id "MyString"
  end
end
