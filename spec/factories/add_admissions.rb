# == Schema Information
#
# Table name: add_admissions
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  category     :string
#  department   :string
#  floor_number :string
#  bed_number   :string
#  bay_number   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :add_admission do
    association :order
    category "Inpatient"
    department "Cardiology"
    floor_number Faker::Number.number(5)
    bed_number Faker::Number.number(5)
    bay_number Faker::Number.number(5)
  end
end
