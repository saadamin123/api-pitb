# == Schema Information
#
# Table name: favorite_complaints
#
#  id           :integer          not null, primary key
#  complaint_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :favorite_complaint do
    association :complaint
    association :user
  end
end
