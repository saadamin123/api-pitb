# == Schema Information
#
# Table name: brands
#
#  id         :integer          not null, primary key
#  company_id :integer
#  name       :string
#  category   :string
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :brand do
    category 'Mycat'
    name 'MyString'
  end
end
