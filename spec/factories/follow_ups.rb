# == Schema Information
#
# Table name: follow_ups
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  follow_up       :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :follow_up do
    user nil
    patient nil
    visit nil
    medical_unit nil
    follow_up "2017-05-22"
  end
end
