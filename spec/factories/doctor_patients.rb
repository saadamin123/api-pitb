# == Schema Information
#
# Table name: doctor_patients
#
#  id         :integer          not null, primary key
#  doctor_id  :integer
#  active     :boolean          default("false")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  patient_id :integer
#

FactoryGirl.define do
  factory :doctor_patient do
    doctor_id 1
    patient_mrn 1
    active false
  end
end
