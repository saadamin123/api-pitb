# == Schema Information
#
# Table name: wards
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  department_id   :integer
#  parent_id       :integer
#  title           :string
#  active          :boolean          default("true")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#  beds_count      :integer          default("0")
#  bays_count      :integer          default("0")
#  deleted_at      :datetime
#  ward_code       :string
#

FactoryGirl.define do
  factory :ward do
    medical_unit_id 1
    department_id ""
    parent_id 1
    title "MyString"
    active false
  end
end
