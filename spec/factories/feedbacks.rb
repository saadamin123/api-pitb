# == Schema Information
#
# Table name: feedbacks
#
#  id              :integer          not null, primary key
#  complaint_id    :integer
#  medical_unit_id :integer
#  user_id         :integer
#  device_id       :integer
#  satisfaction    :string
#  message         :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string
#

FactoryGirl.define do
  factory :feedback do
    complaint
    satisfaction 'yes'
    message 'nothing'
    status 'open'
  end
end
