# == Schema Information
#
# Table name: lab_result_parameters
#
#  id              :integer          not null, primary key
#  parameter_title :string
#  result1         :string
#  result2         :string
#  uom             :string
#  range_title     :string
#  lab_result_id   :integer
#  out_of_range    :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :lab_result_parameter do
    
  end
end
