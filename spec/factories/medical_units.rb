# == Schema Information
#
# Table name: medical_units
#
#  id                    :integer          not null, primary key
#  region_id             :integer
#  title                 :string
#  identification_number :string
#  location              :string
#  latitude              :float
#  longitude             :float
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  category              :string
#  bays_count            :integer          default("0")
#  parent_id             :integer
#  code                  :string
#

# spec/factories/medical_unit.rb
FactoryGirl.define do
  factory :medical_unit do |f|
    f.region_id 44
    f.title 'Kamalia THQ'
    f.category 'THQ'
  end
end
