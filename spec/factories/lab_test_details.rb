# == Schema Information
#
# Table name: lab_test_details
#
#  id                  :integer          not null, primary key
#  order_id            :integer
#  lab_order_detail_id :integer
#  caption             :string
#  range               :string
#  unit                :string
#  value               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  comments            :string
#  test_label          :string
#

FactoryGirl.define do
  factory :lab_test_detail do
    association :order
    caption 'Total Cholesterol'
    range 'Below 200'
    unit 'mg/dL'
    value '160'
  end
end
