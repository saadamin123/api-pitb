# == Schema Information
#
# Table name: brand_drugs
#
#  id           :integer          not null, primary key
#  name         :string
#  category     :string
#  form         :string
#  dumb         :integer
#  packing      :string
#  trade_price  :float
#  retail_price :float
#  mg           :string
#  drug_id      :integer
#  brand_id     :integer
#  created_at   :datetime
#  updated_at   :datetime
#
FactoryGirl.define do
  factory :brand_drug do
    name 'MyString'
    category 'MyString'
    form 'MyString'
    dumb 1
    packing 'MyString'
    trade_price 1.5
    retail_price 1.5
    mg 'MyString'
    drug_id 1
    brand
  end
end
