# == Schema Information
#
# Table name: medical_unit_users
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  user_id         :integer
#  role_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :medical_unit_user do
    medical_unit
    user
    role
  end
end
