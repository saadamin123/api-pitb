# == Schema Information
#
# Table name: vitals
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  weight          :float
#  height          :float
#  temperature     :float
#  bp_systolic     :float
#  bp_diastolic    :float
#  pulse           :float
#  resp_rate       :float
#  o2_saturation   :float
#  head_circ       :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :vital do
    weight Faker::Number.between(1, 999)
    height Faker::Number.between(1, 96)
    temperature Faker::Number.between(95, 107)
    bp_systolic Faker::Number.between(50, 210)
    bp_diastolic Faker::Number.between(33, 120)
    pulse Faker::Number.between(140, 140)
    resp_rate Faker::Number.between(12, 70)
    o2_saturation Faker::Number.between(0, 100)
    head_circ Faker::Number.between(16, 56)
  end
end
