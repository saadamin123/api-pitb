# == Schema Information
#
# Table name: visit_users
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  visit_id    :integer
#  is_checkout :boolean          default("false")
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :visit_user do
    user nil
    visit nil
    is_checkout false
  end
end
