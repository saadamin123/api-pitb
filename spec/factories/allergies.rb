# == Schema Information
#
# Table name: allergies
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  allergy_type    :string
#  value           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :allergy do
    allergy_type 'Drug'
    value Faker::Lorem.words
  end
end
