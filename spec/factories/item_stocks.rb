# == Schema Information
#
# Table name: item_stocks
#
#  id               :integer          not null, primary key
#  medical_unit_id  :integer
#  department_id    :integer
#  store_id         :integer
#  user_id          :integer
#  item_id          :integer
#  quantity_in_hand :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

FactoryGirl.define do
  factory :item_stock do
    medical_unit nil
    department nil
    store nil
    user nil
    item nil
    quantity_in_hand "MyString"
  end
end
