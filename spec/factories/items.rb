# == Schema Information
#
# Table name: items
#
#  id              :integer          not null, primary key
#  generic_item_id :integer
#  medical_unit_id :integer
#  department_id   :integer
#  user_id         :integer
#  item_name       :string
#  item_code       :string
#  item_type       :string
#  main_group      :string
#  uom             :string
#  upc             :string
#  price           :string
#  sub_group       :string
#  strength        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string           default("Open")
#  product_name    :string
#  pack            :float            default("1.0")
#  unit            :float            default("1.0")
#  pack_size       :float            default("1.0")
#

FactoryGirl.define do
  factory :item do
    generic_item nil
    medical_unit nil
    department nil
    store nil
    user nil
    item_name "MyString"
    item_code "MyString"
    item_type "MyString"
    main_group "MyString"
    uom ""
    upc "MyString"
    price "MyString"
    sub_group "MyString"
    strength "MyString"
    quantity_in_hand "MyString"
  end
end
