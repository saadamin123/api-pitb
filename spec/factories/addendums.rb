# == Schema Information
#
# Table name: addendums
#
#  id              :integer          not null, primary key
#  historical_note :text
#  note_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :addendum do
    historical_note "MyText"
    note
  end
end
