# == Schema Information
#
# Table name: stores
#
#  id              :integer          not null, primary key
#  name            :string
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#  department_id   :integer
#

FactoryGirl.define do
  factory :store do
    name "MyString"
    parent_id 1
    medical_unit
  end
end
