# == Schema Information
#
# Table name: inventories
#
#  id              :integer          not null, primary key
#  brand_drug_id   :integer
#  quantity        :integer          default("0")
#  medical_unit_id :integer
#  device_id       :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  store_id        :integer
#  department_id   :integer
#

FactoryGirl.define do
  factory :inventory do
    brand_drug_id 1
    quantity 1
    medical_unit_id 1
    device_id 1
    brand_drug
    medical_unit
  end
end
