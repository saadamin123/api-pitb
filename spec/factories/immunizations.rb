# == Schema Information
#
# Table name: immunizations
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  age             :string
#  vaccine         :string
#  dose            :string
#  due             :date
#  given_on        :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :immunization do
    age '6 Weeks'
    vaccine 'OPV'
    dose '0.05ml'
    due '03-28-2106'
    given_on 'Week'
    patient
  end
end
