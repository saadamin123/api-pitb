# == Schema Information
#
# Table name: pictures
#
#  id             :integer          not null, primary key
#  imageable_id   :integer
#  imageable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  file           :string
#  visit_id       :integer
#

FactoryGirl.define do
  factory :picture do |f|
  	f.id 1
    f.imageable_id 1
    f.imageable_type 'User'
    f.file File.open(File.join(Rails.root, '/spec/fixtures/files/01.png'))
    f.user
  end
end
