# == Schema Information
#
# Table name: brands
#
#  id         :integer          not null, primary key
#  company_id :integer
#  name       :string
#  category   :string
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Brand, type: :model do
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:name) }
  context 'associations' do
    it { should belong_to :company }
  end
end
