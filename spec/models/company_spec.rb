# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string
#  address    :text
#  phone      :string
#  fax        :string
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Company, type: :model do
  it { should validate_presence_of(:name) }
  context 'associations' do
    it { should have_many(:brands)}
  end
end
