# == Schema Information
#
# Table name: favorite_complaints
#
#  id           :integer          not null, primary key
#  complaint_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe FavoriteComplaint, :type => :model do
  it { should validate_presence_of(:complaint_id) }

  context 'associations' do
    it { should belong_to :user }
    it { should belong_to :complaint }
  end
end
