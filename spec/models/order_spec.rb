# == Schema Information
#
# Table name: orders
#
#  id                        :integer          not null, primary key
#  patient_id                :integer
#  doctor_id                 :integer
#  status                    :string
#  medical_unit_id           :integer
#  device_id                 :integer
#  updated_by                :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  department                :string
#  category                  :string
#  order_type                :string           default("Medicine")
#  visit_id                  :integer
#  cancel_reason             :string
#  cancel_reason_details     :string
#  store_id                  :integer
#  department_id             :integer
#  case_number               :string
#  external_medicine         :string
#  barcode_url               :string
#  lab_order_sequence_number :string
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  it { should validate_presence_of(:medical_unit) }
  it { should validate_presence_of(:doctor) }
  it { should validate_presence_of(:device) }
  it { should validate_presence_of(:order_type) }
  it { should validate_presence_of(:status) }
  it { should validate_inclusion_of(:status).in_array(%w[Open Pending Closed Cancel Expired open pending closed cancel expired]) }
  it { should validate_inclusion_of(:order_type).in_array(%w[Admission Radiology Consultation Nursing Procedure Medicine Lab Medication]) }
  context 'associations' do
    it { should have_many(:order_items)}
    it { should have_many(:add_admissions)}
    it { should have_many(:order_logs)}
    it { should have_one(:radiology_order_detail)}
    it { should have_one(:procedure_order_detail)}
    it { should have_one(:nursing_order_detail)}
    it { should have_one(:lab_order_detail)}
    it { should belong_to :medical_unit }
    it { should belong_to :device }
    it { should belong_to :doctor }
    it { should belong_to :patient }
    it { should belong_to :visit }
  end
end
