# == Schema Information
#
# Table name: procedure_order_details
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  procedure    :string
#  option       :string
#  left         :boolean          default("false")
#  right        :boolean          default("false")
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  instructions :string
#  note_id      :integer
#  type         :string
#

require 'rails_helper'

RSpec.describe ProcedureOrderDetail, :type => :model do
  it { should validate_presence_of(:procedure) }
  context 'associations' do
    it { should belong_to :order }
  end
end
