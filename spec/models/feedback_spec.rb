# == Schema Information
#
# Table name: feedbacks
#
#  id              :integer          not null, primary key
#  complaint_id    :integer
#  medical_unit_id :integer
#  user_id         :integer
#  device_id       :integer
#  satisfaction    :string
#  message         :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string
#

require 'rails_helper'

RSpec.describe Feedback, type: :model do
  it { should validate_presence_of(:satisfaction) }
  it { should validate_presence_of(:status) }
  context 'associations' do
    it { should belong_to :user }
    it { should belong_to :device }
    it { should belong_to :medical_unit }
    it { should belong_to :complaint }
  end
end
