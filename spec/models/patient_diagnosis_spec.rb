# == Schema Information
#
# Table name: patient_diagnoses
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  name         :string
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  note_id      :integer
#  type         :string
#  visit_id     :integer
#  patient_id   :integer
#  instructions :text
#  plan         :text
#

require 'rails_helper'

RSpec.describe PatientDiagnosis, :type => :model do
  # it { should validate_presence_of(:code) }
  it { should validate_presence_of(:visit) }

  context 'associations' do
    it { should belong_to :visit }
    it { should belong_to :user }
    it { should belong_to :patient }
  end
end
