# == Schema Information
#
# Table name: bed_allocations
#
#  id                      :integer          not null, primary key
#  medical_unit_id         :integer
#  patient_id              :integer
#  admission_id            :integer
#  ward_id                 :integer
#  bay_id                  :integer
#  bed_id                  :integer
#  status                  :string           default("Free")
#  patient_last_updated_by :integer
#  user_id                 :integer
#  visit_id                :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  comment                 :string
#

require 'rails_helper'

RSpec.describe BedAllocation, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
