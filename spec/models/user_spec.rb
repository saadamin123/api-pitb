# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  role_id                :integer
#  username               :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  title                  :string
#  phone                  :string
#  nic                    :string
#  dob                    :date
#  father_name            :string
#  father_nic             :string
#  mother_name            :string
#  mother_nic             :string
#  address1               :string
#  authentication_token   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  failed_attempts        :integer          default("0"), not null
#  unlock_token           :string
#  locked_at              :datetime
#  address2               :string
#  department_id          :integer
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_uniqueness_of(:username) }
  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:phone) }
  it { should validate_presence_of(:nic) }
  it { should validate_presence_of(:dob) }
  it { should validate_presence_of(:email) }

  context 'associations' do
    it { should belong_to :role }
    it { should have_many :allergies }
    it { should have_many :vitals }
    it { should have_many(:complaints)}
    it { should have_many(:feedbacks)}
    it { should have_many(:favorite_complaints)}
    it { should have_many(:medical_unit_users)}
    it { should have_many(:pictures)}
    it { should have_many(:medical_units)}
    it { should have_many(:home_medications)}
    it { should have_many :immunizations }
  end
end
