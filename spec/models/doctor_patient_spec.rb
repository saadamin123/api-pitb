# == Schema Information
#
# Table name: doctor_patients
#
#  id         :integer          not null, primary key
#  doctor_id  :integer
#  active     :boolean          default("false")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  patient_id :integer
#

require 'rails_helper'

RSpec.describe DoctorPatient, type: :model do
  
  context 'associations' do
    it { should belong_to :patient }
    it { should belong_to :doctor }
  end
end
