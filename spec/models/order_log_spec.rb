# == Schema Information
#
# Table name: order_logs
#
#  id                  :integer          not null, primary key
#  order_id            :integer
#  lab_order_detail_id :integer
#  status              :string
#  updated_by          :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'

RSpec.describe OrderLog, :type => :model do
  it { should validate_presence_of(:status) }
  context 'associations' do
    it { should belong_to :order }
  end
end
