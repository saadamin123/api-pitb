# == Schema Information
#
# Table name: lab_assessments
#
#  id                     :integer          not null, primary key
#  title                  :string
#  uom                    :string
#  specimen               :string
#  status                 :string
#  result_type1           :string
#  result_type2           :string
#  medical_unit_id        :integer
#  parameter_code         :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  machine_code           :string
#  machine_parameter_code :string
#  machine_name           :string
#

require 'rails_helper'

RSpec.describe LabAssessment, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
