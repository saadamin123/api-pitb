# == Schema Information
#
# Table name: drugs
#
#  id             :integer          not null, primary key
#  name           :string
#  overview       :text
#  characterstics :text
#  indications    :text
#  contradictions :text
#  interactions   :text
#  interference   :text
#  effects        :text
#  risk           :text
#  warning        :text
#  storage        :text
#  category       :string
#  created_at     :datetime
#  updated_at     :datetime
#

require 'rails_helper'

RSpec.describe Drug, type: :model do
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:overview) }
end
