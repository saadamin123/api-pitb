# == Schema Information
#
# Table name: admissions
#
#  id                     :integer          not null, primary key
#  medical_unit_id        :integer
#  visit_id               :integer
#  patient_id             :integer
#  referred_from          :integer
#  referred_to            :integer
#  ward_id                :integer
#  doctor_id              :integer
#  user_id                :integer
#  attendent_name         :string
#  attendent_mobile       :string
#  attendent_address      :string
#  attendent_nic          :string
#  attendent_relationship :string
#  status                 :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  admission_number       :string
#

require 'rails_helper'

RSpec.describe Admission, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
