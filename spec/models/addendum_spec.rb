# == Schema Information
#
# Table name: addendums
#
#  id              :integer          not null, primary key
#  historical_note :text
#  note_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe Addendum, :type => :model do
  it { should validate_presence_of(:historical_note) }
  context 'associations' do
    it { should belong_to :note }
  end
end
