# == Schema Information
#
# Table name: icd_codes
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe IcdCode, :type => :model do
  it { should validate_presence_of(:code) }
  it { should validate_presence_of(:name) }
end
