# == Schema Information
#
# Table name: line_items
#
#  id               :integer          not null, primary key
#  order_id         :integer
#  brand_drug_id    :integer
#  quantity         :integer
#  category         :string
#  expiry           :date
#  medical_unit_id  :integer
#  device_id        :integer
#  inventory_id     :integer
#  order_type       :string
#  status           :string
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#  reason           :string
#  order_item_id    :integer
#  transaction_type :string
#  store_id         :integer
#  department_id    :integer
#  batch_id         :integer
#  visit_id         :integer
#

require 'rails_helper'

RSpec.describe LineItem, type: :model do
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:quantity) }
  it { should validate_presence_of(:transaction_type) }
  
  context 'associations' do
    it { should belong_to :inventory }
    it { should belong_to :medical_unit }
    it { should belong_to :brand_drug }
    it { should belong_to :device }
  end
end
