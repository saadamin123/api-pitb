# == Schema Information
#
# Table name: roles
#
#  id             :integer          not null, primary key
#  title          :string
#  default_screen :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe Role, type: :model do
  it { should validate_uniqueness_of(:title) }
end
