# == Schema Information
#
# Table name: admission_order_details
#
#  id             :integer          not null, primary key
#  order_id       :integer
#  category       :string
#  department     :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  admission_type :string
#  instructions   :string
#

require 'rails_helper'

RSpec.describe AdmissionOrderDetail, :type => :model do
  it { should validate_presence_of(:department) }
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:admission_type) }
  it { should validate_inclusion_of(:admission_type).in_array(%w[new disposition]) }

  context 'associations' do
    it { should belong_to :order }
  end
end
