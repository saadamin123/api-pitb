require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::UsersController, type: :controller do
  describe 'Users API' do
    before do
      @user = FactoryGirl.create(:user)
    end

    describe 'GET #user' do
      context 'should return forgot user after given valid phone and dob' do
        before do
          post 'forgot_username', phone: @user.phone, dob: @user.dob
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should not return forgot user from nic' do
        before do
          post 'forgot_username', phone: @user.nic, dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot user after given invalid phone number' do
        before do
          post 'forgot_username', phone: '29529592', dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot user after given invalid dob' do
        before do
          post 'forgot_username', phone: @user.phone, dob: '2016-01-21'
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot user after given nil dob' do
        before do
          post 'forgot_username', phone: @user.phone, dob: nil
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot user after given nil phone number' do
        before do
          post 'forgot_username', phone: nil, dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should return forgot password after given valid phone, nic and dob' do
        before do
          post 'forgot_password', phone: @user.phone, nic: @user.nic, dob: @user.dob
        end

        it { expect(response.status).to eq(200) }
        it { expect(response.body).to match /Message has been sent to your Mobile number/ }
      end
      context 'should not return forgot password from email' do
        before do
          post 'forgot_password', phone: @user.email, dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot password after given invalid phone number' do
        before do
          post 'forgot_password', phone: '29529592', nic: @user.nic, dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot password after given invalid dob' do
        before do
          post 'forgot_password', phone: @user.phone, nic: @user.nic, dob: '2016-01-21'
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot password after given invalid nic' do
        before do
          post 'forgot_password', phone: @user.phone, nic: '636083303603', dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot password after given nil dob' do
        before do
          post 'forgot_password', phone: @user.phone, nic: @user.nic, dob: nil
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
      context 'should not return forgot password after given nil phone number' do
        before do
          post 'forgot_password', phone: nil, nic: @user.nic, dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
       context 'should not return forgot password after given nil nic' do
        before do
          post 'forgot_password', phone: @user.phone, nic: nil, dob: @user.dob
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Record not match/ }
      end
    end
  end
  describe 'HOSPITAL_DOCTORS #user' do
    let(:user) { FactoryGirl.create(:user) }
    let(:medical_unit) { FactoryGirl.create(:medical_unit) }
    let(:device) { FactoryGirl.create(:device) }
    let(:role) { FactoryGirl.create(:role, default_screen: "doctor") }
    let(:medical_unit_user) { FactoryGirl.create(:medical_unit_user, medical_unit: medical_unit, role: role, user: user) }
    context 'should return doctors of medical unit' do
      before do
        get 'hospital_doctors', auth_token: user.authentication_token, medical_unit_id: medical_unit.id, device_id: device.id
      end

      it { expect(response.status).to eq(200) }
    end
    context 'should not return doctor with invalid auth_token' do
      before do
        get 'hospital_doctors', auth_token: "hghsghsdgsh", medical_unit_id: medical_unit.id, device_id: device.id
      end

      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return doctor with invalid device' do
      before do
        get 'hospital_doctors', auth_token: user.authentication_token, medical_unit_id: medical_unit.id, device_id: "hgha555"
      end

      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not return doctor with invalid medical_unit' do
      before do
        get 'hospital_doctors', auth_token: user.authentication_token, medical_unit_id: "dgjadhg111", device_id: device.id
      end

      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid medical unit id/ }
    end
  end
end
