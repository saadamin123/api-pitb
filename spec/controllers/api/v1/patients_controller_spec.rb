require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::PatientsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }
  describe 'CREATE #patient' do
    context 'should create patients with valid data' do
      before do
        post 'create', { patient: FactoryGirl.attributes_for(:patient, first_name: "Larry", last_name: "Smith", gender: 'male'), auth_token: user.authentication_token, device_id: device.uuid }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create patients with invalid auth_token' do
      before do
        post 'create', { patient: FactoryGirl.attributes_for(:patient, first_name: "Larry", last_name: "Smith", gender: 'male'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create patients with invalid device' do
      before do
        post 'create', { patient: FactoryGirl.attributes_for(:patient, first_name: "Larry", last_name: "Smith", gender: 'male'), auth_token: user.authentication_token, device_id: 'gwkhkwh' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
  end
  describe 'UPDATE #patient' do
    let(:patient) { FactoryGirl.create(:patient) }
    context 'should update patients with valid data' do
      before do
        put :update, id: patient, patient: FactoryGirl.attributes_for(:patient, first_name: "Larry", last_name: "Smith"), auth_token: user.authentication_token, device_id: device.uuid
      end
      it { expect(response.status).to eq(200) }
    end
  end
  describe 'Patients API' do
    let(:patient) { FactoryGirl.create(:patient) }
    describe 'GET #patient' do
      context 'should return patient of given patient nic' do
        before do
          get 'index', type: 'nic', query: patient.patient_nic, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given guardian nic' do
        before do
          get 'index', type: 'nic', query: patient.guardian_nic, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given patient nic and guardian nic' do
        before do
          get 'index', type: 'nic', query: patient.patient_nic, query: patient.guardian_nic, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should not return patients without given nic type' do
        before do
          get 'index', query: patient.patient_nic, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Patient not found/ }
      end
      context 'should not return patient with nil params' do
        before do
          get 'index', type: 'nic', query: nil, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Patient not found/ }
      end
      context 'should return patients with nil params' do
        before do
          get 'index', type: 'nic', query: nil, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Patient not found/ }
      end
      context 'should return patient of given patient passport' do
        before do
          get 'index', type: 'passport', query: patient.patient_passport, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given guardian passport' do
        before do
          get 'index', type: 'passport', query: patient.guardian_passport, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given patient passport and guardian passport' do
        before do
          get 'index', type: 'passport', query: patient.patient_passport, query: patient.guardian_passport, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should not return patients without given passport type' do
        before do
          get 'index', query: patient.patient_passport, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Patient not found/ }
      end
      context 'should return patient of given phone1' do
        before do
          get 'index', type: 'phone', query: patient.phone1, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given phone2' do
        before do
          get 'index', type: 'phone', query: patient.phone2, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given phone3' do
        before do
          get 'index', type: 'phone', query: patient.phone3, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return patients of given phone1, phone2 and phone3' do
        before do
          get 'index', type: 'phone', query: patient.phone1, query: patient.phone2, query: patient.phone3, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should not return patients without given phone type' do
        before do
          get 'index', query: patient.phone1, auth_token: user.authentication_token
        end

        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Patient not found/ }
      end
      context 'should return patients of hospital' do
        before do
          FactoryGirl.create(:visit,medical_unit_id: medical_unit.id, patient_id: patient.id, user_id: user.id)
          get 'index', query: medical_unit.id, type: "hospital", auth_token: user.authentication_token,category: 'in-patient', department: 'cardialogy'
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should not return patients of hospital with invalid medical_unit' do
        before do
          get 'index', auth_token: user.authentication_token, query: "123456", type: "hospital",category: 'in-patient', department: 'cardialogy'
        end
        it { expect(response.status).to eq(404) }
        it { expect(response.body).to match /Patient not found/ }
      end
    end
  end
  describe 'SHOW #patient' do
    let(:patient) { FactoryGirl.create(:patient) }
    context 'should return patient of given patient mrn' do
      before do
        get 'show', id: patient.mrn, auth_token: user.authentication_token
      end

      it { expect(response.status).to eq(200) }
    end
    context 'should not return patient with invalid auth_token' do
      before do
        get 'show', id: patient.mrn, auth_token: "hgih0iug0gy0g"
      end

      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return patient with invalid id' do
      before do
        get 'show', id: "636233", auth_token: user.authentication_token
      end

      it { expect(response.status).to eq(404) }
      it { expect(response.body).to match /Patient not found/ }
    end
  end
end
