require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::OrderLogsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:device) { FactoryGirl.create(:device) }
  let(:order) { FactoryGirl.create(:order) }

  describe 'Search #order logs history' do
    context 'should Search order logs history with valid data' do
      before do
        get 'index', { auth_token: user.authentication_token, device_id: device.uuid, order_id: order.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search order logs history with invalid authentication_token' do
      before do
        get 'index', { auth_token: 'hsfkhfhkfkw79', device_id: device.uuid, order_id: order.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search order logs history with invalid device_id' do
      before do
        get 'index', { auth_token: user.authentication_token, device_id: '4uy646', order_id: order.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not Search order logs history with invalid order_id' do
      before do
        get 'index', { auth_token: user.authentication_token, device_id: device.uuid, order_id: '258528' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid order id/ }
    end
  end
end
