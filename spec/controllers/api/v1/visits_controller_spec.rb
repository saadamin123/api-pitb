require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::VisitsController, type: :controller do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let!(:role) { FactoryGirl.create(:role) }
  let!(:device) { FactoryGirl.create(:device) }
  let!(:patient) { FactoryGirl.create(:patient) }
  let!(:visit) {FactoryGirl.create(:visit, patient: patient)}
  describe 'CREATE #visit' do
    context 'should create visit with valid data' do
      before do
        post 'create', { visit: FactoryGirl.attributes_for(:visit, reason: 'checkup', referred_to: 'emergency'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create patients with invalid auth_token' do
      before do
        post 'create', { visit: FactoryGirl.attributes_for(:visit, reason: 'checkup'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid, patient_id: patient.mrn }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create patients with invalid device' do
      before do
        post 'create', { visit: FactoryGirl.attributes_for(:visit, reason: 'checkup'), auth_token: user.authentication_token, device_id: 'gwkhkwh', patient_id: patient.mrn }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create patients with invalid device' do
      before do
        post 'create', { visit: FactoryGirl.attributes_for(:visit, reason: 'checkup'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '7529922752' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
  end
  describe 'INDEX #visits' do
    context 'should search visits with valid data' do
      before do
        get 'index', auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, start_date: "2016-03-15", end_date: Date.today
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not search visits with invalid auth_token' do
      before do
        get 'index', auth_token: "f4f5fr4", device_id: device.uuid, patient_id: patient.mrn, start_date: "2016-03-15", end_date: Date.today
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not search visits with invalid device' do
      before do
        get 'index', auth_token: user.authentication_token, device_id: "dfdfdf444", patient_id: patient.mrn, start_date: "2016-03-15", end_date: Date.today
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not search visits without start_date and end_date' do
      before do
        get 'index', auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn
      end
      it { expect(response.status).to eq(404) }
      it { expect(response.body).to match /Record not found/ }
    end
  end
end
