require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::LookupsController, type: :controller do
  describe 'Users API' do
    before do
      @lookup = FactoryGirl.create(:lookup)
    end

    describe 'GET #lookup' do
      context 'should return searched lookups' do
        before do
          get 'index', category: @lookup.category
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return all lookups' do
        before do
          get 'index', category: 'all'
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return no lookup' do
        before do
          get 'index', category: 'visiter-referred'
        end

        it { expect(response.status).to eq(404) }
      end
      context 'should return no lookups' do
        before do
          get 'index', category: nil
        end

        it { expect(response.status).to eq(404) }
      end
    end
  end
end
