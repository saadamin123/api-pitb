require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::AddAdmissionsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:device) { FactoryGirl.create(:device) }
  let(:order) { FactoryGirl.create(:order) }
  describe 'CREATE #admission' do
    context 'should create admission with valid data' do
      before do
        post 'create', { add_admission: FactoryGirl.attributes_for(:add_admission, category: 'checkup', department: 'emergency', floor_number: '1', bed_number: '001', bay_number: '001'), auth_token: user.authentication_token, device_id: device.uuid, order_id: order.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create admission with invalid auth_token' do
      before do
         post 'create', { add_admission: FactoryGirl.attributes_for(:add_admission, category: 'checkup', department: 'emergency', floor_number: '1', bed_number: '001', bay_number: '001'), auth_token: 'ghegkhegeiyteyi', device_id: device.uuid, order_id: order.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create admission with invalid device' do
      before do
         post 'create', { add_admission: FactoryGirl.attributes_for(:add_admission, category: 'checkup', department: 'emergency', floor_number: '1', bed_number: '001', bay_number: '001'), auth_token: user.authentication_token, device_id: 'jldgljgdljgd', order_id: order.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create admission with invalid order id' do
      before do
         post 'create', { add_admission: FactoryGirl.attributes_for(:add_admission, category: 'checkup', department: 'emergency', floor_number: '1', bed_number: '001', bay_number: '001'), auth_token: user.authentication_token, device_id: device.uuid, order_id: '663' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid order id/ }
    end
  end
  describe 'Search #admission' do
    before do
      post 'create', { add_admission: FactoryGirl.attributes_for(:add_admission, category: 'checkup', department: 'emergency', floor_number: '1', bed_number: '001', bay_number: '001'), auth_token: user.authentication_token, device_id: device.uuid, order_id: order.id }
    end
    context 'should Search admission with valid data' do
      before do
        get 'index', { auth_token: user.authentication_token, order_id: order.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search admission with invalid authentication_token' do
      before do
        get 'index', { auth_token: 'hsfkhfhkfkw79', order_id: order.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search admission with invalid order_id' do
      before do
        get 'index', { auth_token: user.authentication_token, order_id: '258528' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid order id/ }
    end
  end
end
