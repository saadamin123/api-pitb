require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::DevicesController, type: :controller do
  describe 'Devices API' do
  let(:user) { FactoryGirl.create(:user) }
  let(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }

    describe 'GET #device' do
      context 'should return devices' do
        before do
          get 'index', uuid: device.uuid
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should not return device' do
        before do
          get 'index', uuid: '123'
        end

        it { expect(response.status).to eq(404) }
      end
      context 'should not return device' do
        before do
          get 'index', uuid: nil
        end

        it { expect(response.status).to eq(404) }
      end
    end
  end
end
