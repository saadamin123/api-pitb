# == Schema Information
#
# Table name: visits
#
#  id                      :integer          not null, primary key
#  medical_unit_id         :integer
#  patient_id              :integer
#  user_id                 :integer
#  visit_number            :string
#  reason                  :string
#  reason_note             :string
#  ref_department          :string
#  ref_department_note     :string
#  mode_of_conveyance      :string
#  mode_of_conveyance_note :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  referred_to             :string
#  amount_paid             :float
#  current_ref_department  :string
#  current_referred_to     :string
#  is_active               :boolean          default("true")
#  ambulance_number        :string
#  driver_name             :string
#  mlc                     :boolean          default("false")
#  policeman_name          :string
#  belt_number             :string
#  police_station_number   :string
#  department_id           :integer
#  doctor_id               :integer
#  external_visit_number   :string
#

require 'test_helper'

class VisitTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
