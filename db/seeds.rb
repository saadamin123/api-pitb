	# This file should contain all the record creation needed to seed the database with its default values.
	# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
	#
	# Examples:
	#
	#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
	#   Mayor.create(name: 'Emanuel', city: cities.first)


	#  title      :string
	#  location   :string
	#  district   :string
  AdminUser.create!(:username => 'superadmin', :email => 'super_admin@example.com', :password => 'password', :password_confirmation => 'password')
  AdminUser.create!(:username => 'admin', :email => 'admin@example.com', :password => 'password', :password_confirmation => 'password')

  r1 = Role.create(title: 'fdo')
  r2 = Role.create(title: 'Nurse')
  r3 = Role.create(title: 'MO')
  r4 = Role.create(title: 'Dr')
  r5 = Role.create(title: 'Pharmacist')
  r6 = Role.create(title: 'Lab')
  r7 = Role.create(title: 'PharmaAdmin')
  r8 = Role.create(title: 'MS')
  r9 = Role.create(title: 'CH_EMR_fdo')
  r10 = Role.create(title: 'CH_OPD_fdo')
  r11 = Role.create(title: 'CH_IPD_fdo')
  r12 = Role.create(title: 'Lab_fdo')
  r13 = Role.create(title: 'Pathologist')

  plant :seed_region
  plant :seed_medical_unit
  plant :seed_lookup
  plant :seed_new_lookup
  plant :seed_new_vlaue
  plant :seed_visit_reason_value
  plant :seed_procedure_lookup
  plant :seed_study_template
  plant :seed_lookups_values
  plant :visiter_referred_to_lookup
  plant :new_lab_test

  narowal_id= MedicalUnit.where(title: 'DHQ Narowal').first.id
  ch_id= MedicalUnit.where(title: "The Children's Hospital & The Institute Of Child Health, Lahore").first.id
  store_1 = Store.create!(name: "Store1", medical_unit_id: narowal_id)
  store_2 = Store.create!(name: "Store2", medical_unit_id: ch_id)

  fdo_1 = User.create!(first_name: 'Ata', last_name: 'Rabbi', username:'fdo1',password: 'changeit' , phone: '03211211211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+1@clustox.com', role: r1)
  fdo_2 = User.create!(first_name: 'Omer', last_name: 'Khan', username:'fdo2',password: 'changeit' , phone: '032112311211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+9@clustox.com', role: r1)
  fdo_3 = User.create!(first_name: 'Majid', last_name: 'Khan', username:'fdo3',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+100@clustox.com', role: r1)
  fdo_4 = User.create!(first_name: 'Majid', last_name: 'Khan', username:'fdo4',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+101@clustox.com', role: r1)
  fdo_5 = User.create!(first_name: 'Majid', last_name: 'Khan', username:'fdo5',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+102@clustox.com', role: r1)
  fdo_6 = User.create!(first_name: 'Ata', last_name: 'Khan', username:'atarabbi',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+103@clustox.com', role: r1)
  nurse_1 = User.create!(first_name: 'Arfa', last_name: 'Mirza', username:'nurse1',password: 'changeit' , phone: '03211211211', nic: '123451222321111', dob: Time.now , email: 'ata.rabi+2@clustox.com', role: r2)
  mo_1 = User.create!(first_name: 'Naeem', last_name: 'Aslam', username:'mo1',password: 'changeit' , phone: '03211211211', nic: '1234512224321111', dob: Time.now , email: 'ata.rabi+3@clustox.com', role: r3)
  doctor_1 = User.create!(first_name: 'Nadeem', last_name: 'Aslam', username:'doctor1',password: 'changeit' , phone: '03211211211', nic: '1234512223213111', dob: Time.now , email: 'ata.rabi+4@clustox.com', role: r4)
  doctor_2 = User.create!(first_name: 'Naeem', last_name: 'Aslam', username:'doctor2',password: 'changeit' , phone: '03211211211', nic: '1234512223213111', dob: Time.now , email: 'ata.rabi+23@clustox.com', role: r4)
  doctor_3 = User.create!(first_name: 'Hakeem', last_name: 'Aslam', username:'doctor3',password: 'changeit' , phone: '03211211211', nic: '1234512223213111', dob: Time.now , email: 'ata.rabi+24@clustox.com', role: r4)
  doctor_4 = User.create!(first_name: 'Mouman', last_name: 'Aslam', username:'doctor4',password: 'changeit' , phone: '03211211211', nic: '1234512223213111', dob: Time.now , email: 'ata.rabi+25@clustox.com', role: r4)
  doctor_5 = User.create!(first_name: 'Ali', last_name: 'Aslam', username:'doctor5',password: 'changeit' , phone: '03211211211', nic: '1234512223213111', dob: Time.now , email: 'ata.rabi+26@clustox.com', role: r4)
  pharmacist_1 = User.create!(first_name: 'Ali', last_name: 'Ahmed', username:'pharmacist1',password: 'changeit' , phone: '03211278211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+61@clustox.com', role: r5)
  pharmacist_2 = User.create!(first_name: 'Khan', last_name: 'Ahmed', username:'pharmacist2',password: 'changeit' , phone: '03211278211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+62@clustox.com', role: r5)
  pharmacist_3 = User.create!(first_name: 'Javed', last_name: 'Ahmed', username:'pharmacist3',password: 'changeit' , phone: '03211278211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+63@clustox.com', role: r5)
  pharmacist_4 = User.create!(first_name: 'Majid', last_name: 'Ahmed', username:'pharmacist4',password: 'changeit' , phone: '03211278211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+64@clustox.com', role: r5)
  pharmacist_5 = User.create!(first_name: 'Khawar', last_name: 'Ahmed', username:'pharmacist5',password: 'changeit' , phone: '03211278211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+65@clustox.com', role: r5)
  lab_1 = User.create!(first_name: 'Farooq', last_name: 'Tariq', username:'lab1',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+71@clustox.com', role: r6)
  lab_2 = User.create!(first_name: 'Ali', last_name: 'Tariq', username:'lab2',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+72@clustox.com', role: r6)
  lab_3 = User.create!(first_name: 'Usman', last_name: 'Tariq', username:'lab3',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+73@clustox.com', role: r6)
  lab_4 = User.create!(first_name: 'Shameer', last_name: 'Tariq', username:'lab4',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+74@clustox.com', role: r6)
  lab_5 = User.create!(first_name: 'Sajjad', last_name: 'Tariq', username:'lab5',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+75@clustox.com', role: r6)
  pharmaAdmin_1 = User.create!(first_name: 'Omer', last_name: 'Tariq', username:'pharmaAdmin1',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+81@clustox.com', role: r7)
  pharmaAdmin_2 = User.create!(first_name: 'Salman', last_name: 'Tariq', username:'pharmaAdmin2',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+82@clustox.com', role: r7)
  pharmaAdmin_3 = User.create!(first_name: 'Qaiser', last_name: 'Tariq', username:'pharmaAdmin3',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+83@clustox.com', role: r7)
  pharmaAdmin_4 = User.create!(first_name: 'Waleed', last_name: 'Tariq', username:'pharmaAdmin4',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+84@clustox.com', role: r7)
  pharmaAdmin_5 = User.create!(first_name: 'Majid', last_name: 'Tariq', username:'pharmaAdmin5',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+85@clustox.com', role: r7)
  ms_1 = User.create!(first_name: 'Khurram', last_name: 'Tariq', username:'ms1',password: 'changeit' , phone: '03211215611', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+5@clustox.com', role: r8)
  ms_2 = User.create!(first_name: 'Kamal', last_name: 'Tariq', username:'ms2',password: 'changeit' , phone: '03211215611', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+51@clustox.com', role: r8)

  fdo_7 = User.create!(first_name: 'Ata', last_name: 'Rabbi', username:'chfd10',password: 'changeit' , phone: '03211211211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+1111@clustox.com', role: r10)
  fdo_8 = User.create!(first_name: 'Omer', last_name: 'Khan', username:'chfd11',password: 'changeit' , phone: '032112311211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+9111@clustox.com', role: r10)
  fdo_9 = User.create!(first_name: 'Majid', last_name: 'Khan', username:'chfd40',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+100111@clustox.com', role: r9)
  fdo_10 = User.create!(first_name: 'Majid', last_name: 'Khan', username:'chfd41',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+10111@clustox.com', role: r9)
  fdo_11 = User.create!(first_name: 'Majid', last_name: 'Khan', username:'chfd70',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+102111@clustox.com', role: r11)
  fdo_12 = User.create!(first_name: 'Ata', last_name: 'Khan', username:'chfd71',password: 'changeit' , phone: '032112171211', nic: '1234512321111', dob: Time.now , email: 'ata.rabi+10311@clustox.com', role: r11)

  lab_13 = User.create!(first_name: 'Farooq', last_name: 'Tariq', username:'chlab1',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+7111@clustox.com', role: r6)
  lab_14 = User.create!(first_name: 'Ali', last_name: 'Tariq', username:'ch2',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+7112@clustox.com', role: r6)
  lab_15 = User.create!(first_name: 'Usman', last_name: 'Tariq', username:'chlab3',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+71113@clustox.com', role: r6)
  lab_16 = User.create!(first_name: 'Shameer', last_name: 'Tariq', username:'chlab4',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+7114@clustox.com', role: r12)
  lab_17 = User.create!(first_name: 'Sajjad', last_name: 'Tariq', username:'chlab5',password: 'changeit' , phone: '03211211211', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+7115@clustox.com', role: r12)
  ms_3 = User.create!(first_name: 'Khurram', last_name: 'Tariq', username:'chms1',password: 'changeit' , phone: '03211215611', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+115@clustox.com', role: r8)
  ms_4 = User.create!(first_name: 'Kamal', last_name: 'Tariq', username:'chms2',password: 'changeit' , phone: '03211215611', nic: '12345122222321111', dob: Time.now , email: 'ata.rabi+51111@clustox.com', role: r8)

  Device.create!(uuid: 'fb56574625fba22c', assignable_type: "MedicalUnit", assignable_id: narowal_id , operating_system: "os", os_version: "4.1", status: "active", role: r1)
  Device.create!(uuid: 'fb56574625fba22c', assignable_type: "MedicalUnit", assignable_id: ch_id , operating_system: "os", os_version: "4.1", status: "active", role: r9)
  Department.create!(title: "Department1", medical_unit_id: narowal_id)
  Department.create!(title: "Department2", medical_unit_id: ch_id)
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r1, user: fdo_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r1, user: fdo_2 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r1, user: fdo_3 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r1, user: fdo_4 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r1, user: fdo_5 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r2, user: nurse_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r3, user: mo_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r4, user: doctor_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r4, user: doctor_2 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r4, user: doctor_3 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r4, user: doctor_4 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r4, user: doctor_5 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r5, user: pharmacist_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r5, user: pharmacist_2 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r5, user: pharmacist_3 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r5, user: pharmacist_4 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r5, user: pharmacist_5 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r6, user: lab_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r6, user: lab_2 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r6, user: lab_3 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r6, user: lab_4 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r6, user: lab_5 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r7, user: pharmaAdmin_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r7, user: pharmaAdmin_2 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r7, user: pharmaAdmin_3 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r7, user: pharmaAdmin_4 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r7, user: pharmaAdmin_5 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r8, user: ms_1 )
  MedicalUnitUser.create!(medical_unit_id: narowal_id, role: r8, user: ms_2 )

  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r9, user: fdo_7 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r9, user: fdo_8 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r10, user: fdo_9 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r10, user: fdo_10 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r11, user: fdo_11 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r11, user: fdo_12 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r6, user: lab_13 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r6, user: lab_14 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r6, user: lab_15 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r12, user: lab_16 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r12, user: lab_17 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r8, user: ms_3 )
  MedicalUnitUser.create!(medical_unit_id: ch_id, role: r8, user: ms_4 )