attock = Region.where(category: 'district', name: 'Attock').first
['Fateh Jang THQ', 'Hassan Abdal THQ', 'Hazro THQ', 'Jand THQ', 'PindiGheb'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: attock)
end

bahawalnagar = Region.where(category: 'district', name: 'Bahawalnagar').first
['Haroon Abad THQ', 'Chishtian THQ', 'Fort Abbas THQ', 'Minchinabad THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: bahawalnagar)
end

bahawalpur = Region.where(category: 'district', name: 'Bahawalpur').first
['Ahmadpur East THQ', 'Hasilpur THQ', 'Khair Pur Tamewali THQ', 'Yazman THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: bahawalpur)
end

bahawalpur = Region.where(category: 'district', name: 'Bahawalpur').first
['Bahawalpur Victoria Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: bahawalpur)
end

bhakkar = Region.where(category: 'district', name: 'Bhakkar').first
['Kalurkot THQ', 'Mankera THQ', 'DaryaKhan THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: bhakkar)
end

chakwal = Region.where(category: 'district', name: 'Chakwal').first
['Choa Saiden Shah THQ', 'City Hospital Talagang THQ', 'Talagang THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: chakwal)
end

chiniot = Region.where(category: 'district', name: 'Chiniot').first
['Lalian THQ', 'Bhowana THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: chiniot)
end

faisalabad = Region.where(category: 'district', name: 'Faisalabad').first
['Jhumra THQ', 'Jaranwala THQ', 'Tandilianwala THQ', 'Sumundri THQ', 'Govt. General Hospital Samanabad'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: faisalabad)
end

faisalabad = Region.where(category: 'district', name: 'Faisalabad').first
['Allied Hospital', 'DHQ Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: faisalabad)
end

gujranwala = Region.where(category: 'district', name: 'Gujranwala').first
['Wazirabad THQ', 'Kamoke THQ', 'Noshehra Vikran THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: gujranwala)
end

gujranwala = Region.where(category: 'district', name: 'Gujranwala').first
['DHQ Teaching Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: gujranwala)
end

gujrat = Region.where(category: 'district', name: 'Gujrat').first
['Civil Hospital Jalalpur Jattan THQ', 'Civil Hospital Kotla Arab Ali Khan THQ', 'Kharian THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: gujrat)
end

gujrat = Region.where(category: 'district', name: 'Gujrat').first
['Aziz Bhatti Shaheed (DHQ) Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: gujrat)
end

hafizabad = Region.where(category: 'district', name: 'Hafizabad').first
['Pindi Bhattian THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: hafizabad)
end

jhang = Region.where(category: 'district', name: 'Jhang').first
['Shorkot THQ', 'Ahmed PurSial THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: jhang)
end

jhelum = Region.where(category: 'district', name: 'Jhelum').first
['Pd Khan THQ', 'Sohawa THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: jhelum)
end

kasur = Region.where(category: 'district', name: 'Kasur').first
['Chunian THQ', 'Pattoki THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: kasur)
end

khanewal = Region.where(category: 'district', name: 'Khanewal').first
['Jahanian THQ', 'KabirWala THQ', 'Mian Channu THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: khanewal)
end

khushab = Region.where(category: 'district', name: 'Khushab').first
['Khushab THQ', 'Noor Pur Thal THQ', 'Qaidabad THQ', 'Naushera THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: khushab)
end

lahore = Region.where(category: 'district', name: 'Lahore').first
['Govt. Hospital Shahdra THQ', 'GMH Pathi Ground THQ', 'Govt. Muzang Hospital THQ', 'GMH Chohan Road'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: lahore)
end

lahore = Region.where(category: 'district', name: 'Lahore').first
['Mayo Hospital', 'Jinnah Hospital', 'Sir Ganga Ram Hospital', 'Punjab Institute of Cardiology Hospital', 'Gov. Hospital for Psychiatric Deseases', 'Lady Aitchison Hospital', 'Sheikh Zayed Hospital', 'Services Hospital', 'General Hospital', "The Children's Hospital & The Institute Of Child Health, Lahore", 'Lady Willingdon Hospital' ,'Dental Hospital' ,'Gov. Kot Khawaja Saeed Hospital' ,'Nawaz Sharif Hospital', 'Gov. Teaching Hosiptal Shahdara', 'Said Mitha Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: lahore)
end

layyah = Region.where(category: 'district', name: 'Layyah').first
['ChowkAzam THQ', 'Kot Sultan THQ', 'Karor THQ', 'FatehPur', 'Choubara'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: layyah)
end

lodhran = Region.where(category: 'district', name: 'Lodhran').first
['KehrorPacca THQ', 'Dunya Pur THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: lodhran)
end

mandi_bahauddin = Region.where(category: 'district', name: 'Mandi Bahauddin').first
['Mandi Bahauddin THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: mandi_bahauddin)
end

mianwali = Region.where(category: 'district', name: 'Mianwali').first
['Isa Khel THQ', 'Kalabagh THQ', 'Piplan THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: mianwali)
end

multan = Region.where(category: 'district', name: 'Multan').first
['Govt. Mushtaq Lang  THQ', 'Shujabad THQ', 'Govt. Fatima Jinnah Women Hospital THQ', 'Govt. Civil Hospital Multan THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: multan)
end

multan = Region.where(category: 'district', name: 'Multan').first
['Nishtar Hospital THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: multan)
end

muzaffargarh = Region.where(category: 'district', name: 'Muzaffargarh').first
['Alipur THQ', 'Jatoi THQ', 'KotAdu THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: muzaffargarh)
end

narowal = Region.where(category: 'district', name: 'Narowal').first
['Shakargarh THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: narowal)
end

nankana_sahib = Region.where(category: 'district', name: 'Nankana Sahib').first
['Shahkot THQ', 'Sangla Hill THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: nankana_sahib)
end

okara = Region.where(category: 'district', name: 'Okara').first
['Depalpur THQ', 'HavaliLakha THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: okara)
end

pakpattan = Region.where(category: 'district', name: 'Pakpattan').first
['Arifwala THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: pakpattan)
end

rahim_yar_khan = Region.where(category: 'district', name: 'Rahim Yar Khan').first
['Liaquatpur THQ', 'Sadiqabad THQ', 'Khanpur THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: rahim_yar_khan)
end

rahim_yar_khan = Region.where(category: 'district', name: 'Rahim Yar Khan').first
['Sheikh Zayed Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: rahim_yar_khan)
end

rajanpur = Region.where(category: 'district', name: 'Rajanpur').first
['Civil Hospital Shah Wali THQ', 'Rojhan THQ', 'Jampur THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: rajanpur)
end

rawalpindi = Region.where(category: 'district', name: 'Rawalpindi').first
['Gujar Khan THQ', 'Kahuta THQ', 'Murree THQ', 'Taxila'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: rawalpindi)
end

rawalpindi = Region.where(category: 'district', name: 'Rawalpindi').first
['Holy Family', 'DHQ Hospital', 'Benazir Bhutto'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: rawalpindi)
end

sahiwal = Region.where(category: 'district', name: 'Sahiwal').first
['Chichawatni THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: sahiwal)
end

sahiwal = Region.where(category: 'district', name: 'Sahiwal').first
['Gov. Haji Abdul Qayyum Teaching Hospital', 'DHQ Teaching Hospital Sahiwal'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: sahiwal)
end

sargodha = Region.where(category: 'district', name: 'Sargodha').first
['Bhalwal THQ', 'KotMomin THQ', 'Sahiwal THQ', 'Chak No. 90/Sb', 'Bhagtanwala', 'Govt. TB Hospital Sargodha', 'Shahpur'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: sargodha)
end

sargodha = Region.where(category: 'district', name: 'Sargodha').first
['DHQ Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: sargodha)
end

sheikhupura = Region.where(category: 'district', name: 'Sheikhupura').first
['Sharaqpur Sharif THQ', 'Muridke THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: sheikhupura)
end

sialkot = Region.where(category: 'district', name: 'Sialkot').first
['Civil Hospital Daska THQ', 'Pasrur THQ', 'Sambrial THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: sialkot)
end

sialkot = Region.where(category: 'district', name: 'Sialkot').first
['Gov.t Sardar Begum Hospital', 'Allama Iqbal Memorial Hospital'].each do |title|
  MedicalUnit.create!(title: title, category: 'Teaching', region: sialkot)
end

toba_tek_singh = Region.where(category: 'district', name: 'Toba Tek Singh').first
['Govt. Eye-General Hospital THQ', 'Kamalia THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: toba_tek_singh)
end

vehari = Region.where(category: 'district', name: 'Vehari').first
['Mailsi THQ', 'Burewala THQ'].each do |title|
  MedicalUnit.create!(title: title, category: 'THQ', region: vehari)
end

narowal = Region.where(category: 'district', name: 'Narowal').first
['DHQ Narowal'].each do |title|
  MedicalUnit.create!(title: title, category: 'DHQ', region: narowal)
end

lhr = Region.where(category: 'district', name: 'Lahore').first
['Mian Munshi Hospital Lahore'].each do |title|
  MedicalUnit.create!(title: title, category: 'DHQ', region: lhr)
end

layyah = Region.where(category: 'district', name: 'Layyah').first
['DHQ Layyah'].each do |title|
  MedicalUnit.create!(title: title, category: 'DHQ', region: layyah)
end
