education = ['Middle', 'Intermediate', 'Graduation', 'Masters', 'MPhil', 'Doctorate'].each do |value|
  Lookup.create(key: value, category: 'education', value: value)
end