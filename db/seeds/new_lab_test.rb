lab_test = ['Liver Function Test', 'Renal Function Test', 'Electrolyte',
            'Lipid Profile', 'Cardaic Profile','High Perfomance Liquid Chromatography (HPLC)', 
            'Stool Examination Report','Urine Examination Report','Smear Examination Report',
            'Urine Complete Examination','Culture & Sensitivity Report','Blood Culture Report',
            'Blood Chromosome Analysis Report','Coagulation Profile','Mixing Study','Factory Assay',
            'Inhibitors Screening','Coombs Test','CSF - Complete Examination'
           ]

liver_function_test = [['Bilirubin Total', 'upto 5d: < 12.0', 'mg/dl',''],
                       ['Bilirubin Direct', 'upto 0.3', 'mg/dl',''],
                  	   ['Bilirubin Indirect', 'upto 0.6', 'mg/dl',''],
                  	   ['A.L.T.(S.G.P.T)', 'upto 1y: < 56 , upto 12y: < 39', 'U/L',''],
                  	   ['A.S.T.(S.G.P.T)', 'upto 5m: < 82 , upto 12y: < 47', 'U/L',''],
                  	   ['Alk. Phosphatase (ALP)', 'Children < 645, Male: > 80 and < 270, Female: >65 and 240', 'U/L',''],
                  	   ['TotaL Protiens', 'upto 1m: 4.6 - 6.8, upto 12y: 6.0 - 8.0', 'g/dl',''],
                  	   ['Albumin', 'upto 1m: 3.8 - 5.4, upto 12y: 3.4 - 4.8', 'g/dl',''],
                  	   ['Globulin', ' ', 'g/dl',''],
                  	   ['A/G Ratio', ' ', ' ',''],
                  	   ['TotaL Protiens', 'upto 6m: 204, upto 12y: < 45', 'U/L','']
                      ]

renal_function_test = [['Serum Urea', '16 - 40', 'mg/dl',''],
                       ['Serum Creatinine', '0.5 - 1.2', 'mg/dl','']
                      ]

electrolyte_function_test = [['Sodium (Na)', '135 - 154', 'mmol/L',''],
                             ['Potassium (K)', '3.5 - 5.4', 'mmol/L',''],
                             ['Chloride (Cl)', '95 - 107', 'mmol/L',''],
                             ['Calcium (Ca)', '8.8 - 10.8', 'mg/dl',''],
                             ['Phosphrous', '2.0 - 4.5', 'mg/dl',''],
                             ['Magnesium', '1.6 - 2.6', 'mg/dl','']
                            ]

lipid_profile_function_test = [['Triglycerides', 'upto 150', 'mg/dl',''],
                               ['Cholestrol', 'upto 200', 'mg/dl',''],
                               ['HDL', 'more than 40', 'mg/dl',''],
                               ['LDH', 'upto 105', 'mg/dl','']
                              ]

cardaic_profile_function_test = [['C.P.K', 'upto 5d: < 652, upto 12y: < 154', 'U/L',''],
                                 ['LDH', 'upto 12y: M < 683 F < 436', 'U/L',''],
                                ]

high_perfomance_liquid_chromatography = [ ['RBC',' ','x10^6/ul','CBC'],
                                          ['MCH','','pg','CBC'],
                                          ['Neutrophils',' ','%','CBC'],
                                          ['Retic Count',' ','%','CBC'],
                                          ['Hb',' ','g/dl','CBC'],
                                          ['MCHC',' ','g/dl','CBC'],
                                          ['Lymphocytes',' ','%','CBC'],
                                          ['HCT',' ','%','CBC'],
                                          ['Plt',' ','x10^3/ul','CBC'],
                                          ['Monocytes',' ','%','CBC'],
                                          ['MCV',' ','fl','CBC'],
                                          ['WBC',' ','x10^3/ul','CBC'],
                                          ['Eosinophils',' ','%','CBC'],
                                          ['Hb A','> 96.0','%','HPLC'],
                                          ['Hb A2','1.5 - 3.5 ','%','HPLC'],
                                          ['Hb F','< 1.0 (in more than 1 year)','%','HPLC'],
                                          ['Hb S',' ','%','HPLC'],
                                          ['other Hb (Hb D, Hb C)',' ','%','HPLC']
                                        ]

stool_examination_report = [ ['Colour',' ',' ','Physical Examination'],
                             ['Consistency',' ',' ','Physical Examination'],
                             ['Mucous',' ',' ','Physical Examination'],
                             ['Blood','',' ','Physical Examination'],
                             ['Occult Blood',' ',' ','Physical Examination'],
                             ['Pus Cell',' ','/HPF','Physical Examination'],
                             ['RBCs',' ','/HPF','Physical Examination'],
                             ['Cysts',' ',' ','Physical Examination'],
                             ['Ova',' ',' ','Physical Examination'],
                             ['Vegetative','',' ','Physical Examination'],
                             ['Reducing Substance',' ',' ','Chemical Examination'],
                             ['H. pylori stool antigen',' ',' ','Other Stool Test'],
                             ['C. difficile toxin',' ',' ','Other Stool Test']
                           ]

urine_examination_report = [ ['Colour',' ',' ','Physical Examination'],
                             ['Yellow',' ',' ','Physical Examination'],
                             ['Turbid',' ',' ','Physical Examination'],
                             ['Deposit',' ',' ','Physical Examination'],
                             ['Specific Gravity',' ',' ','Chemical Examination'],
                             ['pH',' ',' ','Chemical Examination'],
                             ['Protien',' ',' ','Chemical Examination'],
                             ['Ketone',' ',' ','Chemical Examination'],
                             ['Bilirubin',' ',' ','Chemical Examination'],
                             ['Leucocytes',' ',' ','Chemical Examination'],
                             ['Sugar',' ',' ','Chemical Examination'],
                             ['Blood',' ',' ','Chemical Examination'],
                             ['Bilirubin',' ',' ','Chemical Examination'],
                             ['Nitrite',' ',' ','Chemical Examination'],
                             ['Urobinlinogen',' ',' ','Chemical Examination'],
                             ['Pus Cell',' ','/HPF','Mircoscopy'],
                             ['RBCs',' ','/HPF','Mircoscopy'],
                             ['Epith Cell',' ','/HPF','Mircoscopy'],
                             ['Casts',' ','/HPF','Mircoscopy'],
                             ['Crystal',' ','/HPF','Mircoscopy'],
                             ['Organisms',' ','/HPF','Mircoscopy'],
                             ['Yeast Cell',' ','/HPF','Mircoscopy'],
                             ['Misc',' ','/HPF','Mircoscopy'],
                             ['Reducing Substance',' ',' ','Other Test On Request']
                           ]                           

smear_examination_report = [ ['Gram Stain',' ',' ','Specimen'],
                             ['Z.N.Stain',' ',' ','Specimen'],
                             ['Fungus Stain',' ',' ','Specimen'],
                           ]

urine_complete_examination = [ ['Colour',' ',' ','Physical Examination'],
                               ['Transparency',' ',' ','Physical Examination'],
                               ['Deposite',' ',' ','Physical Examination'],
                               ['Specific Gravity',' ',' ','Chemical Examination'],
                               ['pH',' ',' ','Chemical Examination'],
                               ['Protien',' ',' ','Chemical Examination'],
                               ['Ketone',' ',' ','Chemical Examination'],
                               ['Bilirubin',' ',' ','Chemical Examination'],
                               ['Leucocytes',' ',' ','Chemical Examination'],
                               ['Sugar',' ',' ','Chemical Examination'],
                               ['Blood / Haemoglobin',' ',' ','Chemical Examination'],
                               ['Bilirubin',' ',' ','Chemical Examination'],
                               ['Nitrite',' ',' ','Chemical Examination'],
                               ['Epithelial Cell',' ','/H.P.F','Mircoscopic Examination'],
                               ['Pus Cell',' ','/H.P.F','Mircoscopic Examination'],
                               ['RBCs',' ','/H.P.F','Mircoscopic Examination'],
                               ['Yeast Cell',' ','/H.P.F','Mircoscopic Examination'],
                               ['Hyaline - Casts',' ','/L.P.F','Mircoscopic Examination'],
                               ['Granular - Casts',' ','/L.P.F','Mircoscopic Examination'],
                               ['WBC - Casts',' ','/L.P.F','Mircoscopic Examination'],
                               ['RBC - Casts',' ','/L.P.F','Mircoscopic Examination'],
                               ['Calcium Oxalate',' ','/H.P.F','Mircoscopic Examination'],
                               ['Tripple Phosphate',' ','/H.P.F','Mircoscopic Examination'],
                               ['Uric Acid Crystals',' ','/H.P.F','Mircoscopic Examination'],
                               ['Mucous',' ','/H.P.F','Mircoscopic Examination'],
                               ['Bacteria',' ','/H.P.F','Mircoscopic Examination'],
                               ['Fungal Hyphae',' ','/H.P.F','Mircoscopic Examination']
                              ] 

culture_sensitivity_report = [ ['Amoxicillin (AMX)',' ',' ','Antibiotic'],
                               ['Amplicillin (AMR)',' ',' ','Antibiotic'],
                               ['Carbencillin(CAR)',' ',' ','Antibiotic'],
                               ['Pencillin (P)',' ',' ','Antibiotic'],
                               ['Co - amoxiclav (AMC)',' ',' ','Antibiotic'],
                               ['Oxcillin (OX)',' ',' ','Antibiotic'],
                               ['Cefoxitin (FOX)',' ',' ','Antibiotic'],
                               ['Teicoplanin (TEC)',' ',' ','Antibiotic'],
                               ['Vancomycin (VA)',' ',' ','Antibiotic'],
                               ['Fucidic Acid (FD)',' ',' ','Antibiotic'],
                               ['Amikacin (AK)',' ',' ','Antibiotic'],
                               ['Gentamycin (CN)',' ',' ','Antibiotic'],
                               ['Kanamycin (K)',' ',' ','Antibiotic'],
                               ['Tobramycin (TOB)',' ',' ','Antibiotic'],
                               ['Clarithomycin (CLR)',' ',' ','Antibiotic'],
                               ['Clindamycin (DA)',' ',' ','Antibiotic'],
                               ['Erythromycin (E)',' ',' ','Antibiotic'],
                               ['Lincomycin (L)',' ',' ','Antibiotic'],
                               ['Line Zolid (LZO)',' ',' ','Antibiotic'],
                               ['Cafalexin (CL)',' ',' ','Antibiotic'],
                               ['Cefradine (CE)',' ',' ','Antibiotic'],
                               ['Cefruroxime (CXM)',' ',' ','Antibiotic'],
                               ['Cefaclor (CEC)',' ',' ','Antibiotic'],
                               ['Cefixime (CFM)',' ',' ','Antibiotic'],
                               ['Cefotaxmine (CFZ)',' ',' ','Antibiotic'],
                               ['Cefepime (CIP)',' ',' ','Antibiotic'],
                               ['Levofloxcin (LEV)',' ',' ','Antibiotic'],
                               ['Moxifloxacin (MXI)',' ',' ','Antibiotic'],
                               ['Naladixic Acid (N)',' ',' ','Antibiotic'],
                               ['Norfloxaxin (NOP)',' ',' ','Antibiotic'],
                               ['Ofloxacin (OFX)',' ',' ','Antibiotic'],
                               ['Pipedemic Acid (OFX)',' ',' ','Antibiotic'],
                               ['Sulbactam / Cefoperazone (SCF)',' ',' ','Antibiotic'],
                               ['Colistic Sulphate (CT)',' ',' ','Antibiotic'],
                               ['Metronidazole (MET)',' ',' ','Antibiotic'],
                               ['Fosfomycin (FOS)',' ',' ','Antibiotic'],
                               ['Meropenam (MEM)',' ',' ','Antibiotic'],
                               ['Imipenem (IPM)',' ',' ','Antibiotic'],
                               ['Aztreonam (ATM)',' ',' ','Antibiotic'],
                               ['Chloramphenical (C)',' ',' ','Antibiotic'],
                               ['Piperacillin / Tazobactam (TZP)',' ',' ','Antibiotic'],
                               ['Co - trimoxazole (SXT)',' ',' ','Antibiotic'],
                               ['Polymaxin B (PB)',' ',' ','Antibiotic'],
                               ['Rifampacin (RA)',' ',' ','Antibiotic'],
                              ] 

blood_culture_report = [ ['Comments',' ',' ',''] 
                       ] 

blood_chromosome_analysis_report = [ ['Comments',' ',' ',''] 
                       ]                             

coagulation_profile = [ ['INR',' ','Sec',''],
                        ['Prethrombin Time (PT)',' ','Sec',''],
                        ['APTT',' ','Sec',''],
                        ['Control',' ','Sec','']
                       ]

mixing_study = [  ['Patient Plasma + Normal Plasma','','',''],
                  ['Patient Plasma + Adsorb Plasma','','',''],
                  ['Patient Plasma + Aged Plasma','','',''],
               ] 

factory_assay = [ ['PT','11 Sec - 14 Sec','Sec',''],
                  ['APTT','31 Sec - 36 Sec','Sec',''],
                  ['Thrombin Time','14 Sec - 20 Sec','Sec',''],
                  ['Factor V','72 - 96 UI/ml','UI/ml',''],
                  ['Factor VII','77 - 113 UI/ml','UI/ml',''],
                  ['Factor VIII','77 - 107 %','%',''],
                  ['Factor IX','93 - 131 UI/ml','UI/ml',''],
                  ['Factor X','74 - 100 UI/ml','UI/ml',''],
                  ['Protien C','83 - 113 %','%',''],
                  ['Protien S','70 - 110 %','%',''],
                  ['At III','76 - 102 %','%',''],
                  ['Factor VWF','74 - 94 %','%',''],
                  ['Factor XI','79 - 109 %','%',''],
                  ['Factor XII','80 - 110 %','%',''],
               ] 

inhibitors_screening = [  ['Conclusion','','',''],
                          ['Opinion','','','']
                       ]                     

coombs_test = [  ['DAT','','',''],
                 ['IAT','','','']
              ]

complete_examination = [  ['Volume','','ml',''],
                          ['Appearance','','',''],
                          ['WBC Count','','',''],
                          ['Polymorphs','','/ul',''],
                          ['Lymphocytes','','%',''],
                          ['RBC Count','','%',''],
                          ['Suagr','','/ul',''],
                          ['Protien','','mg/dl','40 - 70 mg/dl'],
                          ['Gram Stain','','mg/dl','15 - 40 mg/dl'],
                          ['Comments','','',''],
                       ]

lab_test.each do |value|
  LabStudy.create(name: value)
end

lft = LabStudy.where(name: 'Liver Function Test').first
liver_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: lft.id)
end

rft = LabStudy.where(name: 'Renal Function Test').first
renal_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: rft.id)
end

eft = LabStudy.where(name: 'Electrolyte').first
electrolyte_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: eft.id)
end

lpft = LabStudy.where(name: 'Lipid Profile').first
lipid_profile_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: lpft.id)
end

cpft = LabStudy.where(name: 'Cardaic Profile').first
cardaic_profile_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: cpft.id)
end

hplc = LabStudy.where(name: 'High Perfomance Liquid Chromatography (HPLC)').first
high_perfomance_liquid_chromatography.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: hplc.id)
end
ser = LabStudy.where(name: 'Stool Examination Report').first
stool_examination_report.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: ser.id)
end

uer = LabStudy.where(name: 'Urine Examination Report').first
urine_examination_report.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       test_label: value[3],lab_study_id: uer.id)
end

smearer = LabStudy.where(name: 'Smear Examination Report').first
smear_examination_report.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: smearer.id)
end

uce = LabStudy.where(name: 'Urine Complete Examination').first
urine_complete_examination.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: uce.id)
end

csr = LabStudy.where(name: 'Culture & Sensitivity Report').first
culture_sensitivity_report.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: csr.id)
end

bcr = LabStudy.where(name: 'Blood Culture Report').first
blood_culture_report.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: bcr.id)
end

bcar = LabStudy.where(name: 'Blood Chromosome Analysis Report').first
blood_chromosome_analysis_report.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: bcar.id)
end

cp = LabStudy.where(name: 'Coagulation Profile').first
coagulation_profile.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: cp.id)
end

ms = LabStudy.where(name: 'Mixing Study').first
mixing_study.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: ms.id)
end

fa = LabStudy.where(name: 'Factory Assay').first
factory_assay.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: fa.id)
end

is = LabStudy.where(name: 'Inhibitors Screening').first
inhibitors_screening.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: is.id)
end

ct = LabStudy.where(name: 'Coombs Test').first
coombs_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: ct.id)
end

ce = LabStudy.where(name: 'CSF - Complete Examination').first
complete_examination.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                      test_label: value[3], lab_study_id: ce.id)
end