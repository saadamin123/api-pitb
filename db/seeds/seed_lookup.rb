visitreason = ['Injury', 'Diarrhea', 'Pregnancy', 'Congestion', 'Headache', 'Chest Pain', 'Ear Pain/Infection', 'Respiration', 'Dental'].each do |value|
  Lookup.create(key: value, category: 'visit-reason', value: value)
end

visiterreferredto = ['Emergency', 'Outpatient', 'Inpatient', 'Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value)
end

modeofconveyance = ['walk-in', 'Public Transport', 'Private Transport', '1122', 'Hospital ambulance', 'Private ambulance'].each do |value|
  Lookup.create(key: value, category: 'mode-of-conveyance', value: value)
end

education = ['Primary', 'Matriculation'].each do |value|
  Lookup.create(key: value, category: 'education', value: value)
end

relationship = ['Father', 'Mother'].each do |value|
  Lookup.create(key: value, category: 'relationship', value: value)
end

emergency = Lookup.where(category: 'visiter-referred-to', value: 'Emergency').first
['ICU', 'CCU', 'Cardio', 'Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: emergency.id)
end

outpatient = Lookup.where(category: 'visiter-referred-to', value: 'Outpatient').first
['ENT', 'Medicine', 'Dermatology', 'Pediatrics', 'Orthopedics', 'Urology'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: outpatient.id)
end

inpatient = Lookup.where(category: 'visiter-referred-to', value: 'Inpatient').first
['Cardio', 'Ophthalmology', 'Surgery', 'Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: inpatient.id)
end

other = Lookup.where(category: 'visiter-referred-to', value: 'Other').first
['Psychiatry'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: other.id)
end

complaint_category = ['Personal/Staff', 'Medical Facilities', 'Infrastructure', 'Management'].each do |value|
  Lookup.create(key: value, category: 'complaint_category', value: value)
end

staff = Lookup.where(category: 'complaint_category', value: 'Personal/Staff').first
['Workload', 'Salary related', 'Harassment', 'Other'].each do |value|
  Lookup.create(key: value, category: 'complaint_category', value: value, parent_id: staff.id)
end

medical_facilities = Lookup.where(category: 'complaint_category', value: 'Medical Facilities').first
['Beds', 'Medicines', 'Medical Equipment', 'Labs', 'Supplies', 'Ambulance', 'Other'].each do |value|
  Lookup.create(key: value, category: 'complaint_category', value: value, parent_id: medical_facilities.id)
end

infrastructure = Lookup.where(category: 'complaint_category', value: 'Infrastructure').first
['Power/Generators', 'Water', 'Toilets', 'Building', 'Other'].each do |value|
  Lookup.create(key: value, category: 'complaint_category', value: value, parent_id: infrastructure.id)
end

management = Lookup.where(category: 'complaint_category', value: 'Management').first
['Corruption', 'Other'].each do |value|
  Lookup.create(key: value, category: 'complaint_category', value: value, parent_id: management.id)
end

ms = Role.where(title: 'MS').first
complaint_role = [ms.id].each do |value|
  Lookup.create(key: value, category: 'complaint_role', value: 'MS')
end

medicine_type = ['Brand name'].each do |value|
  Lookup.create(key: value, category: 'MEDICINE-TYPE', value: value)
end

medicine_order_type = ['Hospital Order', 'Health Dept Order', 'Donation'].each do |value|
  Lookup.create(key: value, category: 'MEDICINE-ORDER-TYPE', value: value)
end

medicine_order_status = ['Completed', 'Partially Filled'].each do |value|
  Lookup.create(key: value, category: 'MEDICINE-ORDER-STATUS', value: value)
end

medicine_audit_reason = ['Human Error', 'Theft', 'Expired', 'Other'].each do |value|
  Lookup.create(key: value, category: 'MEDICINE-AUDIT-REASON', value: value)
end


