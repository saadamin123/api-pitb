strength_unit = ['blank', 'mg', 'g', 'mcg', 'units', 'meq', 'ml'].each do |value|
  Lookup.create(key: value, category: 'Strength_Unit', value: value)
end

frequency_dropdown_1 = ['Once', 'Q1', 'Q2', 'Q3', 'Q4', 'Q6', 'Q8', 'Q8', 'Q12', 'Q24', 'Q48', 'Q72', 'QID', 'BID', 'TID'].each do |value|
  Lookup.create(key: value, category: 'Frequency_dropdown_1', value: value)
end

frequency_dropdown_2 = ['blank', 'Min', 'Hour', 'Day'].each do |value|
  Lookup.create(key: value, category: 'Frequency_dropdown_2', value: value)
end

duration = ['1 day', '2 days', '3 days', '4 days', '5 days', '6 days', '7 days'].each do |value|
  Lookup.create(key: value, category: 'Duration', value: value)
end

route = ['PO', 'SQ', 'IV', 'IM', 'Inhalation', 'Sublingual', 'Ophthalmic', 'Intraauricular', 'Intradermal', 'Intranasal', 'Rectal', 'Intrathecal', 'Intraarticular', 'Intraarterial'].each do |value|
  Lookup.create(key: value, category: 'Route', value: value)
end