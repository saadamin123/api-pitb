lab_study = ['Complete Blood Count - CBC', 'WBC Differential',
             'Renal Function Test - RFT', 'Liver Function Test - LFT',
             'Erythrocyte Sedimentation Rate (ESR)', 'Blood Glucose Test',
             'Thyroid Function Test']

complete_blood_count = [['White Blood Cell (WBC)', '4.8 - 10.8', 'K/mcl'],
                        ['Red Blood Cell (RBC)', '4.7 - 6.1', 'M/mcl'],
                        ['Hemoglobin (HB/Hgb)', '14.0 - 18.0', 'g/dL'],
                        ['Hematocrit (HCT)', '42 - 52', '%'],
                        ['Mean Cell Volume (MCV)', '80 - 100', 'fL'],
                        ['Mean Cell Hemoglobin (MCH)', '27.0 - 32.0', 'pg'],
                        ['Mean Cell Hb Conc (MCHC)', '32.0 - 36.0', 'g/dL'],
                        ['Red Cell Dist Width (RDW)', '11.5 - 14.5', '%'],
                        ['Platelet count', '150 - 450', 'K/mcL'],
                        ['Mean Platelet Volume', '7.5 - 11.0', 'fL']]

wbc_differential = [['Neutrophil (Neut)', '33 - 73', '%'],
                    ['Lymphocyte (Lymph)', '13 - 52', '%'],
                    ['Monocyte (Mono)', '0 - 10', '%'],
                    ['Eosinophil (Eos)', '0 - 5', '%'],
                    ['Basophil (Baso)', '0 - 2', '%'],
                    ['Neutrophil, Absolute', '1.8 - 7.8', 'K/mcL'],
                    ['Lymphocyte, Absolute', '1.0 - 4.8', 'K/mcL'],
                    ['Monocyte, Absolute', '0 - 0.8', 'K/mcL'],
                    ['Eosinophil, Absolute', '0 - 0.45', 'K/mcL'],
                    ['Basophil, Absolute', '0 - 0.2', 'K/mcL']]

# lipid_profile = [['Total Cholesterol', 'Below 200', 'mg/dL'],
#                  ['LDL Cholesterol', 'Below 130', 'md/dL'],
#                  ['HDL Cholesterol', 'Above 39', 'mg/dL'],
#                  ['Triglyceride', 'Below 150', 'md/dL'],
#                  ['Non HDL Cholesterol', 'Below 160', 'mg/DL'],
#                  ['Total Chol/HDL Ratio', '', 'Ratio'],
#                  ['LDL/HDL Ratio', '', 'Ratio'],
#                  ['LDL Particle Number', 'Below 1000', 'nmol/L'],
#                  ['Small LDL P', 'Below 527', 'nmol/L'],
#                  ['LDL Size', 'Below 20.5', 'nm'],
#                  ['HDL P', 'Above 30.5', 'umol/L'],
#                  ['Large HDL P', 'Above 4.8', 'umol/L'],
#                  ['HDL Size', 'Above 9.2', 'nm'],
#                  ['VLDL Cholesterol', 'Below 40', 'md/dL'],
#                  ['Large VLDL P', 'Below 2.7', 'nmol/L'],
#                  ['VLDL Size', 'Below 46.6', 'nm'],
#                  ['LP IR', 'Below 45', ''],
#                  ['Lipoprotein A', 'Below 30', 'md/dL']]

renal_function_test = [['Sodium', '136 - 145', 'mmol/L'],
                       ['Potassium', '3.6 - 5.2', 'mmol/L'],
                       ['Chloride', '100 - 108', 'mmol/L'],
                       ['Total CO2', '21.0 - 30.0', 'mmol/L'],
                       ['Anion Gap', '10 - 20', 'mmol/L'],
                       ['Urea', '2.5 - 6.4', 'mmol/L'],
                       ['Creatinine', '80 - 132', 'umol/L']]

liver_function_test = [['Total Protein', '64 - 82', 'g/L'],
                       ['Albumin', '35 - 50', 'g/L'],
                       ['Globulin', '23 - 35', 'g/L'],
                       ['Total Bilirubin', '3 - 17', 'umol/L'],
                       ['Conjugated Bilirubin', '0 - 3', 'umol/L'],
                       ['Alk. Phosphatase', '50 - 136', 'IU/L'],
                       ['ALT (GPT)', '12 - 78', 'IU/L'],
                       ['AST (GOT)', '15 - 37', 'IU/L'],
                       ['Gamma GT', '15 - 85', 'IU/L']]

erythrocyte_sedimentation_rate = [['ESR', '<20', 'mm/h']]

blood_glucose_test = [['Routine', '80 - 110', 'mg/dL'],
                      ['Post-Prandial', '140 - 200', 'md/dL'],
                      ['Pre-Prandial', '90 - 130', 'mg/dL'],
                      ['Post-Prandial', '140 - 200', 'md/dL'],
                      ['Post Prandial', '140 - 200', 'mg/DL']]

thyroid_function_test = [['Serum Thyroxine (T4)', '4.6 - 12', 'ug/dl'],
                         ['Free Thyroxine fraction (FTF4)',
                          '0.03 - 0.005', '%'],
                         ['Free Thyroxine (FT4)', '0.7 - 1.9', 'ng/dl'],
                         ['Thyroid Hormone Binding Ratio (THBR)',
                          '0.9 - 1.1', ''],
                         ['Free Thyroxine Index (FT4I)', '4 - 11', ''],
                         ['Serum Triiodothyronine (T3)', '80 - 180', 'ng/dl'],
                         ['Free Triiodothyronine I (FT3)', '230 - 619', 'pg/d'],
                         ['Free T3 Index (FT3I)', '80 - 180', ''],
                         ['Radioactive Iodine Uptake (RAIU)', '10 - 30', '%'],
                         ['Serum Thyrotropin (TSH)', '0.5 - 0.6', 'uU/ml'],
                         ['Thyroxine-Binding Globulin (TBG)',
                          '12-20 ug/dl T4+1.8 ugm', ''],
                         ['TRH Stimulation test Peak (TSH)',
                          '9-30 ulU/ml at 20-30 min', ''],
                         ['Serum Thyroglobulin I (Tg)', '0 - 30', 'ng/m'],
                         ['Thyroid microsomal antibody titer (TMAb)',
                          'Varies with method', ''],
                         ['Thyroglobulin antibody titer (TgAb)',
                          'Varies with method', '']]

lab_study.each do |value|
  LabStudy.create(name: value)
end

cbc = LabStudy.where(name: 'Complete Blood Count - CBC').first
complete_blood_count.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: cbc.id)
end

wbc = LabStudy.where(name: 'WBC Differential').first
wbc_differential.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: wbc.id)
end

# lp = LabStudy.where(name: 'Lipid Profile').first
# lipid_profile.each do |value|
#   StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
#                        lab_study_id: lp.id)
# end

rft = LabStudy.where(name: 'Renal Function Test - RFT').first
renal_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: rft.id)
end

lft = LabStudy.where(name: 'Liver Function Test - LFT').first
liver_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: lft.id)
end

esr = LabStudy.where(name: 'Erythrocyte Sedimentation Rate (ESR)').first
erythrocyte_sedimentation_rate.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: esr.id)
end

bgt = LabStudy.where(name: 'Blood Glucose Test').first
blood_glucose_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: bgt.id)
end

tft = LabStudy.where(name: 'Thyroid Function Test').first
thyroid_function_test.each do |value|
  StudyTemplate.create(caption: value[0], range: value[1], unit: value[2],
                       lab_study_id: tft.id)
end
