class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :uuid
      t.references :assignable, :polymorphic => true
      t.references :role
      t.string :operating_system
      t.string :os_version
      t.string :status
      t.float :latitude
      t.float :longitude	
      t.timestamps null: false
    end
  end
end
