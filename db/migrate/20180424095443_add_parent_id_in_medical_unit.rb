class AddParentIdInMedicalUnit < ActiveRecord::Migration
  def change
  	add_column :medical_units, :parent_id, :integer
  end
end
