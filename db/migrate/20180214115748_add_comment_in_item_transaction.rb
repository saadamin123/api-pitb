class AddCommentInItemTransaction < ActiveRecord::Migration
  def change
  	add_column :item_transactions, :comment, :string
  end
end
