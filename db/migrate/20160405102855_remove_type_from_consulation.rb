class RemoveTypeFromConsulation < ActiveRecord::Migration
  def change
  	remove_column :consulation_order_details, :type, :string
    add_column :consulation_order_details, :consulation_type, :string
  end
end
