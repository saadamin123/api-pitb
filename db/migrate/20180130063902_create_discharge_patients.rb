class CreateDischargePatients < ActiveRecord::Migration
  def change
    create_table :discharge_patients do |t|
      t.integer :medical_unit_id
      t.integer :patient_id
      t.integer :bed_id
      t.integer :admission_id
      t.string :treatment_plan
      t.string :medications
      t.string :follow_up
      t.string :discharge_type
      t.string :comments

      t.timestamps null: false
    end
  end
end
