class AddGaurdianFieldInPat < ActiveRecord::Migration
  def change
  	add_column :patients, :guardian_state, :string
  	add_column :patients, :guardian_city, :string
  	add_column :patients, :guardian_near_by_city, :string
  	add_column :patients, :guardian_address, :string
  	add_column :patients, :guardian_phone_type, :string
  	add_column :patients, :guardian_phone, :string
  	add_column :patients, :barcode_url, :string
  	add_column :patients, :qrcode_url, :string
  end
end
