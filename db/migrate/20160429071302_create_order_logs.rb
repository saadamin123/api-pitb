class CreateOrderLogs < ActiveRecord::Migration
  def change
    create_table :order_logs do |t|
      t.references :order
      t.references :lab_order_detail
      t.string :status
      t.string :updated_by
      t.timestamps null: false
    end
  end
end
