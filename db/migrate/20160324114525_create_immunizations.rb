class CreateImmunizations < ActiveRecord::Migration
  def change
    create_table :immunizations do |t|
      t.references :patient
      t.references :user
      t.references :medical_unit
      t.references :visit
      t.string :age
      t.string :vaccine
      t.string :dose
      t.date :due
      t.date :given_on
      t.string :location
      t.string :administred_by

      t.timestamps null: false
    end
  end
end
