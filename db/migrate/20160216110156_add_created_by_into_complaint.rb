class AddCreatedByIntoComplaint < ActiveRecord::Migration
  def change
  	add_column :complaints, :created_by, :string
  end
end
