class AddDepartmentCodeToDepartment < ActiveRecord::Migration
  def change
  	add_column :departments, :department_code, :string
  end
end
