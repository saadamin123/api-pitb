class AddColumnReasonToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :reason, :string
  end
end
