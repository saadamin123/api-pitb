class AddLookupIdToDepartment < ActiveRecord::Migration
  def change
    add_column :departments, :lookup_id, :integer
  end
end
