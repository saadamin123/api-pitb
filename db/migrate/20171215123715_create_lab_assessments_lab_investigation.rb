class CreateLabAssessmentsLabInvestigation < ActiveRecord::Migration
  def change
    create_table :lab_assessments_investigations do |t|
    	t.belongs_to :lab_assessment
    	t.belongs_to :lab_investigation
    	t.integer :sequence
    	t.string :label
    end
  end
end
