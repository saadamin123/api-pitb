class CreateConsulationOrderDetails < ActiveRecord::Migration
  def change
    create_table :consulation_order_details do |t|
      t.references :order
      t.string :department
      t.string :description
      t.string :type

      t.timestamps null: false
    end
  end
end
