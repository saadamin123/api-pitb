class AddColumnTypeToProcedureOrderDetails < ActiveRecord::Migration
  def change
    add_column :procedure_order_details, :type, :string
  end
end
