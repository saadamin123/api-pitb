class AddForeignKeysToLabOrderDetails < ActiveRecord::Migration
  def change
  	add_column :lab_order_details, :order_generator_id, :integer
  	add_column :lab_order_details, :sample_collector_id, :integer
  	add_column :lab_order_details, :lab_technician_id, :integer
  	add_column :lab_order_details, :pathologist_id, :integer
  	
  	add_index :lab_order_details, :order_generator_id
  	add_index :lab_order_details, :lab_technician_id
  	add_index :lab_order_details, :pathologist_id
  	add_index :lab_order_details, :sample_collector_id
  end
end
