class AddColumnDosageToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :dosage, :string
    add_column :order_items, :dosage_unit, :string
  end
end
