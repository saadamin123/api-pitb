class RenameTablePatientLocations < ActiveRecord::Migration
  def change
  	rename_table :patient_locations, :visit_histories
  end
end
