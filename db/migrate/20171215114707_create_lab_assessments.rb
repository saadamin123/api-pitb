class CreateLabAssessments < ActiveRecord::Migration
  def change
    create_table :lab_assessments do |t|
    	t.string :title
    	t.string :uom
    	t.string :specimen
    	t.string :status
    	t.string :result_type1
      t.string :result_type2
    	t.integer :medical_unit_id
      t.string :parameter_code

      t.timestamps null: false
    end
  end
end
