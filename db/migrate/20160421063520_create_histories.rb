class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.references :patient, index: true, foreign_key: true
      t.text :history
      t.string :type

      t.timestamps null: false
    end
  end
end
