class AssociatVisitWithPicture < ActiveRecord::Migration
  def change
  	add_reference :pictures, :visit, index: true
  end
end
