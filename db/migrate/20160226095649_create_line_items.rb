class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :order_id
      t.integer :brand_drug_id
      t.integer :quantity
      t.string :category
      t.integer :added_by
      t.integer :updated_by
      t.date :expiry
      t.integer :issue_id
      t.string :issue_type
      t.integer :medical_unit_id
      t.integer :device_id
      t.integer :inventory_id
      t.string :order_type
      t.string :status
      t.text :description

      t.timestamps null: false
    end
  end
end
