class AddDeletedAtToBed < ActiveRecord::Migration
  def change
    add_column :beds, :deleted_at, :datetime
    add_index :beds, :deleted_at
  end
end
