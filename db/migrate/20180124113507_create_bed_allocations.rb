class CreateBedAllocations < ActiveRecord::Migration
  def change
    create_table :bed_allocations do |t|
      t.integer  :medical_unit_id
      t.integer  :patient_id
      t.integer  :admission_id
      t.integer  :ward_id
      t.integer  :bay_id
      t.integer  :bed_id
      t.boolean  :status
      t.integer   :patient_last_updated_by
      t.integer   :user_id
      t.integer   :visit_id

      t.timestamps null: false
    end
  end
end
