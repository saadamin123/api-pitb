class AddNoteToLabResults < ActiveRecord::Migration
  def change
  	add_column :lab_results, :note, :text
  end
end
