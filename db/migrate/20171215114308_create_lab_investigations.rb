class CreateLabInvestigations < ActiveRecord::Migration
  def change
    create_table :lab_investigations do |t|
    	t.string :profile_name
    	t.integer :medical_unit_id
    	t.string :status
    	t.integer :lab_investigation_type_id
      t.string :profile_code
      t.string :lab_section
      t.string :report_type

      t.timestamps null: false
    end
  end
end
