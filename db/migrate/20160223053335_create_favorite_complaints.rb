class CreateFavoriteComplaints < ActiveRecord::Migration
  def change
    create_table :favorite_complaints do |t|
      t.references :complaint
      t.references :user
      t.timestamps null: false
    end
  end
end
