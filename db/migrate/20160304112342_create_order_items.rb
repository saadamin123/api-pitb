class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :brand_drug_id
      t.integer :quantity_prescribed
      t.integer :frequency
      t.string :frequency_unit
      t.string :duration
      t.integer :quantity_calculated
      t.integer :issued_brand_drug_id
      t.text :notes

      t.timestamps null: false
    end
  end
end
