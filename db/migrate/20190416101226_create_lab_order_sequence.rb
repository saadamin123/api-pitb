class CreateLabOrderSequence < ActiveRecord::Migration
  def change
    create_table :lab_order_sequences do |t|
      t.integer :medical_unit_id
      t.string :lab_sequence
      t.timestamps null: false
    end
  end
end
