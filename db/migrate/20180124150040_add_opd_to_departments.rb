class AddOpdToDepartments < ActiveRecord::Migration
  def change
    add_column :departments, :opd, :boolean,:default => false
  end
end
