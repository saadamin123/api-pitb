class AddPlanColIntoDiagnosis < ActiveRecord::Migration
  def change
    add_column :patient_diagnoses, :plan, :text
  end
end
