class AddPatientFeildsIntoPatient < ActiveRecord::Migration
  def change
    add_column :patients, :blood_group, :string
    add_column :patients, :hiv, :string
    add_column :patients, :hepatitis_b_antigens, :string
    add_column :patients, :hepatitis_c, :string
  end
end
