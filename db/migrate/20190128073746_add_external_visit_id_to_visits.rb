class AddExternalVisitIdToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :external_visit_number, :string
  end
end
