class CreateAddAdmissions < ActiveRecord::Migration
  def change
    create_table :add_admissions do |t|
      t.references :order
      t.string :category
      t.string :department
      t.string :floor_number
      t.string :bed_number
      t.string :bay_number
      t.timestamps null: false
    end
  end
end
