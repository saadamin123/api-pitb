class CreateLabResults < ActiveRecord::Migration
  def change
    create_table :lab_results do |t|
    	t.integer :patient_id
    	t.integer :medical_unit_id
    	t.integer :lab_investigation_id
    	t.integer :lab_order_detail_id

      t.timestamps null: false
    end
  end
end
