class AddColumnInstructionsToRadiologieOrderDetails < ActiveRecord::Migration
  def change
    add_column :radiologie_order_details, :instructions, :text
  end
end
