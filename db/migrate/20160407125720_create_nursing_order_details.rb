class CreateNursingOrderDetails < ActiveRecord::Migration
  def change
    create_table :nursing_order_details do |t|
      t.references :order
      t.string :department
      t.string :instructions
      t.timestamps null: false
    end
  end
end
