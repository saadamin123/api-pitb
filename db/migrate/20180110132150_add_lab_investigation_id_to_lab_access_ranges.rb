class AddLabInvestigationIdToLabAccessRanges < ActiveRecord::Migration
  def change
  	add_column :lab_access_ranges, :lab_investigation_id, :integer
  end
end
