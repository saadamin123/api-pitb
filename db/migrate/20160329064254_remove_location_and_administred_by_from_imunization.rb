class RemoveLocationAndAdministredByFromImunization < ActiveRecord::Migration
  def change
  	remove_column :immunizations, :location, :string
  	remove_column :immunizations, :administred_by, :string
  end
end
