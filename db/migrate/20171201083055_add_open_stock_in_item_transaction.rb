class AddOpenStockInItemTransaction < ActiveRecord::Migration
  def change
  	add_column :item_transactions, :open_stock, :string
  end
end
