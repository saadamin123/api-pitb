class AddCancelReasonInOrder < ActiveRecord::Migration
  def change
    add_column :orders, :cancel_reason, :string
    add_column :orders, :cancel_reason_details, :string
  end
end
