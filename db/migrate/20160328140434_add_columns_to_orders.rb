class AddColumnsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :prn, :boolean
    add_column :orders, :route, :string
  end
end
