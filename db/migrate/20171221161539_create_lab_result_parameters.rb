class CreateLabResultParameters < ActiveRecord::Migration
  def change
    create_table :lab_result_parameters do |t|
    	t.string :parameter_title
    	t.string :result1
    	t.string :result2
    	t.string :uom
    	t.string :range_title
    	t.integer :lab_result_id
    	t.boolean :out_of_range

      t.timestamps null: false
    end
  end
end
