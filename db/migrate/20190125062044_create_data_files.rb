class CreateDataFiles < ActiveRecord::Migration
  def change
    create_table :data_files do |t|
      t.integer :medical_unit_id
      t.integer :user_id
      t.string :file_url
      t.string :status

      t.timestamps null: false
    end
  end
end
