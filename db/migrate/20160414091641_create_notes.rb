class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.references :visit, index: true, foreign_key: true
      t.string :department
      t.text :chief_complaint
      t.text :med_history
      t.text :surgical_history
      t.text :physical_exam
      t.text :instructions
      t.text :plan
      t.string :type

      t.timestamps null: false
    end
  end
end
