class AddColumnsToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :current_ref_department, :string
    add_column :visits, :current_referred_to, :string
  end
end
