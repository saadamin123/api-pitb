class AddColumnToLabOrderDetails < ActiveRecord::Migration
  def change
  	add_column :lab_order_details, :lab_investigation_id, :integer
  end
end
