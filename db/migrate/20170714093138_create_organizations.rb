class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.references :user, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
