class CreateUserWards < ActiveRecord::Migration
  def change
    create_table :user_wards do |t|
      t.integer :user_id
      t.integer :ward_id

      t.timestamps null: false
    end
  end
end
