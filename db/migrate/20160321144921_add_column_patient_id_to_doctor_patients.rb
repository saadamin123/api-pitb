class AddColumnPatientIdToDoctorPatients < ActiveRecord::Migration
  def change
    add_column :doctor_patients, :patient_id, :integer
    remove_column :doctor_patients, :patient_mrn, :string
  end
end
