class AddDeletedAtToWard < ActiveRecord::Migration
  def change
    add_column :wards, :deleted_at, :datetime
    add_index :wards, :deleted_at
  end
end
