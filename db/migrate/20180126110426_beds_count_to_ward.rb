class BedsCountToWard < ActiveRecord::Migration
  def change
  	add_column :wards, :beds_count, :integer, :default => 0
  end
end
