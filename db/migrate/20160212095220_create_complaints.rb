class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.references :user
      t.references :medical_unit
      t.references :device
      t.string :category
      t.string :area
      t.string :description
      t.string :severity_level
      t.string :show_identity
      t.string :status
      t.timestamps null: false
    end
  end
end
