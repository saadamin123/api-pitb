class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.references :patient
      t.string :number
      t.string :category	
      t.timestamps null: false
    end
  end
end
