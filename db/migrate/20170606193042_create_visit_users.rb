class CreateVisitUsers < ActiveRecord::Migration
  def change
    create_table :visit_users do |t|
      t.references :user, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.boolean :is_checkout, default: false

      t.timestamps null: false
    end
  end
end
