class CreateLabOrderDetails < ActiveRecord::Migration
  def change
    create_table :lab_order_details do |t|
      t.references :order, index: true, foreign_key: true
      t.string :test
      t.string :detail
      t.boolean :sample_collected

      t.timestamps null: false
    end
  end
end
