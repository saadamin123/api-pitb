class AddAssignToRoleIntoComplaint < ActiveRecord::Migration
  def change
  	add_column :complaints, :assign_to_role, :integer
  end
end
