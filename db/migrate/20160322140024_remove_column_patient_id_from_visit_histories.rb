class RemoveColumnPatientIdFromVisitHistories < ActiveRecord::Migration
  def change
  	add_column :visit_histories, :visit_id, :integer
    remove_column :visit_histories, :patient_id, :integer
  end
end
