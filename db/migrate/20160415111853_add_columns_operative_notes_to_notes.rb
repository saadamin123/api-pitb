class AddColumnsOperativeNotesToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :subjective, :text
    add_column :notes, :operation_date, :date
    add_column :notes, :procedure_indications, :text
    add_column :notes, :surgeon_assistant_note, :text
    add_column :notes, :procedure_description, :text
    add_column :notes, :anesthesia, :text
    add_column :notes, :complications, :text
    add_column :notes, :specimens, :text
    add_column :notes, :blood_loss, :string
  end
end
