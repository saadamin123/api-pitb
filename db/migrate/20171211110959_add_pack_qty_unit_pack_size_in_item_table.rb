class AddPackQtyUnitPackSizeInItemTable < ActiveRecord::Migration
  def change
  	add_column :items, :pack, :float, :default => 1
  	add_column :items, :quantity, :float, :default => 1
  	add_column :items, :unit, :float, :default => 1
  	add_column :items, :pack_size, :float, :default => 1
  end
end
