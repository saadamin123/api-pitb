class CreateBrandDrugs < ActiveRecord::Migration
  def change
    create_table :brand_drugs do |t|
      t.string :name
      t.string :category
      t.string :form
      t.integer :dumb
      t.string :packing
      t.float :trade_price
      t.float :retail_price
      t.string :mg
      t.integer :drug_id
      t.integer :brand_id

      t.timestamps null: true
    end
  end
end
