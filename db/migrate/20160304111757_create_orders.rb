class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :patient
      t.integer :doctor_id
      t.string :status
      t.integer :medical_unit_id
      t.integer :device_id
      t.integer :updated_by

      t.timestamps null: false
    end
  end
end
