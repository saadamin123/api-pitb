class AddDefaultValueToQuantityInInventories < ActiveRecord::Migration
  def change
    change_column_default :inventories, :quantity, 0
  end
end
