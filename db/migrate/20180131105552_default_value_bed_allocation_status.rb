class DefaultValueBedAllocationStatus < ActiveRecord::Migration
  def change
  	change_column :bed_allocations, :status, :string, :default => 'Free'
  end
end
