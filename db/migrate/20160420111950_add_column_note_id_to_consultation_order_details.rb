class AddColumnNoteIdToConsultationOrderDetails < ActiveRecord::Migration
  def change
    add_column :consultation_order_details, :note_id, :integer
  end
end
