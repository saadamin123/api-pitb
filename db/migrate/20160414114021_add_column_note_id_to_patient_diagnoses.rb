class AddColumnNoteIdToPatientDiagnoses < ActiveRecord::Migration
  def change
    add_column :patient_diagnoses, :note_id, :integer
  end
end
