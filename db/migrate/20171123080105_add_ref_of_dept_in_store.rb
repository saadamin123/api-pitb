class AddRefOfDeptInStore < ActiveRecord::Migration
  def change
  	add_reference :stores, :department, index: true
  end
end
