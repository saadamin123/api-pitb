class ChangeTypeOfCriticalAttr < ActiveRecord::Migration
  def change
  	change_column :complaints, :critical, :string
  end
end
