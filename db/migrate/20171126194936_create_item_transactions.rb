class CreateItemTransactions < ActiveRecord::Migration
  def change
    create_table :item_transactions do |t|
      t.references :item, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.references :department, index: true, foreign_key: true
      t.references :store, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.string :transaction_type
      t.integer :issue_department_id
      t.integer :recieve_store_id

      t.timestamps null: false
    end
  end
end
