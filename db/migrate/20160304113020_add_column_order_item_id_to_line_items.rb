class AddColumnOrderItemIdToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :order_item_id, :integer
  end
end
