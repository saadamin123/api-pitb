class AddIndexCategAndParentIdInRegion < ActiveRecord::Migration
  def change
  	add_index :regions, :category
  	add_index :regions, :parent_id
  end
end
