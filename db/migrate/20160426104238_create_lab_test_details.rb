class CreateLabTestDetails < ActiveRecord::Migration
  def change
    create_table :lab_test_details do |t|
      t.references :order
      t.references :lab_order_detail
      t.string :caption
      t.string :range
      t.string :unit
      t.string :value

      t.timestamps null: false
    end
  end
end
