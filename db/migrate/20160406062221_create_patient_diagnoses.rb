class CreatePatientDiagnoses < ActiveRecord::Migration
  def change
    create_table :patient_diagnoses do |t|
      t.references :patient
      t.references :user
      t.string :name
      t.string :code
      t.timestamps null: false
    end
  end
end
