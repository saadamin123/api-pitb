class IndexingOnOrderModel < ActiveRecord::Migration
  def change
  	add_index :orders, :medical_unit_id
  end
end
