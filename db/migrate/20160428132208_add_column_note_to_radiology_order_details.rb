class AddColumnNoteToRadiologyOrderDetails < ActiveRecord::Migration
  def change
    add_column :radiology_order_details, :note, :text
  end
end
