class ChangeConsulationTableName < ActiveRecord::Migration
  def change
    rename_table :consulation_order_details, :consultation_order_details
  end
end
