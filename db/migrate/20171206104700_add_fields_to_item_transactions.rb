class AddFieldsToItemTransactions < ActiveRecord::Migration
  def change
  	add_column :item_transactions, :sister_incharge_name, :string
  	add_column :item_transactions, :registrar_name, :string
  	add_column :item_transactions, :pharmacist_name, :string
  	add_column :item_transactions, :store_keeper_name, :string
  	add_column :item_transactions, :pharmacist_dms_ams_name, :string
  	add_column :item_transactions, :doctor_nurse_name, :string
  end
end
