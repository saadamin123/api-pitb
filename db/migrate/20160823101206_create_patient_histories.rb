class CreatePatientHistories < ActiveRecord::Migration
  def change
    create_table :patient_histories do |t|
      t.integer :visit_id
      t.integer :patient_id
      t.integer :medical_unit_id
      t.string :visit_reason
      t.string :chief_complaint
      t.text :diagnosis
      t.string :notes
      t.text :medication_order
      t.text :disposition
      t.string :allergies

      t.timestamps null: false
    end
  end
end
