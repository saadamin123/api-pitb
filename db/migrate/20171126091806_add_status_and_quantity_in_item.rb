class AddStatusAndQuantityInItem < ActiveRecord::Migration
  def change
  	add_column :items, :quantity, :string
  	add_column :items, :status, :string, :default => 'Open'
  end
end
