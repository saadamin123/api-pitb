class CreateFollowUps < ActiveRecord::Migration
  def change
    create_table :follow_ups do |t|
      t.references :user, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.date :follow_up

      t.timestamps null: false
    end
  end
end
