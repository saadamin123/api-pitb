class AddPhoneIntoPatient < ActiveRecord::Migration
  def change
  	add_column :patients, :phone1, :string
  	add_column :patients, :phone1_type, :string
  	add_column :patients, :phone2, :string
  	add_column :patients, :phone2_type, :string
  	add_column :patients, :phone3, :string
    add_column :patients, :phone3_type, :string
  end
end
