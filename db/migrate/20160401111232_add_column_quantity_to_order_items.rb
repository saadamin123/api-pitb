class AddColumnQuantityToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :quantity, :float
    remove_column :order_items, :frequency, :integer
    add_column :order_items, :frequency, :string
  end
end
