class AddWardIdToBay < ActiveRecord::Migration
  def change
    add_column :bays, :ward_id, :integer
  end
end
