class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.references :role
      t.string :activity
      t.boolean :access , default: true
      
      t.timestamps null: false
    end
  end
end
