class AddColumnNoteToLabOrderDetails < ActiveRecord::Migration
  def change
    add_column :lab_order_details, :note, :text
  end
end
