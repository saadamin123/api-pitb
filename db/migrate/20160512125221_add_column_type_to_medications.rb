class AddColumnTypeToMedications < ActiveRecord::Migration
  def change
    add_column :medications, :type, :string
  end
end
