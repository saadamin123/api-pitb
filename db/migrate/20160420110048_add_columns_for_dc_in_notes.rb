class AddColumnsForDcInNotes < ActiveRecord::Migration
  def change
  	add_column :notes, :admission_date, :date
    add_column :notes, :discharge_date, :date
    add_column :notes, :free_text, :text
    add_column :notes, :hospital_course, :text
    add_column :notes, :follow_up, :text
  end
end
