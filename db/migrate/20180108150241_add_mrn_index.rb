class AddMrnIndex < ActiveRecord::Migration
  def change
  	add_index :patients, :mrn, unique: true
  	add_index :patients, :phone1
  	add_index :patients, :patient_nic
  	add_index :patients, :guardian_nic
  end
end
