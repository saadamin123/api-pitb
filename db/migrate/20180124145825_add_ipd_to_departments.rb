class AddIpdToDepartments < ActiveRecord::Migration
  def change
    add_column :departments, :ipd, :boolean ,:default => false
  end
end
