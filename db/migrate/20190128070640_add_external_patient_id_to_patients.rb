class AddExternalPatientIdToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :external_patient_id, :string
  end
end
