class CreateComplaintWatchers < ActiveRecord::Migration
  def change
    create_table :complaint_watchers do |t|
      t.references :complaint
      t.references :user
      t.timestamps null: false
    end
  end
end
