class BaysCountToMedicalUnit < ActiveRecord::Migration
  def change
  	add_column :medical_units, :bays_count, :integer, :default => 0
  end
end
