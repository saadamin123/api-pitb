class ChangeStatusTypeInBedAllocation < ActiveRecord::Migration
  def change
  	change_column :bed_allocations, :status, :string
  end
end
