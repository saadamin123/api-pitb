class AddCategoryIntoMedicalUnit < ActiveRecord::Migration
  def change
  	add_column :medical_units, :category, :string
  end
end
