class BedsCountToBay < ActiveRecord::Migration
  def change
  	add_column :bays, :beds_count, :integer, :default => 0
  end
end
