class CreateInvestigationNotes < ActiveRecord::Migration
  def change
    create_table :investigation_notes do |t|
      t.integer :medical_unit_id
      t.integer :patient_id
      t.integer :visit_id
      t.integer :admission_id
      t.integer :user_id
      t.string :note
      t.string :investigation_type

      t.timestamps null: false
    end
  end
end
