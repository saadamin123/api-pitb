class CreatePatientLocations < ActiveRecord::Migration
  def change
    create_table :patient_locations do |t|
      t.string :referred_to
      t.string :ref_department
      t.integer :patient_id
      t.integer :medical_unit_id
      t.integer :referred_by

      t.timestamps null: false
    end
  end
end
