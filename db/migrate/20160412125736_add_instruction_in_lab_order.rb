class AddInstructionInLabOrder < ActiveRecord::Migration
  def change
    add_column :lab_order_details, :instructions, :string
  end
end
