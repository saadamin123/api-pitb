class AddDepartmentIdToLabOrderDetails < ActiveRecord::Migration
  def change
  	add_column :lab_order_details, :department_id, :integer
  end
end
