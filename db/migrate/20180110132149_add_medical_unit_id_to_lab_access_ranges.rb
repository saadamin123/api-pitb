class AddMedicalUnitIdToLabAccessRanges < ActiveRecord::Migration
  def change
  	add_column :lab_access_ranges, :medical_unit_id, :integer
  end
end
