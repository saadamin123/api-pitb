class AddColumnTypeToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :type, :string
  end
end
