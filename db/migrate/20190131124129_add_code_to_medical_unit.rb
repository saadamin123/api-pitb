class AddCodeToMedicalUnit < ActiveRecord::Migration
  def change
    add_column :medical_units, :code, :string
  end
end
