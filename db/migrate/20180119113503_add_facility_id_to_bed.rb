class AddFacilityIdToBed < ActiveRecord::Migration
  def change
    add_column :beds, :facility_id, :integer
  end
end
