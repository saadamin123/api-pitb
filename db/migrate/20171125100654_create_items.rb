class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :generic_item, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.references :department, index: true, foreign_key: true
      t.references :store, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :item_name
      t.string :item_code
      t.string :item_type
      t.string :main_group
      t.string :uom
      t.string :upc
      t.string :price
      t.string :sub_group
      t.string :strength
      t.string :quantity_in_hand

      t.timestamps null: false
    end
  end
end
