class CreatePatientFamilyMedicalHistories < ActiveRecord::Migration
  def change
    create_table :patient_family_medical_histories do |t|
      t.references :user, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.string :note

      t.timestamps null: false
    end
  end
end
