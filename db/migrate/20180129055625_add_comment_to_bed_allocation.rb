class AddCommentToBedAllocation < ActiveRecord::Migration
  def change
    add_column :bed_allocations, :comment, :string
  end
end
