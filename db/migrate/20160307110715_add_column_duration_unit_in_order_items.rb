class AddColumnDurationUnitInOrderItems < ActiveRecord::Migration
  def change
  	add_column :order_items, :duration_unit, :string
  	remove_column :order_items, :duration, :string
  	add_column :order_items, :duration, :integer
  end
end
