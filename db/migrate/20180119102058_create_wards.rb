class CreateWards < ActiveRecord::Migration
  def change
    create_table :wards do |t|
      t.integer :medical_unit_id
      t.integer :department_id
      t.integer :parent_id
      t.string :title
      t.boolean :active ,:default => true

      t.timestamps null: false
    end
  end
end
