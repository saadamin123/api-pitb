class AddUnidentifyInPatientModel < ActiveRecord::Migration
  def change
  	add_column :patients, :unidentify, :boolean, default: false
  end
end
