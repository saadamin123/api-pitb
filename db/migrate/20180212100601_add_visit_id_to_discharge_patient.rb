class AddVisitIdToDischargePatient < ActiveRecord::Migration
  def change
    add_column :discharge_patients, :visit_id, :integer
  end
end
