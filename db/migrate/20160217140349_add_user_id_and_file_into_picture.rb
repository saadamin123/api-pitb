class AddUserIdAndFileIntoPicture < ActiveRecord::Migration
  def change
  	add_reference :pictures, :user, index: true
  	add_column :pictures, :file, :string
  end
end
